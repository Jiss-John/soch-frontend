import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { faSearch, faEdit, faTrash, faEye, faAngleDown } from '@fortawesome/free-solid-svg-icons';
import { User } from '../../global-module/interfaces/userInterface';
import { UserApiService } from '@global/services/User/user-api.service';
import { UserService } from '@global/services/User/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css'],
  encapsulation : ViewEncapsulation.None
})
export class UserListComponent implements OnInit {
  user_list: User[];
  cols: any[];
  searchIcon = faSearch;
  edit_icon = faEdit;
  trash_icon = faTrash;
  view_icon = faEye;
  down_arrow = faAngleDown;
  totalRecords: any;
  public popoverTitle: string = '';
  public popoverMessage: string = 'Do you want to delete user?';
  public confirmClicked: boolean = false;
  public cancelClicked: boolean = false;

  constructor(private _userApiService: UserApiService,
              private _userService: UserService,
              private router: Router ) { }

  ngOnInit() {
    this.getUsers();
    this.cols = [
      { field: 'firstname', header: 'Name' },
      { field: 'userName', header: 'User Name' },
      { field: 'email', header: 'Email' },
      { field: 'roleDto', header: 'Role' },
      { field: 'mobileNumber', header: 'Mobile' },
      { field: 'isActive', header: 'Status' },
    ];
  }
  toggleDetails(user) {
    console.log('asdfsdf', JSON.stringify(user));
  }
  getUsers(): void {
    this._userApiService.getAllUsers().subscribe((Data) => {
      this.user_list = Data;
      this.totalRecords = this.user_list.length;
    }, (error) => {
      console.log(error);
    }
    );
  }

  addUser() {
    let user: User;
    this._userService.setUser(user);
    this.router.navigateByUrl('/user/add');
  }

  deleteUser(user_row): void {
 
    this._userApiService.deleteUser(user_row).subscribe((response) =>
    {
      this.getUsers();
    }, (error) => {
      console.log(error);
    }
    );
  }

  editUser(user_row: User){
    this._userService.setUser(user_row);
    this.router.navigateByUrl('/user/edit');
  }

  viewUser(user_row: User){
    this._userService.setUser(user_row);
    this.router.navigateByUrl('/user/view');
  }
}



