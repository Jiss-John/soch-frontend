import { Component, OnInit } from '@angular/core';
import { User } from '../../global-module/interfaces/userInterface';
import { Router } from '@angular/router';
import { UserService } from '@global/services/User/user.service';

@Component({
  selector: 'app-view-user',
  templateUrl: './view-user.component.html',
  styleUrls: ['./view-user.component.css']
})
export class ViewUserComponent implements OnInit {

  userObj:User;
  constructor(private _userService: UserService,private router:Router) { }

  ngOnInit() {
    if (this._userService.getUser()) {
      this.userObj = this._userService.getUser();
    }
  }

  back(){
     this.router.navigateByUrl("/user/list")
  }

}
