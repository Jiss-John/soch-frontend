import { Component, OnInit, ViewChild } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormControl, RequiredValidator } from '@angular/forms';
import { User } from '@global/interfaces/userInterface';
import { Router } from '@angular/router';
import { Designation } from '@global/interfaces/designationInterface';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import Swal from 'sweetalert2';
import { Division } from '@global/interfaces/divisionInterface';
import { FacilityType } from '@global/interfaces/facilityTypeInterface';
import { Pattern } from '@shared/infrastructure/Validator/pattern';
import { RoleApiService } from '@global/services/Role/role-api.service';
import { UserApiService } from '@global/services/User/user-api.service';
import { UserService } from '@global/services/User/user.service';
import { FacilityApiService } from '@global/services/Facility/facility-api.service';
import { DesignationApiService } from '@global/services/Designation/designation-api.service';
import { DivisionApiService } from '@global/services/Division/division-api.service';



@Component({
  selector: 'app-user-creation',
  templateUrl: './user-creation.component.html',
  styleUrls: ['./user-creation.component.css']
})
export class UserCreationComponent implements OnInit {


  designation_list: Designation[] = [];
  role_list: any[] = [];
  roleListForSelect: any=[];
  divisions_list: Division[] = [];
  facilityType_list: FacilityType[] = [];
  registerForm: FormGroup;
  submitted = false;
  userObj = new User();
  dropdownSettings: IDropdownSettings;
  errorMsg: any;
  userRoleMapping: any;
  pattern = new Pattern();
  count: number;
  title: string;
  facilities: any[];
  error: string;


  constructor(private formBuilder: FormBuilder,
    private roleApiService: RoleApiService,
    private _userApiService: UserApiService,
    private _userService: UserService,
    private facilityTypeApiService: FacilityApiService,
    private router: Router, private _designationApiService: DesignationApiService,
    private divisionApiService: DivisionApiService,
  ) { }

  ngOnInit() {
    this.createUser();
    this.divisionList();
    this.getDesignations();

    this.count = 3;
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'id',
      textField: 'name',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 4,
      allowSearchFilter: true,
    };

    if (this._userService.getUser()) {
      this.userObj = this._userService.getUser();
      console.log(this.userObj)
     
    }
    if (this.userObj.id) {
      this.count = 0;
      this.onDivisionSelect(this.userObj.divisionId);

    }
    this.errorMsg = '';
    this.title = 'User Information';
   
  }

  createUser() {
    this.registerForm = this.formBuilder.group({
      firstname: ['', [Validators.required, Validators.pattern, Validators.maxLength(99)]],
      lastname: ['', [Validators.pattern, Validators.maxLength(99)]],
      mobileNumber: ['', [Validators.required, Validators.pattern(this.pattern.mobilePattern)]],
      email: ['', [Validators.required, Validators.pattern(this.pattern.emailPattern), Validators.maxLength(99)]],
      designationId: ['', Validators.required],
      userName: ['', [Validators.required, Validators.pattern(this.pattern.userNamePattern), Validators.minLength(4)]],
      roleDto: ['',Validators.required],
      landlineNumber: ['', Validators.pattern],
      isActive: ['Active', Validators.required],
      facilityTypeId: ['', Validators.required],
      facilityId: ['', Validators.required],
      divisionId: ['', Validators.required],
      id: [''],
    }
    );
   
   
  }

  onDivisionSelect(divisionId) {
    this.facilities = [];
    this.count++;

    this.roleListForSelect = [];
    this.divisions_list.forEach(element => {
      if (element.id.toString() == this.userObj.divisionId.toString()) {
        this.facilityType_list = element.facilityTypes;
      }
    });
    
    this.facilityType_list.forEach(element => {
      if (element.id == this.userObj.facilityTypeId) {
        this.roleListForSelect = element.roleDto;
      }
    });

    if (this.count > 3) {
      this.userObj.roleDto = [];
      this.userObj.facilityTypeId = this.facilityType_list[0].id;
    }

    if (!this.userObj.id) {
      this.userObj.facilityTypeId = undefined;
    } else {
      this.onFacilityTypeSelect(this.userObj.facilityTypeId);
    }
   
  }

  onFacilityTypeSelect(event) {
    this.count++;
    this.facilityType_list.forEach(element => {
      if (element.id == this.userObj.facilityTypeId) {
        this.roleListForSelect = element.roleDto;
        if(!this.userObj.id){
           this.userObj.roleDto = this.roleListForSelect;
        }
      
      }
    });
    if (this.count > 4) {
      this.userObj.roleDto = [];
    }

    //facilty dropdown service call
    this.facilityTypeApiService.getId(this.userObj.divisionId, this.userObj.facilityTypeId)
      .subscribe(res => {
        this.facilities = res.body;
      });
  }

  onFacilitySelect(event) {
    console.log(event);
  }

  get user() { return this.registerForm.controls; }

  addUser() {

    this.submitted = true;
    if (this.registerForm.invalid) {
      if(this.userObj.roleDto == undefined){
       this.error ="Role is required"
      }
      this.errorMsg = '';
      return;
    }
    else {

      this.userObj = this.setValue(this.userObj, this.registerForm);
      console.log(this.userObj.roleDto)
      this._userApiService.addUser(this.userObj)
        .subscribe((response) => {
          Swal.fire('', 'Successfully saved changes!', 'success');

          if (this._userService.getUser()) {
            this.userObj = this._userService.getUser();
          }

          if (this.userObj.needToResendToFacility) {
            this.router.navigateByUrl('/facility/add');
          }
          else {
            this.router.navigateByUrl('/user/list');
          }
        }, (error) => {
          this.errorMsg = error.error.details[0].description;
        });
    }
  }
  divisionList(): void {
    this.divisionApiService.divisionList()
      .subscribe((data) => {
        this.divisions_list = data;
        if (this.userObj.divisionId) {
          this.onDivisionSelect(this.userObj.divisionId);
        }
      }, (error) => {
        console.log(error);
      });
  }

  getDesignations(): void {
    this._designationApiService.getAllDesignations().subscribe((Data) => {
      this.designation_list = Data;
    }, (error) => {
      console.log(error);
    }
    );
  }

  onReset() {
    this.submitted = false;
    this.registerForm.reset();
    //this.userObj = new User();
    //this.router.navigateByUrl('/user/list');
  }
  goBack() {
    this.router.navigateByUrl('/user/list');
  }


  setValue(userObj, form) {
    userObj.firstname = form.value.firstname;
    userObj.lastname = form.value.lastname;
    userObj.designationId = form.value.designationId;
    userObj.userName = form.value.userName;
    userObj.landlineNumber = form.value.landlineNumber;
    userObj.facilityTypeId = form.value.facilityTypeId;
    userObj.divisionId = form.value.divisionId;
    userObj.facilityId = form.value.facilityId;
    userObj.roleDto = form.value.roleDto;
    userObj.mobileNumber = form.value.mobileNumber;
    userObj.email = form.value.email;
    return userObj;
  }


 
}
