import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
describe('AppComponent', () => {

  let comp;

  const logger = {
    info : (x) => {},
    error : (x) => {}
  };
  const translate = {
    addLangs : (x) => {},
    setDefaultLang : (x) => {},
    getBrowserLang : () => '',
    use : (x) => {}
  };

  beforeEach(async(() => {
    comp = new AppComponent(logger as any, translate as any);
  }));
  it('should create the app', async(() => {
    expect(comp).not.toBeNull();
  }));
});
