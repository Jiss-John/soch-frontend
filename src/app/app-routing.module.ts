import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserCreationComponent } from './user/user-creation/user-creation.component';
import { AuthGuard } from './global-module/services/auth.guard';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { FacilityListComponent } from './facility/facility-list/facility-list.component';
import { FacilityCreationComponent } from './facility/facility-creation/facility-creation.component';
import { DesignationCreationComponent } from './designation/designation-creation/designation-creation.component';
import { DesignationListComponent } from './designation/designation-list/designation-list.component';
import { ViewDivisionComponent } from './division/view-division/view-division.component';
import { AddDivisionComponent } from './division/add-division/add-division.component';
import { UserListComponent } from './user/user-list/user-list.component';
import { RoleListingComponent } from './role/role-listing/role-listing.component';
import { RoleCreationComponent } from './role/role-creation/role-creation.component';
import { DivisionListComponent } from './division/division-list/division-list.component';
import { ViewDesignationComponent } from './designation/view-designation/view-designation.component';
import { ViewUserComponent } from './user/view-user/view-user.component';
import { ViewFacilityComponent } from './facility/view-facility/view-facility.component';
import { ViewRoleComponent } from './role/view-role/view-role.component';
import { MastersAddComponent } from './masters/masters-add/masters-add.component';
import { MastersListComponent } from './masters/masters-list/masters-list.component';
import { MastersViewComponent } from './masters/masters-view/masters-view.component';
import { AccessmasterscreenComponent } from './accessmasterscreen/accessmasterscreen.component';

const routes: Routes = [

  { path: "user/dashboard", pathMatch: 'full', component: DashboardComponent },

  { path: "facility/add", pathMatch: 'full', component: FacilityCreationComponent },
  { path: "facility/list", pathMatch: 'full', component: FacilityListComponent },
  { path: "facility/view", pathMatch: 'full', component: ViewFacilityComponent },

  { path: "division/list", pathMatch: 'full', component: DivisionListComponent },
  { path: "division/add", pathMatch: 'full', component: AddDivisionComponent },
  { path: "division/view", pathMatch: 'full', component: ViewDivisionComponent },

  { path: "user/list", pathMatch: 'full', component: UserListComponent },
  { path: "user/add", pathMatch: 'full', component: UserCreationComponent },
  { path: "user/edit", pathMatch: 'full', component: UserCreationComponent },
  { path: "user/view", pathMatch: 'full', component: ViewUserComponent },

  { path: "designation/add", pathMatch: 'full', component: DesignationCreationComponent },
  { path: "designation/list", pathMatch: 'full', component: DesignationListComponent },
  { path: "designation/view", pathMatch: 'full', component: ViewDesignationComponent },

  { path: "role/add", pathMatch: 'full', component: RoleCreationComponent },
  { path: "role/list", pathMatch: 'full', component: RoleListingComponent },
  { path: "role/view", pathMatch: 'full', component: ViewRoleComponent },

  { path: 'masters/list', pathMatch: 'full', component: MastersListComponent },
  { path: 'masters/add', pathMatch: 'full', component: MastersAddComponent },
  { path: 'masters/view', pathMatch: 'full', component: MastersViewComponent },

  { path: '', component: DashboardComponent, canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  {path: 'access/:id' , component:AccessmasterscreenComponent},

  {
    path: 'ti',
    loadChildren: () => import('./ti-module/ti-module.module').then(m => m.TIModule)
  },

  {
    path: 'cst',
    loadChildren: () => import('./cst-module/cst-module.module').then(m => m.CstModuleModule)
  },

  {
    path: 'art',
    loadChildren: () => import('./features/art/art.module').then(m => m.ArtModule)
  },

  {
    path: 'product',
    loadChildren: () => import('./product/product.module').then(m => m.ProductModule)
  },

  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
