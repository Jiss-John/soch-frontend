import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CstModuleRoutingModule } from './cst-module-routing.module';
import { HomeComponent } from './home/home.component';
import { CstModuleComponent } from './cst-module.component';


@NgModule({
  declarations: [CstModuleComponent,HomeComponent],
  imports: [
    CommonModule,
    CstModuleRoutingModule
  ]
})
export class CstModuleModule { }
