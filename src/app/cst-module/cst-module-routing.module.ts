import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { CstModuleComponent } from './cst-module.component';


const routes: Routes = [
  // { path: "home", component: CstModuleComponent},
  { path: '', component: CstModuleComponent,
  children: [
    { path: 'home', component: HomeComponent },
  ]
},

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CstModuleRoutingModule { }
