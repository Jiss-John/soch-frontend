import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CstModuleComponent } from './cst-module.component';

describe('CstModuleComponent', () => {
  let component: CstModuleComponent;
  let fixture: ComponentFixture<CstModuleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CstModuleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CstModuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
