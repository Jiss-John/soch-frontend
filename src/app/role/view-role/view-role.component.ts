import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Role } from '../../global-module/interfaces/roleInterface';
import { RoleService } from '@global/services/Role/role.service';

@Component({
  selector: 'app-view-role',
  templateUrl: './view-role.component.html',
  styleUrls: ['./view-role.component.css']
})
export class ViewRoleComponent implements OnInit {

  constructor(private router: Router, private roleService:RoleService) { }

  roleObj = new Role();
  
  ngOnInit() {

    if(this.roleService.getRole()){
      this.roleObj=this.roleService.getRole();
    }
  }
  addRoleButton() {
		const role = new Role();
		this.roleService.setRole(role);
		this.router.navigateByUrl('/role/add');
	}
  goBack() {
    this.router.navigateByUrl('/role/list');
  }

}
