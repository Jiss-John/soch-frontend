import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Role } from '../../global-module/interfaces/roleInterface';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { RoleApiService } from '@global/services/Role/role-api.service';
import { RoleService } from '@global/services/Role/role.service';
import { FacilityApiService } from '@global/services/Facility/facility-api.service';
import { FacilityType } from '@global/interfaces/facilityTypeInterface';
import { MastersApiService } from '@global/services/masters/masters-api.service';


@Component({
  selector: 'app-role-creation',
  templateUrl: './role-creation.component.html',
  styleUrls: ['./role-creation.component.css']
})
export class RoleCreationComponent implements OnInit {

  addRoleFormGroup: FormGroup;
  submitted = false;

  rolePattern = '^[a-zA-Z]+[ a-zA-Z]*$';
  timePattern = '^[0-9]*$';
  facilityType_list: FacilityType[] = [];
  error: string;
  constructor(private formBuilder: FormBuilder, private router: Router,
    private roleApiService: RoleApiService, private roleService: RoleService,
    private masterApiService: MastersApiService) { }

  roleObj = new Role();

  ngOnInit() {

    this.addRoleFormGroup = this.formBuilder.group({
      name: ['', [Validators.required, Validators.pattern(this.rolePattern),Validators.maxLength(99)]],
      isPrimary: ['', [Validators.required]],
      isActive: ['', [Validators.required]],
      id:[''],
      facilityTypeId: ['', [Validators.required]]
    });
     if(this.roleService.getRole()){
       this.roleObj = this.roleService.getRole();
     }
     this.getFacilityTypeList();
  }

  get role() {
    return this.addRoleFormGroup.controls;
  }
//To get all facility types 
getFacilityTypeList() {
  this.masterApiService.fetchFacilityTypeList().subscribe((Data) => {
  this.facilityType_list = Data;
  }, (error) => {
  console.log(error);
  }
  );
  } 
  saveRole() {
    this.submitted = true;
    if (this.addRoleFormGroup.invalid) {
      return;
    }
    else {
      this.roleObj = this.addRoleFormGroup.value;
      this.roleApiService.saveRole(this.roleObj)
      .subscribe((response) => {
        Swal.fire('', 'Saved Successfully!', 'success');
        this.router.navigateByUrl('/role/list');
      }, (error) => {
        this.error = error.error.details[0].description;
      });
    }
  }

  onReset() {
    this.submitted = false;
    this.addRoleFormGroup.reset();
  }

  onCancel(){
    this.onReset();
    this.router.navigateByUrl('/role/list');
  }

}

