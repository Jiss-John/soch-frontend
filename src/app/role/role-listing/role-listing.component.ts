import { Component, OnInit } from '@angular/core';
import { faSearch, faTrash, faEye, faEdit } from '@fortawesome/free-solid-svg-icons';
import { Role } from '../../global-module/interfaces/roleInterface';
import { Router } from '@angular/router';
import { RoleApiService } from '@global/services/Role/role-api.service';
import { RoleService } from '@global/services/Role/role.service';
import { AccesssettingsService } from '@global/services/accesssettings.service';

@Component({
	selector: 'app-role-listing',
	templateUrl: './role-listing.component.html',
	styleUrls: ['./role-listing.component.css']
})
export class RoleListingComponent implements OnInit {
	role_list: Role[];
	cols: any[];
	searchIcon = faSearch;
	edit_icon = faEdit;
	trash_icon = faTrash;
	view_icon = faEye;
	totalRecords: any;
	public popoverTitle: string = '';
	public popoverMessage: string = 'Do you want to delete role?';
	public confirmClicked: boolean = false;
	public cancelClicked: boolean = false;

	constructor(private roleApiService: RoleApiService, private roleService: RoleService, private router: Router, private accessService: AccesssettingsService) { }

	ngOnInit() {

		this.roleList();
		this.cols = [
			{ field: 'name', header: 'Role Name' },
			{ field: 'facilityType', header: 'Facility Type Name' },
			{ field: 'status', header: 'Status' },
		];


	}

	roleList(): void {
		this.roleApiService.roleList()
			.subscribe((data) => {
				this.role_list = data,
				this.totalRecords = this.role_list.length;
			}, (error) => {
				console.log(error);
			});
	}

	addRoleButton() {
		const role = new Role();
		this.roleService.setRole(role);
		this.router.navigateByUrl('/role/add');
	}

	editRole(role: Role) {
		this.roleService.setRole(role);
		this.router.navigateByUrl('/role/add');
	}
	deleteRole(role: Role) {
		this.roleApiService.roleDelete(role)
			.subscribe((data) => {
				this.roleList();
			}, (error) => {
				console.log(error);
			});
	}
	viewRole(role: Role) {
		this.roleService.setRole(role);
		this.router.navigateByUrl('/role/view');
	}

	viewAccess(role: Role){
	this.router.navigate(['access',role.id]);

        
	}
}
