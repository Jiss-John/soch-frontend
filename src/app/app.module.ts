import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FacilityCreationComponent } from './facility/facility-creation/facility-creation.component';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { TableModule } from 'primeng/table';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgxPaginationModule } from 'ngx-pagination';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { HttpClientModule } from '@angular/common/http';
import { DashboardComponent } from './dashboard/dashboard.component';
import { FacilityListComponent } from './facility/facility-list/facility-list.component';
import { AddDivisionComponent } from './division/add-division/add-division.component';
import { ViewDivisionComponent } from './division/view-division/view-division.component';
import { DesignationCreationComponent } from './designation/designation-creation/designation-creation.component';
import { DesignationListComponent } from './designation/designation-list/designation-list.component';
import { RoleCreationComponent } from './role/role-creation/role-creation.component';
import { RoleListingComponent } from './role/role-listing/role-listing.component';
import { DivisionListComponent } from './division/division-list/division-list.component';
import { ViewRoleComponent } from './role/view-role/view-role.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { MatCheckboxModule } from '@angular/material';
import { UserListComponent } from './user/user-list/user-list.component';
import { MastersListComponent } from './masters/masters-list/masters-list.component';
import { MastersViewComponent } from './masters/masters-view/masters-view.component';
import { MastersAddComponent } from './masters/masters-add/masters-add.component';
import { UserCreationComponent } from './user/user-creation/user-creation.component';
import { SharedModule } from '@shared/shared.module';
import { GlobalModule } from '@global/global.module';
import { ConfirmationPopoverModule } from 'angular-confirmation-popover';
import { ViewDesignationComponent } from './designation/view-designation/view-designation.component';
import { ViewUserComponent } from './user/view-user/view-user.component';
import { ViewFacilityComponent } from './facility/view-facility/view-facility.component';
import { JwtInterceptor } from './global-module/services/jwt.interceptor';
import { DatePipe } from '@angular/common';
import { TIModule } from './ti-module/ti-module.module';
import { ArtModule } from "./features/art/art.module";
import { AccessmasterscreenComponent } from './accessmasterscreen/accessmasterscreen.component';
import { StorageModule } from '@ngx-pwa/local-storage';
import { MaterialModule } from './material/material.module';

@NgModule({
  declarations: [
    AppComponent,
    FacilityCreationComponent,
    UserCreationComponent,
    LoginComponent,
    RegisterComponent,
    DashboardComponent,
    FacilityListComponent,
    AddDivisionComponent,
    ViewDivisionComponent,
    DesignationCreationComponent,
    DesignationListComponent,
    UserListComponent,
    RoleListingComponent,
    RoleCreationComponent,
    DivisionListComponent,
    ViewRoleComponent,
    ViewDesignationComponent,
    ViewUserComponent,
    ViewFacilityComponent,
    MastersListComponent,
    MastersViewComponent,
    MastersAddComponent,
    AccessmasterscreenComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FontAwesomeModule,
    NgxPaginationModule,
    BrowserAnimationsModule,
    HttpClientModule,
    TableModule,
    MaterialModule,
    NgMultiSelectDropDownModule.forRoot(),
    ConfirmationPopoverModule.forRoot({
      confirmButtonType: 'danger'
    }),
    SharedModule,
    TIModule,   
    ArtModule, 
    ArtModule, StorageModule.forRoot({ IDBNoWrap: true }), 
  ],
  entryComponents: [
    ViewDesignationComponent
  ],
  providers: [
    GlobalModule,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    DatePipe
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
