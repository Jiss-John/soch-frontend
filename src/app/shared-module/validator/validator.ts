import { AbstractControl } from '@angular/forms';

export function ValidateDropdownSelection(control: AbstractControl) {
  if (control.value =='-1' ) {
    return { invalidSelection: true };
  }
  return null;
}

export function ValidateMultiDropdownSelection(control: AbstractControl) {
    if (control.value.length ==0 ) {
      return { invalidSelection: true };
    }
    return null;
  }