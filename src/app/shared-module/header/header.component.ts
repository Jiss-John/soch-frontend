import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { faSearch,
         faEdit, faTrash, faEye,
         faWeight, faBell, faComment,
         faQuestionCircle, faArrowDown,
          faCog, faLock, faSignOutAlt
       } from '@fortawesome/free-solid-svg-icons';
import { User } from 'app/global-module/interfaces/userInterface';
import { AuthenticationService } from '@global/services/Login/authentication.service';
import { UserService } from '@global/services/User/user.service';
import { MENU } from '@shared/menu-config/parent-menu-config';
import { SUB_MENU } from '@shared/menu-config/sub-menu-config';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  private subscription: Subscription;
  message: any;
  searchIcon = faSearch;
  faWeight = faWeight;
  trash_icon = faTrash;
  view_icon = faEye;
  bell_icon = faBell;
  Chaticon = faComment;
  ques_icon = faQuestionCircle;
  down_icon = faArrowDown;
  cog_icon = faCog;
  lock_icon = faLock;
  logOut_icon = faSignOutAlt;
  user: User;
  userInitial: any;
  isShowProfile: boolean = false;
  title = 'angular-material-tab-router';
  navLinks: any[];
  activeLinkIndex = -1;
  parentMenu: any;
  accessCode: string;
  subMenu: any;
  userInfo: any;
  tempArray = [];
  constructor(private router: Router,
              private authenticationService: AuthenticationService,
              private userService: UserService) {
    // TO DO get this data from login service response
    this.parentMenu = MENU['SUPERADMIN'];
  }
  ngOnInit() {
    // this.router.events.subscribe((res) => {
    //   this.activeLinkIndex = this.navLinks.indexOf(this.navLinks.find(tab => tab.link === '.' + this.router.url));
    // });
    this.user = JSON.parse(localStorage.getItem('currentUser'));
    this.userInfo = JSON.parse(localStorage.getItem('currentUser'));
    console.log( "##accessCodes"+ this.userInfo.accessCodes);
    if(this.userInfo.accessCodes){
      this.userInfo.accessCodes.forEach(element => {
        let elem = element.split('-')[0];
        if ( elem === 'DASH') { elem = 'DASHBOARD'; } else { elem = elem; }
         if (this.tempArray.indexOf(elem) === -1) {
           this.tempArray.push(elem);
         }
       });
    }
   console.log(this.tempArray);
  }
  logout() {
    this.authenticationService.logout();
    this.router.navigate(['/login']);
  }
  toggleDiv() {
    const divToggle = document.getElementById('callout');
    if (divToggle.style.display === 'block') {
      divToggle.style.display = 'none';
    } else {
      divToggle.style.display = 'block';
    }
}
  }
