export class Pattern{
    namePattern='^([A-Za-z]+\s?)*\s*$'; //pattern allows capital and small alphabets with sinle space only
    alphanumericCodePattern='^[a-zA-Z0-9]*$';//pattern allows alphabets and numbers with no space
    emailPattern='^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$';//Email pattern[eg:abc@ust.com]
    mobilePattern='[0-9]{10}$';
    digitsOnlyPattern='^[0-9]*';
    whiteSpace='^[-a-zA-Z()]+(\s+[-a-zA-Z()]+)*$';

    pinCodePattern = "^[1-9]+[0-9]*$";
    userNamePattern = "^[a-zA-Z0-9]*$";
    passwordPattern = "^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{4,8}$";
    aadharPattern = "^[0-9]*$";
    beneficiaryCodePattern='[0-9]{14}$';
    preArtNumberPattern='[A-Z]{3}[/][0-9]{3}[/]+$';
    artNumberPattern='[A-Z]{3}[/][0-9]{3}[/]+$';
    beneficiaryTiAgePattern = '^0*(1[2-9]|[2-9][0-9]|100)$';
    beneficiaryTiNumberOfPartnersPattern = '^([1-9]|1[012345])$';
    beneficiaryTiIfYesHowManyDaysAWeekPattern = '^([1-7])$';
    agePattern = '([1-9]|[1-8][0-9]|9[0-9]|100)';
    counsellingDurationPattern = '^([0-9]|1[0-9]|2[0-4])$';
    

}
