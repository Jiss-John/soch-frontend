import { FormGroup } from '@angular/forms';

export function PasswordMatchValidator(primaryControlName: string, primaryMatchingControlName: string, secondaryControlName: string, secondaryMatchingControlName: string) {
    return (formGroup: FormGroup) => {
        const primaryControl = formGroup.controls[primaryControlName];
        const primaryMatchingControl = formGroup.controls[primaryMatchingControlName];
        const secondaryControl = formGroup.controls[secondaryControlName];
        const secondaryMatchingControl = formGroup.controls[secondaryMatchingControlName];

        if ((primaryMatchingControl.errors && !primaryMatchingControl.errors.mustMatch) || (secondaryMatchingControl.errors && !secondaryMatchingControl.errors.mustMatch)) {
            // return if another validator has already found an error on the matchingControl
            return;
        }

        // set error on matchingControl if validation fails
        if ((primaryControl.value !== primaryMatchingControl.value)||(secondaryControl.value !== secondaryMatchingControl.value)) {
            primaryMatchingControl.setErrors({ mustMatch: true });
            secondaryMatchingControl.setErrors({ mustMatch: true });
        }
        else {
            primaryMatchingControl.setErrors(null);
            secondaryMatchingControl.setErrors(null);
            console.log("yes else");
        }
    }
}