﻿import { NgModule } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

// plugins
import { DialogModule } from 'primeng/dialog';
import {ToastModule} from 'primeng/toast';
import { NgbCarouselModule } from '@ng-bootstrap/ng-bootstrap';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

import {
    RestrictInputDirective,
    EnableDisableControlsDirective
} from './directive/index';


import { FooterComponent } from './footer/footer.component';
import { SpinnerComponent } from './spinner/spinner.component';

import {
    DatexPipe,
    EllipsisPipe,
    SafeHtmlPipe,
    SplitPipe
} from './pipes/index';
import { HeaderComponent } from './header/header.component';
import { AppRoutingModule } from 'app/app-routing.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { MatTabsModule } from '@angular/material';
import { AlertComponent } from './infrastructure/alert/alert.component';

declare var resourcesVersion: any;

export function createTranslateLoader(http: HttpClient) {
    return new TranslateHttpLoader(http, './assets/i18n/', '.json?v=' + resourcesVersion);
}

@NgModule({
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        RouterModule,
        AppRoutingModule,
        FontAwesomeModule,
        DialogModule,
        ToastModule,
        NgbCarouselModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: (createTranslateLoader),
                deps: [HttpClient]
            }
        }),
        MatTabsModule,
    ],
    declarations: [
        // pipes
        DatexPipe,
        EllipsisPipe,
        SafeHtmlPipe,
        SplitPipe,

        // directives
        RestrictInputDirective,
        EnableDisableControlsDirective,

        // components
        SpinnerComponent,
        FooterComponent,
        HeaderComponent,
        AlertComponent
    ],
    providers: [
    ],
    exports: [
        // Angular modules
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        RouterModule,

        // plugins
        DialogModule,
        ToastModule,
        NgbCarouselModule,
        TranslateModule,

        // pipes
        DatexPipe,
        EllipsisPipe,
        SafeHtmlPipe,
        SplitPipe,

        // directives
        RestrictInputDirective,
        EnableDisableControlsDirective,

        // shared components
        SpinnerComponent,
        HeaderComponent,
        FooterComponent,
        AlertComponent
    ]
})

export class SharedModule {
}
