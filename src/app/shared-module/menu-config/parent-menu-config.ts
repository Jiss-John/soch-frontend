import { SUB_MENU } from './sub-menu-config';




export const MENU = {
  "SUPERADMIN":
    [
      {
        menuOrder: 0, menu: 'Dashboard',
        hasSubmenu: true, routerLink: '/user/dashboard',
        isSubmenuExpanded: false,
        submenu: null
      },
      {
        menuOrder: 1, menu: 'Users',
        hasSubmenu: false, routerLink: '/user/list',
        isSubmenuExpanded: false,
        submenu: null
      },
      {
        menuOrder: 2, menu: 'Division',
        hasSubmenu: true, routerLink: '/division/list',
        isSubmenuExpanded: false,
        submenu: null
      },
      {
        menuOrder: 3, menu: 'Roles',
        hasSubmenu: true, routerLink: '/role/list',
        isSubmenuExpanded: false,
        submenu: null
      },
      {
        menuOrder: 4, menu: 'Masters',
        hasSubmenu: true, routerLink: '/masters/list',
        isSubmenuExpanded: false,
        submenu: null
      },
      {
        menuOrder: 5, menu: 'Designation',
        hasSubmenu: true, routerLink: '/designation/list',
        isSubmenuExpanded: false,
        submenu: null
      },
      {
        menuOrder: 6, menu: 'Facility',
        hasSubmenu: true, routerLink: '/facility/list',
        isSubmenuExpanded: false,
        submenu: null
      },
      {
        menuOrder: 7, menu: 'Product',
        hasSubmenu: true, routerLink: '/product/list',
        isSubmenuExpanded: false,
        submenu: null
      },
      {
        menuOrder: 8, menu: 'TI',
        hasSubmenu: true, routerLink: '/ti/beneficiary/list',
        isSubmenuExpanded: false,
        submenu: null
      },
      {
        menuOrder: 9, menu: 'CST',
        hasSubmenu: true, routerLink: 'cst',
        isSubmenuExpanded: false,
        submenu: null
      },
      {
        menuOrder: 10, menu: 'Notification',
        hasSubmenu: true, routerLink: '',
        isSubmenuExpanded: false,
        submenu: null
            },
            {
              menuOrder: 11, menu: 'Beneficiary',
              hasSubmenu: true, routerLink: '/ti/beneficiary/list',
              isSubmenuExpanded: false,
              submenu: null
            }

    ],
  "TI":
    [
      {
        menuOrder: 0, menu: 'Dashboard',
        hasSubmenu: true, routerLink: '',
        isSubmenuExpanded: false,
        submenu: null
      },
      {
        menuOrder: 1, menu: 'Products',
        hasSubmenu: true, routerLink: '',
        isSubmenuExpanded: false,
        submenu: null
      },
      {
        menuOrder: 2, menu: 'Beneficiary',
        hasSubmenu: true, routerLink: '/ti/beneficiary/list',
        isSubmenuExpanded: false,
        submenu: null
      }, {
        menuOrder: 3, menu: 'Users',
        hasSubmenu: false, routerLink: '',
        isSubmenuExpanded: false,
        submenu: null
      },

    ]
} 
