export const SUB_MENU = {
    'NACO' :  [
        {
          label: 'Users',
          link: './user/list',
          index: 0
        }, {
          label: 'Add New User',
          link: './user/add',
          index: 1
        }, {
          label: 'Roles',
          link: './role/list',
          index: 2
        }, {
          label: 'Add New Role',
          link: './role/add',
          index: 3
        }, {
          label: 'Masters',
          link: './masters/list',
          index: 4
        }
      ],
        
    'SACS' : [
        
    ],
    'TI' : [
       
    ]
};
