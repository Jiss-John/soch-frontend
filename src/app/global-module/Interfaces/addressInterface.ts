export class Address{
    address: string;
    districtId:  number;
    stateId:  number;
    pincode: string;
    city: string;
    id: number;
    state?:any;
    district?:any;
}