export class ArtCd4RecordResult{
    id:number;
	beneficiaryId:number;
	facilityId:number;
	currentTestCount:number;
	testDate:string;
	sampleType:string
	nextAppointment:string;
	sampleId:number;
}