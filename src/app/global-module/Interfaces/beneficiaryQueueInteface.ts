export class BeneficiaryQueue {
    beneficiaryQueueId: any;
    beneficiaryId: any;
    barCode: string;
    beneficiaryStatus: string;
    beneficiaryUid: string;
    contactNumber: string;
    firstname: string;
    middlename: string;
    lastname: string;
    gender: string;
    artNumber: string;
    preArtNumber: string;
    familyId: any;
    priority: string;
    beneficiaryArtStatus: string;
    artStartDate: string;
    beneficiaryType: string;
    age: string;
    beneficiaryActivityStatus: string;
    lastVisitDate: string;
    weightBand: string;
    weight: any;
    regimenId: any;
    regimenName: string;
    assignedTo: any;

}
