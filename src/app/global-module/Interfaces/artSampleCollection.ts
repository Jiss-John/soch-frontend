export class ArtSampleCollection{
    id:number;
	beneficiaryId:number;
	facilityId:number;
	test:string;
	barcode:string;
	testType:string;
	sampleCollectedDate:string;
	sampleId:number;
    
    beneficiaryName:string;
    age:string;
    artNo:string;
	preArtNo:string;
	gender:string;
}