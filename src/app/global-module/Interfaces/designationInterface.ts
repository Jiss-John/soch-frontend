export class Designation {
    id?: number;
    title?: string;
    description?: string;
    active?: boolean;
    isDelete?: boolean;
    createdBy?: number;
    modifiedBy?: number;
    createdTime?: any;
    modifiedTime?: any;
}
