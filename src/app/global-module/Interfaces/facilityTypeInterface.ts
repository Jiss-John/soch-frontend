
export class FacilityType {
  id?: any;
  facilityTypeName?: String;
  isActive?: boolean;
  isDelete?: boolean;
  createdBy?: any;
  createdTime?: any;
  modifiedBy?: any;
  modifiedTime?: any;
  division?:any;
  user?:any;
  roleDto:any;
  facilityType:any;

}