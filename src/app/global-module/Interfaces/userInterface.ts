import { UserRoleMapping } from './userRoleMappingInteface';
import { DivisionFacilityTypeMapper } from './division-facilityMapper';

export class User {
    id: any;
	firstname: string;
	lastname: string;
	email: string;
	designationId: any;
	facilityTypeId: number;
	facilityId: number;
	divisionId: any;
	divisionName: string;
	facilityTypeName: string;
	designationName: string;
	landlineNumber: string;
	mobileNumber: any;
	status: any;
	isActive: boolean;
	isDelete: boolean;
	createdBy: any;
	createdTime: any;
	modifiedBy: any;
	modifiedTime: any;
	isBeneficiary: any;
	userAuthId: any;
	userName: string;
	password: string;
    lastLoginTime: string;
	userRoleMappingDto: UserRoleMapping[];
	divisionFacilityMap: DivisionFacilityTypeMapper;
	roleDto: any;
	needToResendToFacility: boolean;
}
