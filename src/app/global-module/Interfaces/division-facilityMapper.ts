export class DivisionFacilityTypeMapper {
  id?: any;
  facilityType_id?: any;
  division_id?: any;
  division_name?: String;
  facilityTypeName?: String;
}