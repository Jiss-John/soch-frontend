import { ProductDetail } from './productDetail';

export class ProductDispense{
    dispenseDate: String;
    dispensedTo: String;
    facProdStockTransId: number;
    beneficiaryId:number;
    facilityId: number;
    dispensationItemDtoList: ProductDetail[];
}
