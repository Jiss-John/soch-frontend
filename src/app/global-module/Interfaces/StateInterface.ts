import { District } from './districtInterface';

export class State {
    id:number;
    name:string;
    districts:District[];

  }