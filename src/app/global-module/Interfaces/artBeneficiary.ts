export class artBeneficiary{
id?: number;
firstName?: string;
gender?: string;
artNumber?: string;
preArtNumber?: string;
beneficiaryArtStatus?: string;
artStartDate?: string;
beneficiaryType?: string;
familyId?: string;
isActive?: string;
isDelete?: string;
priority?: string;
beneficiaryUid?: string;
age?: string;
contactNumber?: string;
beneficiaryStatus?: string;
beneficiaryDto?: string;
assignedTo?: string;
lastName?: string;
}
