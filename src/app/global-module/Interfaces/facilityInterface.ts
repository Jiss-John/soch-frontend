import { Address } from './addressInterface';
import { FacilityType } from './facilityTypeInterface';
import { Division } from './divisionInterface';
import { User } from './userInterface';

export class Facility {
  id: any;
  name: string;
  code: string;
  status: string;
  facilityTypeId: number;
  divisionId: number;
  stateId: number;
  districtId: number;
  city: string;
  facilityAddress: string;
  userId: number;
  pincode: string;
  address = new Address();
  facilityType = new FacilityType();
  user = new User();
  userFacilityMappingId: number;
  stateName: string;
  districtName: string;
  division = new Division();
 
}
