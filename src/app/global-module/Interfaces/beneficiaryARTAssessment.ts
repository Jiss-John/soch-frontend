export class BeneficiaryARTAssessment{

    beneficiaryName?: String;
    beneficiaryUId?: String;
    hivCareRegistrationNumber?: String;
    artRegistrationNumber?: String;
    assessmentDate?: Date;
    nameOfLac?: String;
    LacRegistrationNumber?: String;
    treatmentStatusAtRegistration?: String;
    artCentreCode?: String;
    artCentreAddress?: String;
    gender?: String;
    age?: Number;
    dob?: Date;
    category?: String;
    beneficiaryPhoneNumber?: Number;
    beneficiaryAddress?: String;
    beneficiaryCity?: String;
    beneficiaryTaluk?: String;
    beneficiaryDistrict?: String;
    beneficiaryState?: String;
    caregiversName?: String;
    caregiversAddress?: String;
    entryPoint?: String;
    beneficiaryTransferredFrom?: String;
    nameOfPreviousClinic?: String;
    dateTransferredIn?: Date;
    riskFactorHiv?: String;
    iduSubstitutionTherapy?: String;
    education?: String;
    occupationalStatus?: String;
    maritalStatus?: String;
    monthlyIncome?: Number;
    familyId?: Number;
    familyMemberUid?: String;
    relationToPatient?: String;
    parentAge?: Number;
    parentSex?: String;
    parentAlive?: String;
    hivStatus?: String;
    parentOnArt?: String;
    parentHivCare?: String;
    patientDied?: String;
    patientCorrespondingDate?: Date;
    patientOptOut?: String;
    patientOptOutCorrespondingDate?: Date;
    onMedicalAdvice?: String;
    patientOnMedicalAdviceCorrespondingDate?: Date;
    lostFollowUp?: String;
    patientLastVisitCorrespondingDate?: Date;
    habitAlcoholUse?: String;
    habitSmoking?: String;
    tobaccoUse?: String;
    patientHbv?: Boolean;
    patientHcv?: Boolean;
    hbvStatus?: String;
    hcvStatus?: String;
    otherAilments?: any;
    detailOtherAilments?: String;
    otherExistingConditions?: String;
    medicationOtherThanArt?: String;
    cptDateInitiated?: Date;
    drugAllergy?: String;
    contraception?: String;
    obstetricHistory?: String;
    lastMenstrualPeriod?: Date;
    pregnantNow?: Boolean;
    referredPptct?: Boolean;
    deliveryOutcome?: String;
    papSmear?: String;
    otherRemarks?: String;
    dateNgoCareAndSupport?: String;
    instituteNameNgo?: String;
    purposesLinkageNgo?: String;
    stayingWith?: String;
    guardianCaregiver?: String;
    guardianSex?: String;
    guardianAge?: Number;
    guardianEducation?: String;
    birthHistory?: String;
    birthWeight?: String;
    neonatalComplications?: String;
    infantFeeding?: String;
    forBreastFeed?: Number;
    firstDbs?: String;
    secondDbs?: String;
    thirdWbb?: String;
    fourthAntibodyTest?: String;
    milestoneAchieved?: String;
    immuneAgeBirth?: Date;
    immuneSixWeeks?: Date;
    immuneTenWeeks?: Date;
    immuneFourteenWeeks?: Date;
    immuneThreeMonth?: Date;
    immuneSixteenMonth?: Date;
    immuneFiveYears?: Date;
    immuneTenYears?: Date;
    immuneFifteenYears?: Date;
    vitaminNineMon?: Boolean;
    vitaminDateNine?: Date;
    vitaminEighteenMon?: Boolean;
    vitaminDateEighteen?: Date;
    vitaminTwentyFourMon?: Boolean;
    vitaminDateTwentyFour?: Date;
    vitaminThirtyMon?: Boolean;
    vitaminDateThirty?: Date;
    vitaminThirtySixMon?: Boolean;
    vitaminDateThirtySix?: Date;
    vitaminFourtyTwoMon?: Boolean;
    vitaminDateFourtyTwo?: Date;
    vitaminFourtyEightMon?: Boolean;
    vitaminDateFourtyEight?: Date;
    vitaminFiftyFourMon?: Boolean;
    vitaminDateFiftyFour?: Date;
    vitaminSixtyMon?: Boolean;
    vitaminDateSixty?: Date;
    dateNextVisit?: Date;
    tbTreatment?: Boolean;
    pregnancy?: Boolean;
    condomsGiven?: Boolean;
    counsellingNotes?: String;
    beneficiaryAccountName?: String;
    beneficiaryAccountNumber?: Number;
    beneficiaryBankIfsc?: String;
    cancelledCheque?: String;
    remarks?: String;
    dateOfLinkingLac?: Date;
    nextVisitDate?: Date;
    chooseNextInQueue?: String;
    chooseUserInQueue?: String;
    


    








   

}