import { FacilityType } from './facilityTypeInterface';

export class Role{
    id:any;
    divisionId: any;
    name: String;
    divisionName:String;
    status: String;
    isPrimary:boolean;
    isDelete:boolean;
    createdBy:any;
	createdTime:any;
	modifiedBy:any;
    modifiedTime:any;
    isActive:boolean;
    facilityTypeId:number;
    facilityType=new FacilityType();
}