export class BeneficiaryTIReferral {
   id: number;
   mobileNumber: string;
   firstName: string;
   lastName: string;
   isActive: boolean;
   beneficiaryId: number;
   activityStatus: string;
   healthStatus: string;
   uidNumber: string;
   referralStatus: string;
   referredTo: string;
   referralFacility: string;
   dateOfReferral: string;
   referredFacility: string;
   transferDate: Date;
   referredFrom: Date;
   rntcpCenter: any[];
   ictcCenter: any[];
   dsrcCenter: any[];
   tiCenter: any[];
}