export class UserRoleMapping{
    UserRoleMappingId:any;  //it is variable to store UserRoleMapp id
	userId:any;
	id:any;		//it variable to store role Id
	name:string;   //it variable to store roleName
	isActive:boolean;
	isDelete:boolean;
	createdBy:any;
	createdTime:any;
	modifiedBy:any;
	modifiedTime:any;
}