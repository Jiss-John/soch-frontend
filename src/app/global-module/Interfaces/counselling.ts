export class Counselling {
   id?: number;
   mobileNumber?: string; 
   firstName?: string;
   lastName?: string;
   durationOfCounselling?:number;
   typeOfCounsellingProvided?:string;
   noOfCondomsDistributed?:number;
   isActive?: boolean;
   beneficiaryId?:number;
   beneficiaryActivityStatus?:string;
   healthStatus?:string;
   beneficiaryUId?:string;
   beneficiaryName?:string;
  }