export class BeneficiaryRVAssessment
{
    beneficiaryId?:number;
    firstName?:string;
    lastName?:string;
    ostCode?:number;
    mobileNumber?:any;
    dueDateOfAssessment?:any;
    assessmentPending?:any;
    assessmentDate?:Date;
    beneficiaryActivityStatus?:string;
    highNumberOfSexualEncounters?:any;
    lowCondomUse?:any;
    firstYearInSexWorkAndBelowAgeOf25Years?:number;
    stiReportedInLastThreeMonthsTi?:number;
    stiReportedInLastThreeMonthsIdu?:number;
    alcohol?:number;
    unsafeSex?:number;
    violence?:number;
    condomRequirementPerWeekIdu?:number;
    condomRequirementPerWeekTi?:number;
    highNumberOfDrugUsingPartners?:number;
    sharingOfNeedlesSyringes?:number;
    injectingGreaterThanThreeTimes?:number;
    useOfAlcoholApartFromInjections?:number;
    unsafeSexSexWithNonRegularPartner?:number;
    mobilityFromOneHotspotToAnother?:number;
    needlesSyringesRequiremenPerWeek:number;
   



}