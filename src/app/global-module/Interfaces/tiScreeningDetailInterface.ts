export class TiScreeningDetail{
    screeningId?:number;
	beneficiaryId?:number;
	createdBy:number;
	dateOfScreening:Date;
	feverGreaterThanThreeWeeks:boolean;
	infection:string;
	isActive:boolean;
	isDelete:boolean;
	modifiedBy:number;
	presenceOfSweatGreaterThanThreeWeeks:boolean;
	prolongedCoughGreaterThanThreeWeeks:boolean;
	screeningStatusHiv:string;
	screeningStatusSyphilis:string;
	status:string;
	tbStatus:string;
	weightlossGreaterThan3kgInLastFourWeeks:boolean;
}