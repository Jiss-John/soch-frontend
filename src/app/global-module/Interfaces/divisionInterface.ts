import { FacilityType } from './facilityTypeInterface';

export class Division
{

    id:any;
    ddg_id:any;
    npo_id:any;
    scc_id:any;
    name: String;
    isActive:boolean;
    headDdgName: String;
    headDdgEmail: String;
    headDdgMobileNo:number;
    npoName: String;
    npoEmail: String;
    npoMobileNo:number;
    sccName: String;
    sccEmail: String;
    sccMobileNo:number;
    code: String;
    divisionAddress: String;
    created_by:any;
    created_time:any;
    modified_by:any;
    modified_time:any;
    is_delete:any;
    facilityTypeIds:any;
    facilityTypeDivisionMappings:any[];
    facilityTypes:FacilityType[]=[];
    facilityTypeDivisionMappingsList:any;
}