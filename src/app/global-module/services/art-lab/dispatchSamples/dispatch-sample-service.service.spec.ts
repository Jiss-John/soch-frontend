import { TestBed } from '@angular/core/testing';

import { DispatchSampleServiceService } from './dispatch-sample-service.service';

describe('DispatchSampleServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DispatchSampleServiceService = TestBed.get(DispatchSampleServiceService);
    expect(service).toBeTruthy();
  });
});
