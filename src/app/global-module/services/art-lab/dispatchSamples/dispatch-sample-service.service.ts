import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ArtSampleCollection } from '../../../../global-module/interfaces/artSampleCollection';
import { DispatchSample } from '../../../../global-module/Interfaces/dispatchSample';

@Injectable({
  providedIn: 'root'
})
export class DispatchSampleServiceService {

  constructor( private httpService:HttpClient) { }

  cd4CollectedSamplesList():Observable<ArtSampleCollection[]>{
    return this.httpService.get<ArtSampleCollection[]>(environment.cstAPIUrl +'lab/sample/cd4list');
  }

  ViralLoadcollectedSamplesList():Observable<ArtSampleCollection[]>{
    return this.httpService.get<ArtSampleCollection[]>(environment.cstAPIUrl +'lab/sample/virallist');
  }

  previousDispatchedCd4SampleList():Observable<DispatchSample[]>{
    return this.httpService.get<DispatchSample[]>(environment.cstAPIUrl +'lab/dispatch/cd4list');
  }

  previousDispatchedViralLoadSampleList():Observable<DispatchSample[]>{
    return this.httpService.get<DispatchSample[]>(environment.cstAPIUrl +'lab/dispatch/virallist');
  }

  dispatchSamples(samples: DispatchSample[]) {
    let body = JSON.stringify(samples);
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options = { headers: headers }
      return this.httpService.post(environment.cstAPIUrl + 'lab/dispatch', body, options);
 
  }
}
