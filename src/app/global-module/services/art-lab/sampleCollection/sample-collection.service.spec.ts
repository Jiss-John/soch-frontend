import { TestBed } from '@angular/core/testing';

import { SampleCollectionService } from './sample-collection.service';

describe('SampleCollectionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SampleCollectionService = TestBed.get(SampleCollectionService);
    expect(service).toBeTruthy();
  });
});
