import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '@env';
import { ArtSampleCollection } from '@global/interfaces/artSampleCollection';
import { ArtCd4RecordResult } from '@global/Interfaces/artCd4RecordResult';


@Injectable({
  providedIn: 'root'
})
export class SampleCollectionService {

  RecordResult(selected: ArtCd4RecordResult[]) {
      let body = JSON.stringify(selected);
      let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
      let options = { headers: headers }
        return this.httpService.post(environment.cstAPIUrl + 'lab/cd4record', body, options);

  }
  

 

  getCD4DispatchedBeneficiaryList() :Observable<ArtSampleCollection[]>{
    return this.httpService.get<ArtSampleCollection[]>(environment.cstAPIUrl +'lab/result/cd4list');
  }

  addSamples(data:ArtSampleCollection[]) {
    let body = JSON.stringify(data);
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options = { headers: headers }
      return this.httpService.post(environment.cstAPIUrl + 'lab/sample/add', body, options);
 
  }

  constructor(private httpService:HttpClient) { }

  getBeneficiaries():Observable<ArtSampleCollection[]>{
    return this.httpService.get<ArtSampleCollection[]>(environment.cstAPIUrl +'lab/beneficiary/list');
  }


}
