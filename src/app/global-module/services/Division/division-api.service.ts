import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { environment } from 'environments/environment';
import { Division } from '@global/interfaces/divisionInterface';


@Injectable({
  providedIn: 'root'
})
export class DivisionApiService {

  constructor(private _httpService: HttpClient) { }

  divisionList():Observable<Division[]>{
    return this._httpService.get<Division[]>(environment.APIUrl +'division/list');
  }

  saveDivision(division: Division) {
    let body = JSON.stringify(division);
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options = { headers: headers }
      return this._httpService.post(environment.APIUrl + 'division/add', body, options);
 
  }

  divisionDelete(division: Division) {
    let body = JSON.stringify(division);
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options = { headers: headers }
      return this._httpService.post(environment.APIUrl + 'division/delete/'+division.id,options);
    
  }


}
