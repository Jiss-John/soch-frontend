import { Injectable } from '@angular/core';
import { Division } from '@global/interfaces/divisionInterface';

@Injectable({
  providedIn: 'root'
})
export class DivisionService {

  constructor() { }

  divisionValue:Division;

  setDivision(division:Division){
    this.divisionValue=division;
  }
  getDivision(){
   return this.divisionValue;
  }
}
