import { TestBed } from '@angular/core/testing';

import { DivisionApiService } from './division-api.service';

describe('DivisionApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DivisionApiService = TestBed.get(DivisionApiService);
    expect(service).toBeTruthy();
  });
});
