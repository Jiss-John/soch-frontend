import { Injectable } from '@angular/core';
import { User } from '@global/interfaces/userInterface';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor() { }

  user=new User();

  setUser(user_row:User){
    this.user=user_row;
  }

  getUser(){
    let userData=this.user;
    return userData;
  }

}
