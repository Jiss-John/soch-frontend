import { Injectable } from '@angular/core';
import { User } from '@global/interfaces/userInterface';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { Observable, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { StorageMap } from '@ngx-pwa/local-storage';


@Injectable({
  providedIn: 'root'
})
export class UserApiService {
  private currentUserSubject: BehaviorSubject<any>;
  public currentUser: Observable<any>;


  constructor(private _httpService: HttpClient, private storageMap: StorageMap) { 
    this.currentUserSubject = new BehaviorSubject<any>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue() {
    return this.currentUserSubject.value;
  }

  addUser(user: User) {
    console.log(user);
    let body = JSON.stringify(user);
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options = { headers: headers }
    return this._httpService.post(environment.APIUrl+'user/add', body, options); 
  }

  getAllUsers(): Observable<User[]>{
    return this._httpService.get<User[]>(environment.APIUrl+'user/list');
  }

  deleteUser(user) {
    let body = JSON.stringify(user);
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options = { headers: headers }
      return this._httpService.delete(environment.APIUrl+'user/delete/'+user.id, options);
  }


  authenticate( user:User): Observable<any>{
    console.log(user);
    let body = JSON.stringify(user);
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options = { headers: headers }
      return this._httpService.post(environment.APIUrl+'auth/authenticate', body, options);
  }
  logout() {// remove user from local storage and set current user to null
    localStorage.removeItem('currentUser');
    //this.storageMap.clear().subscribe(() => {});
}
}
