import { TestBed } from '@angular/core/testing';

import { AccesssettingsService } from './accesssettings.service';

describe('AccesssettingsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AccesssettingsService = TestBed.get(AccesssettingsService);
    expect(service).toBeTruthy();
  });
});
