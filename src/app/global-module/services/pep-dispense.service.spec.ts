import { TestBed } from '@angular/core/testing';

import { PepDispenseService } from './pep-dispense.service';

describe('PepDispenseService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PepDispenseService = TestBed.get(PepDispenseService);
    expect(service).toBeTruthy();
  });
});
