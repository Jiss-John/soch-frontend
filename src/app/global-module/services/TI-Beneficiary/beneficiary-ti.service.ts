import { Injectable } from '@angular/core';
import { BeneficiaryTI } from '@global/interfaces/beneficiaryTIRegistrationInterface';

@Injectable({
  providedIn: 'root',
})
export class BeneficiaryTIService {

  constructor() { }

  beneficiaryTI:BeneficiaryTI

  setBeneficiaryTI(beneficiary_row:BeneficiaryTI){
    this.beneficiaryTI = beneficiary_row;
  }


  getBeneficiaryTI(){
    let beneficiaryData=this.beneficiaryTI;
    return beneficiaryData;
  }

  
}
