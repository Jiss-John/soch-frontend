import { TestBed, inject } from '@angular/core/testing';
import { BeneficiaryTIService } from './beneficiary-ti.service';


describe('BeneficiaryTIService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BeneficiaryTIService]
    });
  });

  it('should be created', inject([BeneficiaryTIService], (service: BeneficiaryTIService) => {
    expect(service).toBeTruthy();
  }));
});
