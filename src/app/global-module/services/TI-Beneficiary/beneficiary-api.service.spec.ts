import { TestBed } from '@angular/core/testing';

import { BeneficiaryApiService } from './beneficiary-api.service';

describe('BeneficiaryApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BeneficiaryApiService = TestBed.get(BeneficiaryApiService);
    expect(service).toBeTruthy();
  });
});
