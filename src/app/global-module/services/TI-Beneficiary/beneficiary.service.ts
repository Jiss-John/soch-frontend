import { Injectable } from '@angular/core';
import { Beneficiary } from '@global/interfaces/Beneficiary';

@Injectable({
  providedIn: 'root'
})
export class BeneficiaryService {

  constructor() { }


  beneficiary:Beneficiary;


  setBeneficiary(beneficiary_row:Beneficiary){
    this.beneficiary=beneficiary_row;
  }

  getBeneficiary(){
    let beneficiaryData=this.beneficiary;
    return beneficiaryData;
  }

  
}
