import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BeneficiaryTI } from '@global/interfaces/beneficiaryTIRegistrationInterface';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs';
import { Counselling } from '@global/Interfaces/counselling';

@Injectable({
  providedIn: 'root',
})
export class BeneficiaryTIApiService {
 

  constructor(private _httpService: HttpClient) { }

  addBeneficiaryTI(beneficiaryTI: BeneficiaryTI) {
    let body = JSON.stringify(beneficiaryTI);
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options = { headers: headers }
    if (beneficiaryTI.beneficiaryId) {
      return this._httpService.post(environment.tiAPIUrl + 'beneficiary/update', body, options);
    }
    else {
      return this._httpService.post(environment.tiAPIUrl + 'beneficiary/add', body, options);
    }
  }


  getAllBeneficiariesTI(): Observable<BeneficiaryTI[]> {
    return this._httpService.get<BeneficiaryTI[]>(environment.tiAPIUrl + 'beneficiary/list');
  }

  getAllBeneficiariesOutwardTI(): Observable<BeneficiaryTI[]> {
    return this._httpService.get<BeneficiaryTI[]>(environment.tiAPIUrl + 'beneficiary/referral/outward/list');
  }

  getAllBeneficiariesInwardTI(): Observable<BeneficiaryTI[]> {
    return this._httpService.get<BeneficiaryTI[]>(environment.tiAPIUrl + 'beneficiary/referral/inward/list');
  }
  getAllBeneficiariesTransferInTI(): Observable<BeneficiaryTI[]> {
    return this._httpService.get<BeneficiaryTI[]>(environment.tiAPIUrl + 'beneficiary/transfer/in/list');
  }
  getAllBeneficiariesTransferOutTI(): Observable<BeneficiaryTI[]> {
    return this._httpService.get<BeneficiaryTI[]>(environment.tiAPIUrl + 'beneficiary/transfer/out/list');
  }

  getAllBeneficiariesTransitTI(): Observable<BeneficiaryTI[]> {
    return this._httpService.get<BeneficiaryTI[]>(environment.tiAPIUrl + 'beneficiary/transit/list');
  }

  getAllBeneficiariesTransferOutDueTI() {
    return this._httpService.get<BeneficiaryTI[]>(environment.tiAPIUrl + 'beneficiary/transfer/out/due/list');
  }

  disableBeneficiaryTI(beneficiaryTI: BeneficiaryTI) {
    let body = JSON.stringify(beneficiaryTI);
    console.log(body);
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options = { headers: headers }
    return this._httpService.post(environment.tiAPIUrl + 'beneficiary/delete', body, options);
  }


  getMaxOfBeneficiaryId(): Observable<any> {
    return this._httpService.get<any>(environment.tiAPIUrl + 'beneficiary/beneficiaryid');
  }


  getOrwNames(): Observable<BeneficiaryTI[]> {
    return this._httpService.get<BeneficiaryTI[]>(environment.tiAPIUrl + 'beneficiary/orwnamelist');
  }

  getPeNames(): Observable<BeneficiaryTI[]> {
    return this._httpService.get<BeneficiaryTI[]>(environment.tiAPIUrl + 'beneficiary/penamelist');
  }

  getOrwCode(): Observable<BeneficiaryTI> {
    return this._httpService.get<BeneficiaryTI>(environment.tiAPIUrl + 'beneficiary/orwcode');
  }

  getPeCode(): Observable<BeneficiaryTI> {
    return this._httpService.get<BeneficiaryTI>(environment.tiAPIUrl + 'beneficiary/pecode');
  }

  doCounselling(conselObj: Counselling) {
    let body = JSON.stringify(conselObj);
    console.log(body);
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options = { headers: headers }
    return this._httpService.post(environment.tiAPIUrl + 'counselling/add', body, options);
  }

  referralAdd(referalObj: BeneficiaryTI) {
    let body = JSON.stringify(referalObj);
    console.log(body);
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options = { headers: headers }
    return this._httpService.post(environment.tiAPIUrl + 'referral/add', body, options);
  }


  rejectReferral(beneficiary_row: BeneficiaryTI) {
    let body = JSON.stringify(beneficiary_row);
    console.log(body);
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options = { headers: headers }
    return this._httpService.post(environment.tiAPIUrl + 'referral/reject', body, options);
  }

  approveReferral(beneficiary_row: BeneficiaryTI) {
    let body = JSON.stringify(beneficiary_row);
    console.log(body);
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options = { headers: headers }
    return this._httpService.post(environment.tiAPIUrl + 'referral/approve', body, options);
  }


}
