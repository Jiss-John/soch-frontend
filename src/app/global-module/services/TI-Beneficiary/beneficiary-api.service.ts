import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs';
import { Beneficiary } from '@global/interfaces/Beneficiary';
import{TiScreeningDetail} from '@global/interfaces/tiScreeningDetailInterface';
import { BeneficiaryTI } from '@global/interfaces/beneficiaryTIRegistrationInterface';
import { BeneficiaryRVAssessment } from '@global/interfaces/beneficiaryRVAssessment';
import { TiDistributionDetail } from '@global/interfaces/tiDistributionInterface';


@Injectable({
  providedIn: 'root'
})


export class BeneficiaryApiService {

  constructor(private _httpService: HttpClient) { }
  
  addBeneficiary(beneficiary:Beneficiary) {
    console.log(beneficiary);
    let body = JSON.stringify(beneficiary);
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options = { headers: headers }
    if(beneficiary.id){
      return this._httpService.post(environment.tiAPIUrl +'beneficiary/update', body, options);
    }
    else{
      return this._httpService.post(environment.tiAPIUrl+'beneficiary/add', body, options); 
    }
  }

  getAllBeneficiaries(): Observable<Beneficiary[]>{
    return this._httpService.get<Beneficiary[]>(environment.tiAPIUrl+'beneficiary/list');
  }


  deleteBeneficiary(beneficiary:Beneficiary) {
    let body = JSON.stringify(beneficiary);
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options = { headers: headers }
      return this._httpService.post(environment.tiAPIUrl+'beneficiary/delete', body, options);
  }

  //save beneficiary screening details
  saveScreeningDetails(tiScreeningDetail:TiScreeningDetail){
    let body = JSON.stringify(tiScreeningDetail);
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options = { headers: headers }
    return this._httpService.post(environment.tiAPIUrl +'screening/add', body, options);
  }

  saveRVassessmentDetails(tiRVassessment:BeneficiaryRVAssessment){
    let body = JSON.stringify(tiRVassessment);
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options = { headers: headers }
    return this._httpService.post(environment.tiAPIUrl +'rvAssessment/add', body, options);

  }

  //get beneficiary details by uid or mobile number
  getBeneficiaryBysearchValue(searchValue:String):Observable<BeneficiaryTI>{
    return this._httpService.get<BeneficiaryTI>(environment.tiAPIUrl+'beneficiary/search/'+searchValue);
  }

  saveDistributionDetails(tiDistributionDetail:TiDistributionDetail){
    let body = JSON.stringify(tiDistributionDetail);
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options = { headers: headers }
    return this._httpService.post(environment.tiAPIUrl +'distribution/add', body, options);
  }


}
