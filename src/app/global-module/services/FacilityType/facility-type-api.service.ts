import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs';
import { FacilityType } from '@global/interfaces/facilityTypeInterface';

@Injectable({
  providedIn: 'root',
})
export class FacilityTypeApiService {

  constructor(private httpService: HttpClient) { }

  addFacilityType(facilityType: FacilityType) {
    let body = JSON.stringify(facilityType);
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options = { headers: headers }
    if(facilityType.id){
      return this.httpService.post(environment.APIUrl +'facilityType/edit', body, options);
    }
    else{
      return this.httpService.post(environment.APIUrl +'facilityType/add', body, options);
    }
    

  }

  getFacilityTypes(): Observable<FacilityType[]>{
    return this.httpService.get<FacilityType[]>(environment.APIUrl+'facilityType/list');
  }

  getDivisionNotMappedFacilityTypes(): Observable<FacilityType[]>{
    return this.httpService.get<FacilityType[]>(environment.APIUrl+'facilityType/divisionNotMappedFacTypes');
  }
}
