import { Injectable } from '@angular/core';
import { FacilityType } from '@global/interfaces/facilityTypeInterface';

@Injectable({
  providedIn: 'root',
})
export class FacilityTypeService {

  constructor() { }

  
  facilityType:FacilityType;

  setFacilityType(facilityType:FacilityType){
    this.facilityType=facilityType;
  }

  getFacilityType(){
    let Data=this.facilityType;
    return Data;
  }
}
