import { TestBed, inject } from '@angular/core/testing';

import { FacilityTypeApiService } from './facility-type-api.service';

describe('FacilityTypeApiService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FacilityTypeApiService]
    });
  });

  it('should be created', inject([FacilityTypeApiService], (service: FacilityTypeApiService) => {
    expect(service).toBeTruthy();
  }));
});
