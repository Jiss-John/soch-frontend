import { Injectable } from '@angular/core';
import { Role } from '@global/interfaces/roleInterface';

@Injectable({
  providedIn: 'root'
})
export class RoleService {

  constructor() { }

  roleValue:Role;

  setRole(role:Role){
    
    this.roleValue=role;
    // this.roleValue.id=role.id;
    // this.roleValue.roleName=role. roleName;
    // this.roleValue.accessLevel=role. accessLevel;
    // this.roleValue.userType=role. userType;
    // this.roleValue.isDelete=role.isDelete;
  }
  getRole(){
   return this.roleValue;
  }
}
