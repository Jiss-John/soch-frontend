import { Injectable } from '@angular/core';
import { Role } from '@global/interfaces/roleInterface';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable} from 'rxjs';
import { environment } from 'environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RoleApiService {

  constructor(private _httpService: HttpClient) { }

  roleList():Observable<Role[]>{
    return this._httpService.get<Role[]>(environment.APIUrl +'role/list');
  }

  saveRole(role: Role) {
    let body = JSON.stringify(role);
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options = { headers: headers }
      return this._httpService.post(environment.APIUrl + 'role/add', body, options);
  }

  roleDelete(role: Role) {
    let body = JSON.stringify(role);
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options = { headers: headers }

      return this._httpService.post(environment.APIUrl + 'role/delete/'+role.id, body, options);
  }
}
