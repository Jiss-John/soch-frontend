import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { environment } from '@env';
import { User } from '@global/interfaces/userInterface';
import { BeneficiaryQueue } from '@global/Interfaces/beneficiaryQueueInteface';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PepDispenseService {
  user = new User();


  constructor(private _httpService: HttpClient) { }


  setUser(user_row: User) {
    this.user = user_row;
  }
  getUser() {
    let userData = this.user;
    return userData;
  }

  dispenseUser(user): Observable<BeneficiaryQueue> {
    let body = JSON.stringify(user);
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options = { headers: headers }
    return this._httpService.post<BeneficiaryQueue>(environment.cstAPIUrl + 'drugdispense/pep/' + user.id, options);
  }
}
