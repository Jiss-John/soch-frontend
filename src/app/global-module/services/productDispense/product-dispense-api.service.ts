import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Designation } from '@global/interfaces/designationInterface';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs';
import { ProductDispense } from '@global/Interfaces/productAddDispense';
import { BeneficiaryQueue } from '@global/Interfaces/beneficiaryQueueInteface';
@Injectable({
  providedIn: 'root'
})
export class ProductDispenseApiService {

  constructor(private _httpService: HttpClient) { }

  dispenseProduct(dispense: ProductDispense) {
    console.log(dispense);
    let body = JSON.stringify(dispense);
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options = { headers: headers };
    return this._httpService.post(environment.cstAPIUrl + 'drugdispense/savedispense ', body, options);
    }


      getBeneficiaryDetails(assignedTo: number): Observable<BeneficiaryQueue[]> {
        return this._httpService.get<BeneficiaryQueue[]>(environment.cstAPIUrl + 'beneficiaryQueue/list/' + assignedTo);
      }
}
