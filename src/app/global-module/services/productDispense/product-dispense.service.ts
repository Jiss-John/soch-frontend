import { Injectable } from '@angular/core';
import { BeneficiaryQueue } from '@global/interfaces/beneficiaryQueueInteface';
@Injectable({
  providedIn: 'root'
})
export class ProductDispenseService {

  constructor() { }

  beneficiary = new BeneficiaryQueue();

  setBeneficiary(row: BeneficiaryQueue) {
    this.beneficiary = row;
  }

  getBeneficiary() {
    return this.beneficiary;
  }

}
