import { TestBed } from '@angular/core/testing';

import { ProductDispenseApiService } from './product-dispense-api.service';

describe('ProductDispenseApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProductDispenseApiService = TestBed.get(ProductDispenseApiService);
    expect(service).toBeTruthy();
  });
});
