import { TestBed } from '@angular/core/testing';

import { ProductDispenseService } from './product-dispense.service';

describe('ProductDispenseService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProductDispenseService = TestBed.get(ProductDispenseService);
    expect(service).toBeTruthy();
  });
});
