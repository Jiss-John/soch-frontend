import { TestBed } from '@angular/core/testing';

import { MastersApiService } from './masters-api.service';

describe('MastersApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MastersApiService = TestBed.get(MastersApiService);
    expect(service).toBeTruthy();
  });
});
