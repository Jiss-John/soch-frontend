import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'environments/environment';
import { MastersList } from '@global/interfaces/mastersListInterface';
import { GlobalStatus } from '@global/interfaces/globalStatusInterface';
import { ProductType } from '@global/interfaces/productTypeInterface';
import { ProductUom } from '@global/interfaces/productUomInterface';
import { PatientInfo } from '@global/interfaces/patientInfoInterface';
import { GITStatus } from '@global/interfaces/gitStatusInterface';
import { IndentReqStatus } from '@global/interfaces/indentReqStatusInterface';
import { IndentReasons } from '@global/interfaces/indentReasonsInterface';
import { PatientStatus } from '@global/interfaces/patientStatusInterface';
import { InventoryStatus } from '@global/interfaces/inventoryStatus';
import { FacilityType } from '@global/interfaces/facilityTypeInterface';
import { Master } from '@global/Interfaces/master.interface';

@Injectable({
  providedIn: 'root'
})
export class MastersApiService {

  constructor(private _httpService: HttpClient) { }

  //Services to get values of masters

  getMasterList(): Observable<MastersList[]> {
    return this._httpService.get<MastersList[]>(environment.APIUrl + 'masters/list');
  }

  fetchFacilityTypeList(): Observable<FacilityType[]> {
    return this._httpService.get<FacilityType[]>(environment.APIUrl + 'facilityType/list');
  }

  fetchProductTypeList(): Observable<ProductType[]> {
    return this._httpService.get<ProductType[]>(environment.APIUrl + 'producttypes/list');
  }

  fetchProductUomList(): Observable<ProductUom[]> {
    return this._httpService.get<ProductUom[]>(environment.APIUrl + 'productuom/list');
  }

  fetchGlobalStatusList(): Observable<GlobalStatus[]> {
    return this._httpService.get<GlobalStatus[]>(environment.APIUrl + 'statusglobal/list');
  }

  fetchInventoryStatusList(): Observable<InventoryStatus[]> {
    return this._httpService.get<InventoryStatus[]>(environment.APIUrl + 'inventorystatus/list');
  }

  fetchGITStatusList(): Observable<GITStatus[]> {
    return this._httpService.get<GITStatus[]>(environment.APIUrl + 'gitstatus/list');
  }

  fetchIndentReqStatusList(): Observable<IndentReqStatus[]> {
    return this._httpService.get<IndentReqStatus[]>(environment.APIUrl + 'indentrequeststatus/list');
  }

  fetchIndentReasonsList(): Observable<IndentReasons[]> {
    return this._httpService.get<IndentReasons[]>(environment.APIUrl + 'indentreasons/list');
  }

  fetchPatientStatusList(): Observable<PatientStatus[]> {
    return this._httpService.get<PatientStatus[]>(environment.APIUrl + 'patientstatus/list');
  }

  fetchPatientInfoList(): Observable<PatientInfo[]> {
    return this._httpService.get<PatientInfo[]>(environment.APIUrl + 'patientinformation/list');
  }

  fetchAllLabTypes(): Observable<Master[]> {
    return this._httpService.get<Master[]>(environment.APIUrl + 'master/labTypes');
  }

  fetchAllBatchNoOptions(): Observable<Master[]> {
    return this._httpService.get<Master[]>(environment.APIUrl + 'master/batchNoOptions');
  }

  //Services to add/update values of masters

  saveGITStatus(eachMasterObj: GITStatus, masterObj: MastersList) {
    let body = JSON.stringify(eachMasterObj);
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options = { headers: headers }
    return this._httpService.post(environment.APIUrl + 'gitstatus/save', body, options);
  }

  savePatientInfo(eachMasterObj: PatientInfo, masterObj: MastersList) {
    let body = JSON.stringify(eachMasterObj);
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options = { headers: headers }
    return this._httpService.post(environment.APIUrl + 'patientinformation/save', body, options);
  }

  savePatientStatus(eachMasterObj: PatientStatus, masterObj: MastersList) {
    let body = JSON.stringify(eachMasterObj);
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options = { headers: headers }
    return this._httpService.post(environment.APIUrl + 'patientstatus/save', body, options);
    
  }

  saveIndentReasons(eachMasterObj: IndentReasons, masterObj: MastersList) {
    let body = JSON.stringify(eachMasterObj);
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options = { headers: headers }
    return this._httpService.post(environment.APIUrl + 'indentreasons/save', body, options);
 
  }

  saveIndentReqStatus(eachMasterObj: IndentReqStatus, masterObj: MastersList) {
    let body = JSON.stringify(eachMasterObj);
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options = { headers: headers }
    return this._httpService.post(environment.APIUrl + 'indentrequeststatus/save', body, options);

  }

  saveProductType(eachMasterObj: ProductType, masterObj: MastersList) {
    let body = JSON.stringify(eachMasterObj);
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options = { headers: headers }
    return this._httpService.post(environment.APIUrl + 'producttypes/add', body, options);

  }

  saveProductUom(eachMasterObj: ProductUom, masterObj: MastersList) {
    let body = JSON.stringify(eachMasterObj);
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options = { headers: headers }
    return this._httpService.post(environment.APIUrl + 'productuom/add', body, options);

  }

  saveGlobalStatus(eachMasterObj: GlobalStatus, masterObj: MastersList) {
    let body = JSON.stringify(eachMasterObj);
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options = { headers: headers }
    return this._httpService.post(environment.APIUrl + 'statusglobal/add', body, options);
  }

  addFacility(eachMasterObj: FacilityType, masterObj: MastersList) {
    let body = JSON.stringify(eachMasterObj);
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options = { headers: headers }
    return this._httpService.post(environment.APIUrl + 'facilityType/add', body, options);

  }

  saveInventoryStatus(eachMasterObj: InventoryStatus, masterObj: MastersList) {
    let body = JSON.stringify(eachMasterObj);
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options = { headers: headers }
    return this._httpService.post(environment.APIUrl + 'inventorystatus/add', body, options);
  }
}
