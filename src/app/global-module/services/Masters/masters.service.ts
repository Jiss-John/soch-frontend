import { Injectable } from '@angular/core';
import { MastersList } from '@global/interfaces/mastersListInterface';

@Injectable({
  providedIn: 'root',
})
export class MastersService {

  constructor() { }
  masters: MastersList;
  //using local storage to transfer data between components
  setMaster(master_row: MastersList) {
    localStorage.setItem('masterObj', JSON.stringify(master_row));
    this.masters = master_row;
  }

  getMaster() {
    return JSON.parse(localStorage.getItem('masterObj'));
  }

  // For individual inner master table

  eachMaster: any;

  setOneMaster(eachMaster_row: any) {
    localStorage.setItem('eachMaster', JSON.stringify(eachMaster_row));
    this.eachMaster = eachMaster_row;
  }

  getOneMaster() {
    return JSON.parse(localStorage.getItem('eachMaster'));
  }


}
