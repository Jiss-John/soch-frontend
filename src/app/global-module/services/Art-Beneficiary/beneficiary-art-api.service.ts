import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { artBeneficiary } from '@global/interfaces/artBeneficiary';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BeneficiaryArtApiService {

  constructor(private _httpClient: HttpClient) { }

  getAllBeneficiariesArt(userid): Observable<artBeneficiary[]> {
    return this._httpClient.get<artBeneficiary[]>(
      environment.cstAPIUrl + `beneficiaryQueue/list/${userid}`
      );
  }
}
