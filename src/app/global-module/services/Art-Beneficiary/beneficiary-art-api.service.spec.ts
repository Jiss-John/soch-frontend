import { TestBed } from '@angular/core/testing';

import { BeneficiaryArtApiService } from './beneficiary-art-api.service';

describe('BeneficiaryArtApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BeneficiaryArtApiService = TestBed.get(BeneficiaryArtApiService);
    expect(service).toBeTruthy();
  });
});
