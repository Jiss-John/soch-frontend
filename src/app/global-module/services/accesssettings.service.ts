import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AccesssettingsService {

  constructor(private httpClient:HttpClient) { }
 
  getAccessData(id): Observable<HttpResponse<[]>>{
 
     let url =  environment.APIUrl +'access-settings/'+id+'/list ';
    return this.httpClient.get<[]>(
        url, { observe: 'response' });
  }

  


  addAccess(id,access) {

     let body = JSON.stringify(access);
     let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
     let options = { headers: headers }
     this.httpClient.post(environment.APIUrl +'access-settings/'+id+'/save ', body, {headers: headers}).subscribe(
      (data) => {
        
      },
      (err: HttpErrorResponse) => {
          if (err.error instanceof Error) {
              console.log('Client-side error occured.');
          } else {
              console.log('Server-side error occured.');
          }
      }
  );


    
  
 
  }
}
