import { TestBed, inject } from '@angular/core/testing';

import { SelectDistrictFromStateService } from './select-district-from-state.service';

describe('SelectDistrictFromStateService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SelectDistrictFromStateService]
    });
  });

  it('should be created', inject([SelectDistrictFromStateService], (service: SelectDistrictFromStateService) => {
    expect(service).toBeTruthy();
  }));
});
