import { TestBed } from '@angular/core/testing';

import { FacilityApiService } from './facility-api.service';

describe('FacilityApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FacilityApiService = TestBed.get(FacilityApiService);
    expect(service).toBeTruthy();
  });
});
