import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'environments/environment';
import { Facility } from '@global/interfaces/facilityInterface';
import { FacilityType } from '@global/interfaces/facilityTypeInterface';

@Injectable({
  providedIn: 'root'
})
export class FacilityApiService {

  constructor(private _httpService: HttpClient) { }

  id: number;

  getAllFacilities(): Observable<Facility[]> {
    return this._httpService.get<Facility[]>(environment.APIUrl + 'facility/list');
  }

  getAllFacilityTypes(): Observable<FacilityType[]> {
    return this._httpService.get<FacilityType[]>(environment.APIUrl + 'facilityType/divisionlist');
  }

  getMaxOfFacilityId(): Observable<any> {
    return this._httpService.get<any>(environment.APIUrl + 'facility/facilityId');
  }

  saveFacility(facility: Facility) {
    let body = JSON.stringify(facility);
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options = { headers: headers }
    if (facility.id) {
      return this._httpService.post(environment.APIUrl + 'facility/update', body, options);
    }
    else {
      return this._httpService.post(environment.APIUrl + 'facility/add', body, options);
    }

  }

  deleteFacility(facility: Facility) {

    let body = JSON.stringify(facility);
    console.log(body);
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options = { headers: headers }
    return this._httpService.post(environment.APIUrl + 'facility/delete/' + facility.id, options);
  }

  getId(divisionId, facilityTypeId): Observable<HttpResponse<[]>> {
    let url = environment.APIUrl + 'facility/get/' + divisionId + '/' + facilityTypeId;
    return this._httpService.get<[]>(url, { observe: 'response' });
  }




}
