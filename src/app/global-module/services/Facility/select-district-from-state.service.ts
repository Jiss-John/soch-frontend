import { Injectable } from '@angular/core';
import { State } from '@global/interfaces/stateInterface';
import { District } from '@global/interfaces/districtInterface';
import { environment } from 'environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class SelectDistrictFromStateService {

  constructor(private httpService:HttpClient) { }
  states:State[];
  districts:District[];
  district:District;

  getAllStatesAndDistricts() {
    return this.httpService.get<State[]>(environment.APIUrl+'state/list')
  }

}
