import { Injectable } from '@angular/core';
import { Facility } from '@global/interfaces/facilityInterface';

@Injectable({
  providedIn: 'root'
})
export class FacilityService {

  constructor() { }

  facility: Facility;

  setFacility(facility_row: Facility) {
    this.facility = facility_row;
  }

  getFacility() {
    return this.facility;
  }
}
