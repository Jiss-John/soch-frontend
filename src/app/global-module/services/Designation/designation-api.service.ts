import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Designation } from '@global/interfaces/designationInterface';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DesignationApiService {

  constructor(private _httpService: HttpClient) { }

  addDesignation(designation: Designation) {
    console.log(designation);
    let body = JSON.stringify(designation);
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options = { headers: headers };
    return this._httpService.post(environment.APIUrl + 'designation/save', body, options);
    }

  getAllDesignations(): Observable<Designation[]> {
    return this._httpService.get<Designation[]>(environment.APIUrl + 'designation/list');
  }

  deleteDesignation(designation: Designation) {
    let body = JSON.stringify(designation);
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options = { headers: headers }
    return this._httpService.post(environment.APIUrl + 'designation/delete/'+designation.id, body, options);
  }
}
