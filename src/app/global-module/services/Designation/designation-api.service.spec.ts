import { TestBed } from '@angular/core/testing';

import { DesignationApiService } from './designation-api.service';

describe('DesignationApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DesignationApiService = TestBed.get(DesignationApiService);
    expect(service).toBeTruthy();
  });
});
