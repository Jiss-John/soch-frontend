import { Injectable } from '@angular/core';
import { Designation } from '@global/interfaces/designationInterface';

@Injectable({
  providedIn: 'root'
})
export class DesignationService {

  constructor() { }

  designation:Designation;

  setDesignation(designation_row:Designation){
    this.designation=designation_row;
  }

  getDesignation(){
    let designationData=this.designation;
    return designationData;
  }
}
