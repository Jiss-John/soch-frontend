﻿import {
    NgModule,
    APP_INITIALIZER,
    ErrorHandler,
    ModuleWithProviders,
    SkipSelf,
    Optional
} from '@angular/core';

import {
    AuthGuardService,
    NotificationService,
    SharedData,
    SharedDataService
} from './services/index';
import { AlertComponent } from '@shared/infrastructure/alert/alert.component';
import { LoggerService } from '@core/services/logger.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { ErrorInterceptor } from './services/error.interceptor';
import { fakeBackendProvider } from './services/fake-backend';
import { CustomNGXLoggerService, NGXMapperService, NGXLoggerHttpService } from 'ngx-logger';
import { BrowserInfoService } from '@core/Services/browser-info.service';
import { AuthService } from '@core';

export function sharedDataServiceFactory(service: SharedDataService) {
    return () => service.populateCommonData();
}

@NgModule({
    imports: [
        

    ],
    declarations: [
        
    ],
    providers: [
        AuthGuardService,
        SharedDataService,
        NotificationService,
        {
            provide: APP_INITIALIZER,
            useFactory: sharedDataServiceFactory,
            deps: [SharedDataService],
            multi: true
        }
    ],
    exports: [
    ]
})

export class GlobalModule {

    // Prevent global module to be injected multiple times
    constructor( @Optional() @SkipSelf() parentModule: GlobalModule) {
        if (parentModule) {
            throw new Error(
                'GlobalModule is already loaded. Import it in the AppModule only');
        }
    }

    static forRoot(): ModuleWithProviders {
        return {
            ngModule: GlobalModule,
            providers: [LoggerService,
                { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
                fakeBackendProvider,
                LoggerService,
                CustomNGXLoggerService,
                NGXMapperService,
                NGXLoggerHttpService,
                BrowserInfoService,
                AuthService
            ]
        };
    }
}
