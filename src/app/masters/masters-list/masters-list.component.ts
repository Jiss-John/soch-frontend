import { Component, OnInit, Inject } from '@angular/core';
import { MastersList } from '@global/interfaces/mastersListInterface';
import { Router } from '@angular/router';
import { faSearch, faEdit, faTrash, faEye } from '@fortawesome/free-solid-svg-icons';
import { MastersApiService } from '@global/services/masters/masters-api.service';
import { MastersService } from '@global/services/masters/masters.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'masters-list',
  templateUrl: './masters-list.component.html',
  styleUrls: ['./masters-list.component.css']
})
export class MastersListComponent implements OnInit {
  masters_list: MastersList[];
  list: any;
  cols: any[];
  searchIcon = faSearch;
  edit_icon = faEdit;
  trash_icon = faTrash;
  view_icon = faEye;
  totalRecords: any;
  public confirmClicked: boolean = false;
  public cancelClicked: boolean = false;

  constructor(private mastersAPIService: MastersApiService, private mastersService: MastersService, private router: Router,
  ) { }

  ngOnInit() {
    this.getMasterList();
    this.cols = [
      { field: 'id', header: 'Serial No.' },
      { field: 'masterName', header: 'Master Name' },
      { field: 'masterDescription', header: 'Description' },
    ];
  }

  /*** 
   * Fetch all masters list from server
  */
  getMasterList(): void {
    this.mastersAPIService.getMasterList().subscribe((masterData) => {
      this.masters_list = masterData;
      this.totalRecords = this.masters_list.length;
    }, (error) => {
      this.showErrorPopup(error);
    }
    );
  }


  /*** 
   * View master-navigates to corresponding master view
  */
  viewMaster(master_row: MastersList) {
    this.mastersService.setMaster(master_row);
    this.router.navigateByUrl("masters/view");
  }

/*** 
   * show service errors
  */
  showErrorPopup(error) {
    console.log("####Service Error-:" + error);
    Swal.fire('', error, 'error');
  }

}
