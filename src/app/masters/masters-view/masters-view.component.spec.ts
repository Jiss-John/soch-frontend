import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MastersViewComponent } from './masters-view.component';

describe('MastersViewComponent', () => {
  let component: MastersViewComponent;
  let fixture: ComponentFixture<MastersViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MastersViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MastersViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
