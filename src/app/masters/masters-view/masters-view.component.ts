import { Component, OnInit, Inject } from '@angular/core';
import { GlobalStatus } from '@global/interfaces/globalStatusInterface';
import { Router } from '@angular/router';
import { MastersService } from '@global/services/masters/masters.service';
import { MastersApiService } from '@global/services/masters/masters-api.service';
import { ProductType } from '@global/interfaces/productTypeInterface';
import { faSearch, faEdit, faTrash, faEye } from '@fortawesome/free-solid-svg-icons';
import { MastersList } from '@global/interfaces/mastersListInterface';
import { ProductUom } from '@global/interfaces/productUomInterface';
import { Subscription } from 'rxjs';
import { GITStatus } from '@global/interfaces/gitStatusInterface';
import { IndentReqStatus } from '@global/interfaces/indentReqStatusInterface';
import { IndentReasons } from '@global/interfaces/indentReasonsInterface';
import { PatientStatus } from '@global/interfaces/patientStatusInterface';
import { PatientInfo } from '@global/interfaces/patientInfoInterface';
import { Facility } from '@global/interfaces/facilityInterface';
import { InventoryStatus } from '@global/interfaces/inventoryStatus';
import Swal from 'sweetalert2';

@Component({
  selector: 'masters-view',
  templateUrl: './masters-view.component.html',
  styleUrls: ['./masters-view.component.css']
})
export class MastersViewComponent implements OnInit {
  cols: any[];
  masterView_list: any[];
  totalRecords: any;
  masterTitle: string = "";
  searchIcon = faSearch;
  edit_icon = faEdit;
  trash_icon = faTrash;
  view_icon = faEye;
  masterObj: MastersList;
  eachMaster: any; //used to stores row of selected values from inner list of each master
  masterViewId: string;
  masterViewData: any;
  sub: any;
  subscription: Subscription;
  addTitle:string = "New Status";
  constructor(private mastersAPIService: MastersApiService, private mastersService: MastersService, private router: Router) {
  }

  ngOnInit() {
    this.masterViewData = this.mastersService.getMaster();
    this.masterTitle = this.masterViewData && this.masterViewData.masterName ? this.masterViewData.masterName : "";
    if (this.masterViewData && this.masterViewData.id) {
      this.list_display(this.masterViewData.id);
    }
  }

  //function to display list of each masters based on the master id
  list_display(viewId) {
    //ID in the switch case must be match with main master table
    switch (viewId) {
      case 1: this.getFacilityTypeList();
        this.cols = [
          { field: 'facilityTypeName', header: 'Facility Type' },
        ];
        this.masterTitle = "Facility Types";
        this.eachMaster = new Facility; //intialize eachMaster as null object
        break;
      case 2: this.getProductTypeList();
        this.cols = [
          { field: 'productTypeName', header: 'Product Type' },
        ];
        this.eachMaster = new ProductType; //intialize eachMaster as null object
        break;
      case 3: this.getProductUomList();
        this.cols = [
          { field: 'uomName', header: 'UOM Name' },
        ];
        this.masterTitle = this.masterTitle + " - Unit of Measurement";
        this.eachMaster = new ProductUom; //intialize eachMaster as null object
        break;
      case 4: this.getGlobalStatusList();
        this.cols = [
          { field: 'globalStatusName', header: 'Status Name' },
        ];
        this.masterTitle = "Global Status";
        this.eachMaster = new GlobalStatus; //intialize eachMaster as null object
        break;
      case 5: this.getInventoryStatusList();
        this.cols = [
          { field: 'inventoryStatusName', header: 'Status Name' },
        ];
        this.masterTitle = "Inventory Status";
        this.eachMaster = new InventoryStatus; //intialize eachMaster as null object
        break;
      case 6: this.getGITStatusList();
        this.cols = [
          { field: 'gitStatusName', header: 'Global Status Name' },
        ];
        this.eachMaster = new GITStatus(); //intialize eachMaster as null object
        break;
      case 7: this.getIndentReqStatusList();
        this.cols = [
          { field: 'indentRequestStatusName', header: 'Status Name' },
        ];
        this.eachMaster = new IndentReqStatus; //intialize eachMaster as null object
        break;
      case 8: this.getIndentReasonsList();
        this.cols = [
          { field: 'indentReasonsName', header: 'Indent Reason' },
        ];
        this.eachMaster = new IndentReasons; //intialize eachMaster as null object
        this.addTitle = "Indent Reason";
        break;
      case 9: this.getPatientStatusList();
        this.cols = [
          { field: 'patientStatusName', header: 'Status Name' },
        ];
        this.masterTitle = "Patient Status";
        this.eachMaster = new PatientStatus; //intialize eachMaster as null object
        break;
      case 10: this.getPatientInfoList();
        this.cols = [
          { field: 'dataType', header: 'Data Type' },
          { field: 'dataValue', header: 'Data Value' },
        ];
        this.masterTitle = "Patient Data Values Reasons";
        this.addTitle = "New Data Value";
        this.eachMaster = new PatientInfo; //intialize eachMaster as null object
        break;
    }
  }

  /*** 
   * Fetch all Indent Reasons list from server
  */
  getIndentReasonsList() {
    this.mastersAPIService.fetchIndentReasonsList().subscribe((masterData) => {
      this.masterView_list = masterData;
      this.totalRecords = this.masterView_list.length;
    }, (error) => {
      this.showErrorPopup(error);
    }
    );
  }

  /*** 
   * Fetch all Patient Status list from server
  */
  getPatientStatusList() {
    this.mastersAPIService.fetchPatientStatusList().subscribe((masterData) => {
      this.masterView_list = masterData;
      this.totalRecords = this.masterView_list.length;
    }, (error) => {
      this.showErrorPopup(error);
    }
    );
  }

  /*** 
   * Fetch all Facility Type list from server
  */
  getFacilityTypeList() {
    this.mastersAPIService.fetchFacilityTypeList().subscribe((masterData) => {
      this.masterView_list = masterData;
      this.totalRecords = this.masterView_list.length;
    }, (error) => {
      this.showErrorPopup(error);
    }
    );
  }

  /*** 
    * Fetch all Patient Info list from server
   */
  getPatientInfoList() {
    this.mastersAPIService.fetchPatientInfoList().subscribe((masterData) => {
      this.masterView_list = masterData;
      this.totalRecords = this.masterView_list.length;
    }, (error) => {
      this.showErrorPopup(error);
    }
    );
  }

  /*** 
   * Fetch all GIT status list from server
  */
  getGITStatusList() {
    this.mastersAPIService.fetchGITStatusList().subscribe((masterData) => {
      this.masterView_list = masterData;
      this.totalRecords = this.masterView_list.length;
    }, (error) => {
      this.showErrorPopup(error);
    }
    );
  }


  /*** 
    * Fetch all Product type list from server
   */
  getProductTypeList() {
    this.mastersAPIService.fetchProductTypeList().subscribe((masterData) => {
      this.masterView_list = masterData;
      this.totalRecords = this.masterView_list.length;
    }, (error) => {
      this.showErrorPopup(error);
    }
    );
  }

  /*** 
     * Fetch all Product UOM list from server
    */
  getProductUomList() {
    this.mastersAPIService.fetchProductUomList().subscribe((masterData) => {
      this.masterView_list = masterData;
      this.totalRecords = this.masterView_list.length;
    }, (error) => {
      this.showErrorPopup(error);
    }
    );
  }

  /*** 
   * Fetch all Global status list from server
  */
  getGlobalStatusList() {
    this.mastersAPIService.fetchGlobalStatusList().subscribe((masterData) => {
      this.masterView_list = masterData;
      this.totalRecords = this.masterView_list.length;
    }, (error) => {
      this.showErrorPopup(error);
    }
    );
  }


  /*** 
   * Fetch all Inventory status list from server
  */
  getInventoryStatusList() {
    this.mastersAPIService.fetchInventoryStatusList().subscribe((masterData) => {
      this.masterView_list = masterData;
      this.totalRecords = this.masterView_list.length;
    }, (error) => {
      this.showErrorPopup(error);
    }
    );
  }

  /*** 
   * Fetch all Indent status list from server
  */
  getIndentReqStatusList() {
    this.mastersAPIService.fetchIndentReqStatusList().subscribe((masterData) => {
      this.masterView_list = masterData;
      this.totalRecords = this.masterView_list.length;
    }, (error) => {
      this.showErrorPopup(error);
    }
    );
  }

  //Function navigates back to master list
  goBack() {
    localStorage.removeItem('masterObj');
    this.router.navigateByUrl('/masters/list');
  }

  //navigates to edit page for edit each rows in inner list of each master
  editMaster(master_row: any) {
    localStorage.setItem("isAdd", "false");
    this.mastersService.setOneMaster(master_row);
    this.router.navigateByUrl("masters/add");
  }

  //navigates to add page
  addMasterBtn() {
    localStorage.setItem("isAdd", "true");
    this.mastersService.setOneMaster(this.eachMaster);
    this.router.navigateByUrl("masters/add");
  }

  /*** 
   * show service errors
  */
  showErrorPopup(error) {
    console.log("####Service Error-:" + error);
    Swal.fire('', error, 'error');
  }

}
