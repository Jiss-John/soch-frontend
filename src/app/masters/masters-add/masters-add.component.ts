import { Component, OnInit } from '@angular/core';
import { MastersApiService } from '@global/services/masters/masters-api.service';
import { MastersService } from '@global/services/masters/masters.service';
import { Router } from '@angular/router';
import { FormGroup, Validators, FormBuilder, AbstractControl } from '@angular/forms';
import Swal from 'sweetalert2';
import { MastersList } from '@global/interfaces/mastersListInterface';

@Component({
  selector: 'masters/masters-add',
  templateUrl: './masters-add.component.html',
  styleUrls: ['./masters-add.component.css']
})
export class MastersAddComponent implements OnInit {

  masterObj: MastersList;
  title: string = "";
  eachMasterObj: any;
  addMasterFormGroup: FormGroup;
  submitted = false;
  error: any;
  patientDataTypesVal: any = [];
  dataTypeSelVal: any = '';

  constructor(private formBuilder: FormBuilder, private mastersAPIService: MastersApiService, private mastersService: MastersService, private router: Router,
  ) { }

  ngOnInit() {
    this.eachMasterObj = this.mastersService.getOneMaster(); //stores row of selected values from inner list of each master
    this.masterObj = this.mastersService.getMaster(); //stores row of selected master from master list
    if (this.masterObj) {
      if (this.masterObj.masterName) {
        if (this.masterObj.id === 10) {
          this.title = "Data Value";
        } else if (this.masterObj.id === 6 || this.masterObj.id === 7) {
          this.title = "Status";
        } else if (this.masterObj.id === 8) {
          this.title = "Indent Reason";
        } else {
          this.title = this.masterObj.id === 1 || this.masterObj.id === 2 ? (this.masterObj.masterName).slice(0, -1) : this.masterObj.masterName;
        }

      }

      this.formGroup_master();
      if (this.masterObj.id === 10 && localStorage.getItem('isAdd') === 'false' && this.eachMasterObj.dataType) {
        this.dataTypeSelVal = this.eachMasterObj.dataType;
      }

    }
  }

  //fetch data type values from server
  fetchPatientDataType() {
    this.mastersAPIService.fetchPatientInfoList().subscribe((masterData) => {
      for (let i = 0; i < masterData.length; i++) {
        this.patientDataTypesVal.push(masterData[i].dataType);
      }
      if (this.patientDataTypesVal && this.patientDataTypesVal.length) {
        //this.patientDataTypesVal.push("Select Data Type");
        this.patientDataTypesVal = this.removeDuplicate(this.patientDataTypesVal);
      }

    }, (error) => {
      this.showErrorPopup(error);
    }
    );
  }
  //validate form fields using form group
  formGroup_master() {
    //ID in the switch case must be match with main master table
    switch (this.masterObj.id) {
      case 1: this.addMasterFormGroup = this.formBuilder.group({
        facilityTypeName: ['', [Validators.required, Validators.maxLength(99), Validators.pattern]],
        id: [''],
        isActive: ['active']
      });
        break;

      case 2: this.addMasterFormGroup = this.formBuilder.group({
        productTypeName: ['', [Validators.required, Validators.maxLength(99), this.emptySpaceValidator]],
        id: [''],
      });
        break;

      case 3: this.addMasterFormGroup = this.formBuilder.group({
        uomName: ['', [Validators.required, Validators.maxLength(99), this.emptySpaceValidator]],
        id: [''],
      });
        break;

      case 4: this.addMasterFormGroup = this.formBuilder.group({
        globalStatusName: ['', [Validators.required, Validators.maxLength(99), this.emptySpaceValidator]],
        id: [''],
      });
        break;

      case 5: this.addMasterFormGroup = this.formBuilder.group({
        inventoryStatusName: ['', [Validators.required, Validators.maxLength(99), this.emptySpaceValidator]],
        id: [''],
      });
        break;
      case 6: this.addMasterFormGroup = this.formBuilder.group({
        gitStatusName: ['', [Validators.required, Validators.maxLength(99), this.emptySpaceValidator]],
        id: [''],
      });
        break;
      case 7: this.addMasterFormGroup = this.formBuilder.group({
        indentRequestStatusName: ['', [Validators.required, Validators.maxLength(99), this.emptySpaceValidator]],
        id: [''],
      });
        break;
      case 8: this.addMasterFormGroup = this.formBuilder.group({
        indentReasonsName: ['', [Validators.required, Validators.maxLength(99), this.emptySpaceValidator]],
        id: [''],
      });
        break;
      case 9: this.addMasterFormGroup = this.formBuilder.group({
        patientStatusName: ['', [Validators.required, Validators.maxLength(99), this.emptySpaceValidator]],
        id: [''],
      });
        break;
      case 10: this.addMasterFormGroup = this.formBuilder.group({
        dataType: ['', [Validators.required]],
        dataValue: ['', [Validators.required, Validators.maxLength(99), this.emptySpaceValidator]],
        id: [''],
      });
        break;
    }
  }

  get master() {
    return this.addMasterFormGroup.controls;
  }

  //Save values to master
  saveMaster() {
    this.submitted = true;
    if (this.addMasterFormGroup.invalid) {
      return;
    }
    else {
      if (this.masterObj.id === 1) {
        this.eachMasterObj = this.addMasterFormGroup.value;
        this.mastersAPIService.addFacility(this.eachMasterObj, this.masterObj)
          .subscribe((response) => {
            console.log(response)
            this.showSuccessPopup(response);
          }, (error) => {
            console.log("###"+JSON.stringify(error));
            this.error =error.error ? error.error.details[0].description : '';
          });
      } else if (this.masterObj.id === 2) {
        this.mastersAPIService.saveProductType(this.eachMasterObj, this.masterObj)
          .subscribe((response) => {
            this.showSuccessPopup(response);
          }, (error) => {
            let errormsg = JSON.stringify(error);
            this.error = error.error.details[0].description;

          });
      } if (this.masterObj.id === 3) {
        this.mastersAPIService.saveProductUom(this.eachMasterObj, this.masterObj)
          .subscribe((response) => {
            this.showSuccessPopup(response);
          }, (error) => {
            this.error = error.error.details[0].description;
          });
      } else if (this.masterObj.id === 4) {
        this.mastersAPIService.saveGlobalStatus(this.eachMasterObj, this.masterObj)
          .subscribe((response) => {
            this.showSuccessPopup(response);
          }, (error) => {
            this.error = error.error.details[0].description;
          });
      } if (this.masterObj.id === 5) {
        this.mastersAPIService.saveInventoryStatus(this.eachMasterObj, this.masterObj)
          .subscribe((response) => {
            this.showSuccessPopup(response);
          }, (error) => {
            this.error = error.error.details[0].description;
          });
      } else if (this.masterObj.id === 6) {
        this.mastersAPIService.saveGITStatus(this.eachMasterObj, this.masterObj)
          .subscribe((response) => {
            this.showSuccessPopup(response);
          }, (error) => {
            this.error = error.error.details[0].description;
          });
      } else if (this.masterObj.id === 7) {
        this.mastersAPIService.saveIndentReqStatus(this.eachMasterObj, this.masterObj)
          .subscribe((response) => {
            this.showSuccessPopup(response);
          }, (error) => {
            this.error = error.error.details[0].description;
          });
      } else if (this.masterObj.id === 8) {
        this.mastersAPIService.saveIndentReasons(this.eachMasterObj, this.masterObj)
          .subscribe((response) => {
            this.showSuccessPopup(response);
          }, (error) => {
            this.error = error.error.details[0].description;
          });
      } else if (this.masterObj.id === 9) {
        this.mastersAPIService.savePatientStatus(this.eachMasterObj, this.masterObj)
          .subscribe((response) => {
            this.showSuccessPopup(response);
          }, (error) => {
            if(error)
            {
                 
                 this.error = "Status name" +" " +  this.eachMasterObj.patientStatusName + " " + "already exist.";
            }
           
            
          });
      } else if (this.masterObj.id === 10) {
        this.mastersAPIService.savePatientInfo(this.eachMasterObj, this.masterObj)
          .subscribe((response) => {
            this.showSuccessPopup(response);
          }, (error) => {
            this.error = error.error.details[0].description;
          });
      }

    }
  }

  /*** 
     * show service errors
    */
  showErrorPopup(error) {
    console.log("####Service Error-:" + error);
    Swal.fire('', error, 'error');
  }

  /*** 
     * show service success popup
    */
  showSuccessPopup(response: Object) {
    if (response) {
      Swal.fire('', 'Successfully saved changes!', 'success');
      this.router.navigateByUrl('/masters/view');
    }
  }

  /*** 
     * on cancel button click
    */
  onCancel() {
    localStorage.removeItem('eachMaster');
    this.router.navigateByUrl('/masters/view');
  }

  /*** 
     * remove duplicates from list
    */
  removeDuplicate(dataValArr) {
    let outputArray = Array.from(new Set(dataValArr))
    return outputArray
  }

  /*** 
     * dataType select on change method
    */
  changeDataType(e) {
    this.addMasterFormGroup.get('dataType').setValue(e.target.value, {
      onlySelf: true
    });

    this.eachMasterObj.dataType = e.target.value ? e.target.value : "";
  }

  emptySpaceValidator(control: AbstractControl) {
    if (control && control.value && !control.value.replace(/\s/g, '').length) {
      control.setValue('');
      return { required: true }
    }
    else {
      return null;
    }
  }

}


