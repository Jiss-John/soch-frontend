import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MastersAddComponent } from './masters-add.component';

describe('MastersAddComponent', () => {
  let component: MastersAddComponent;
  let fixture: ComponentFixture<MastersAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MastersAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MastersAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
