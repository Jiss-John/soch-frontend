import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccessmasterscreenComponent } from './accessmasterscreen.component';

describe('AccessmasterscreenComponent', () => {
  let component: AccessmasterscreenComponent;
  let fixture: ComponentFixture<AccessmasterscreenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccessmasterscreenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccessmasterscreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
