import { Component, OnInit } from '@angular/core';
import { AccesssettingsService } from '@global/services/accesssettings.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-accessmasterscreen',
  templateUrl: './accessmasterscreen.component.html',
  styleUrls: ['./accessmasterscreen.component.css']
})
export class AccessmasterscreenComponent implements OnInit {
  valuesarray: any = [];
  role_id:any;
  temparray:any=[];
  select_all = false;
  select_row = false;
   obj1;
   obj2;
   obj3;
 

  access: any = ["View", "Add", "Edit", "Delete"];
  constructor(private getDb: AccesssettingsService, private router: ActivatedRoute, private routerpath: Router,) { }

  ngOnInit() {

    this.role_id = this.router.snapshot.params['id'];
    console.log(this.role_id)
    this.getDb.getAccessData(this.role_id)
      .subscribe(res => {
        this.valuesarray = res.body;
        console.log( res.body);
        this.valuesarray.forEach(element => {

         console.log(element.module)
     
        });
  
      });

  }



  onSelectAll(e: any, row): void {

  
    this.valuesarray.forEach(key => {

      if (row == key.module) {

         this.obj1 = key.accessSettingsInfo;
         this.obj1.forEach(element => {
          element.canView = e.target.checked;
          element.canCreate = e.target.checked;
          element.canEdit = e.target.checked;
          element.canDelete = e.target.checked;
          let array:any =[];
          array.push(element);
          this.getDb.addAccess(this.role_id,array);
          
        });
      }
    })
  }


  onSelectRow(e: any, row, correskey): void {
   
  
    this.valuesarray.forEach(key => {
      if (correskey == key.module) {
        this.obj2 = key.accessSettingsInfo;

        this.obj2.forEach(element => {
          if (row == element.accessName) {
            element.canView = e.target.checked;
            element.canCreate = e.target.checked;
            element.canEdit = e.target.checked;
            element.canDelete = e.target.checked;
            let array:any =[];
          array.push(element);
          this.getDb.addAccess(this.role_id,array);
          }
        });
      }
    })


  }

  onSelectEach(e: any, row, correskey, accessComp){
    console.log(e);
    console.log(row);
    console.log(accessComp);
    console.log(correskey);
    this.valuesarray.forEach(key => {
      if (correskey == key.module) {
        this.obj3 = key.accessSettingsInfo;

        this.obj3.forEach(element => {
          if (row == element.accessName) {
            Object.keys(element).forEach(value => {
              if(accessComp==value ){
                console.log("hai"+element[value]);
                element[value] = e.target.checked;
                  
                let array:any =[];
                array.push(element);
                this.getDb.addAccess(this.role_id,array);
                
              }
             
            })
              
        
          }
        });
      }
    })

  }


  goBack() {
    this.routerpath.navigateByUrl('/role/list');
  }

}
