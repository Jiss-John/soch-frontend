import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LabTestRoutingModule } from './lab-test-routing.module';
import { Cd4TestComponent } from './cd4-test/cd4-test.component';
import { ViralLoadTestComponent } from './viral-load-test/viral-load-test.component';
import { LabTestComponent } from './lab-test.component';
import { MatTabsModule, MatCheckboxModule } from '@angular/material';


@NgModule({
  declarations: [LabTestComponent],
  imports: [
    CommonModule,
    MatTabsModule,
    MatCheckboxModule,
    LabTestRoutingModule,
  ]
})
export class LabTestModule { }
