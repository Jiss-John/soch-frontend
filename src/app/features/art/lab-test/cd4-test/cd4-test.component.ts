import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cd4-test',
  templateUrl: './cd4-test.component.html',
  styleUrls: ['./cd4-test.component.css']
})
export class Cd4TestComponent implements OnInit {
  links = [
    { label: 'Sample Collection', route: 'sampleCollection' },
    { label: 'Dispatch Collected samples', route: 'dispatchcollectedsamples' }, 
    { label: 'Previous Dispatched', route: 'previousdispatched' },
    { label: 'Record Result', route: 'recordresult' },
    { label: 'Test Result', route: 'testresult' },
   
  ];

  activeLink = this.links[0];
  
  constructor() { }

  ngOnInit() {
  }

}
