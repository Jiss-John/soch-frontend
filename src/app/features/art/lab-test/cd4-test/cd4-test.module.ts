import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Cd4TestRoutingModule } from './cd4-test-routing.module';
import { DispatchCollectedSamplesComponent } from './dispatch-collected-samples/dispatch-collected-samples.component';
import { PreviousDispatchedComponent } from './previous-dispatched/previous-dispatched.component';
import { RecordResultComponent } from './record-result/record-result.component';
import { TestResultComponent } from './test-result/test-result.component';
import { Cd4TestComponent } from './cd4-test.component';
import { TableModule } from 'primeng/table';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { MatDatepickerModule, MatNativeDateModule, MatDialogModule, MatCheckboxModule, MatFormFieldModule, MatInputModule, MatTabsModule, MatTableModule } from '@angular/material';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { SampleCollectionComponent, DialogCollectionDatePicker } from './sample-collection/sample-collection.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { NgbDatepicker, NgbModule } from '@ng-bootstrap/ng-bootstrap';





@NgModule({
  declarations: [
    DispatchCollectedSamplesComponent,
    PreviousDispatchedComponent,
    RecordResultComponent,
    TestResultComponent,
    Cd4TestComponent,
    SampleCollectionComponent,
    DialogCollectionDatePicker
  ],
  imports: [
    NgbModule,
    MatTableModule,
    CommonModule,
    MatTabsModule,
    TableModule,
    MatCheckboxModule,
    Cd4TestRoutingModule,
    FontAwesomeModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatDialogModule,
    MatCheckboxModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatDialogModule,
    MatCheckboxModule,
    MatFormFieldModule, 
    MatInputModule,
    NgMultiSelectDropDownModule,
   
  ],
  entryComponents : [DialogCollectionDatePicker]
})
export class Cd4TestModule { }
