import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { SampleCollectionService } from '@global/services/art-lab/sampleCollection/sample-collection.service';
import { ArtSampleCollection } from '@global/interfaces/artSampleCollection';
import Swal from 'sweetalert2';
import { ArtCd4RecordResult } from '@global/Interfaces/artCd4RecordResult';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-record-result',
  templateUrl: './record-result.component.html',
  styleUrls: ['./record-result.component.css']
})
export class RecordResultComponent implements OnInit {
  sample_types = [
    { value: "NULL", viewValue: "SELECT" },
    { value: "good", viewValue: "GOOD" },
    { value: "bad", viewValue: "BAD" },
    { value: "not_received", viewValue: "NOT RECEIVED" }
  ];
  currentCount: any;
  //ELEMENT_DATA: PeriodicElement[] = [];
  ELEMENT_DATA: any = [];


  displayedColumns: string[] = [
    "select",
    "beneficiaryName",
    "artNo",
    "preArtNo",
    "sampleCollectedDate",
    "lastTestCount",
    "lastTestDate",
    "currentTestCount",
    "testDate",
    "nextAppoinment",
    "sampleType"
  ];
  dataSource = new MatTableDataSource<ArtCd4RecordResult>(this.ELEMENT_DATA);
  selection = new SelectionModel<ArtCd4RecordResult>(true, []);
  
  error: any;
  nextAppoinment: string;

  constructor(private sampleCollectionService: SampleCollectionService,
    private datePipe: DatePipe) { }

  public testData: any = {};
  public testDataArray: any[] = [];
  public sample: any[] = [];
  public flag: boolean = false;
  sample_type: string;
  testDate:any;

  jk: any = false;
  rowid;

  sampleTypeOfTest;
  ngOnInit() {
    this.getBeneficiaries();
    this.testDate = this.datePipe.transform(Date.now(), 'yyyy-MM-dd')
    this.nextAppoinment = this.datePipe.transform(Date.now(), 'yyyy-MM-dd')
  }
  sampleUpdate(temp, row) {
    if (temp === "good") {
      // (this.rowid == row.id){}
      console.log(row)
      //this.onChangeCount(row)
      this.sample = [];
      this.sample.splice(row.id, 0, true);
    }
    else {

      this.sample = []
      this.sample.splice(row.id, 0, false);

    }
    row.sampleType = temp;
    console.log(temp);
    console.log(row.id);
  }

  onChangeCount(temp, row) {
    if (Number(temp.target.value)) {
      console.log(temp)
      this.jk = row.id;
    }
    else {
      this.jk = false;
    }

  }

  RecordResult() {
    console.log(this.selection)
    this.sampleCollectionService.RecordResult(this.selection.selected)
      .subscribe((response) => {
        Swal.fire('', 'Successfully added samples!', 'success');
        this.dataSource = new MatTableDataSource([]);
      }, (error) => {
        this.error = error.error.details[0].description;
      });
  }

 
  isAllSelected() {

    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    debugger;
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: ArtCd4RecordResult): string {
    if (!row) {

      return `${this.isAllSelected() ? "select" : "deselect"} all`;
    }
    return `${

      this.selection.isSelected(row) ? "deselect" : "select"
      } row ${row.id + 1}`;
  }

  getBeneficiaries() {
    this.sampleCollectionService.getCD4DispatchedBeneficiaryList()
      .subscribe((data) => {
        this.ELEMENT_DATA = data;
        this.dataSource = new MatTableDataSource(this.ELEMENT_DATA);
        console.log(this.ELEMENT_DATA)
      }, (error) => {
        console.log(error);
      });
  }

  

}


