import { Component, OnInit } from '@angular/core';
import { DispatchSampleServiceService } from '@global/services/art-lab/dispatchSamples/dispatch-sample-service.service';
import { ArtSampleCollection } from '../../../../../global-module/interfaces/artSampleCollection';
import { DispatchSample } from '../../../../../global-module/Interfaces/dispatchSample';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { Facility } from '@global/interfaces/facilityInterface';
import { FacilityApiService } from '@global/services/Facility/facility-api.service';

@Component({
  selector: 'app-dispatch-collected-samples',
  templateUrl: './dispatch-collected-samples.component.html',
  styleUrls: ['./dispatch-collected-samples.component.css']
})
export class DispatchCollectedSamplesComponent implements OnInit {

  cols: any[];
  sample_list: ArtSampleCollection[] = [];
  dispatch_list: DispatchSample[] = [];
  dispatchSample: DispatchSample = new DispatchSample();
  select_row = false;

  totalRecords: any;
  error: any;
  user: any;
  facilities_list: Facility[];
  facilityId: number;
  constructor(
    private dispatchSampleService: DispatchSampleServiceService,
    private router:Router,
    private facilityApiService:FacilityApiService
    ) { }



  ngOnInit() {
    this.cd4CollectedSamplesList();
    this.getFacilities();
    this.user = JSON.parse(localStorage.getItem('currentUser'));
    this.facilityId=this.user.facilityId;
    this.cols = [
      { field: 'beneficiaryName', header: 'Beneficiary Name' },
      { field: 'age', header: 'Age' },
      { field: 'gender', header: 'Gender' },
      { field: 'preArtNo', header: 'Pre ART No' },
      { field: 'artNo', header: 'ART No' },
      { field: 'sampleCollectedDate', header: 'Sample Date' },
    ];
  }

  addSamples(){
    this.router.navigateByUrl('/sampleCollection');

  }

  cd4CollectedSamplesList(): void {
    this.dispatchSampleService.cd4CollectedSamplesList()
      .subscribe((data) => {
        this.sample_list = data,
        console.log(this.sample_list)
          this.totalRecords = this.sample_list.length;
      }, (error) => {
        console.log(error);
      });
  }

  getFacilities(): void {
    this.facilityApiService.getAllFacilities().subscribe((facilityData) => {
      this.facilities_list = facilityData;
      console.log(this.facilities_list)
    }, (error) => {
      console.log(error);
    }
    );
  }



  onDispatch(e: any, data: ArtSampleCollection) {
    if (e.target.checked) {
      this.dispatchSample = new DispatchSample();
      this.dispatchSample.sampleId = data.sampleId;
      this.dispatchSample.beneficiaryId = data.beneficiaryId;
      this.dispatchSample.facilityId = data.facilityId;
      this.dispatch_list.push(this.dispatchSample);
    }
    if (!e.target.checked) {
      let index=0;
      this.dispatch_list.forEach(e=>{
        if(e.sampleId==data.id){
          this.dispatch_list.splice(index);
        }
        index++;
      })
    }
    console.log(this.dispatch_list);
  }

  dispatchSamples() {
    this.user = JSON.parse(localStorage.getItem('currentUser'));
    this.dispatch_list.forEach(e=>{
      e.facilityId=this.facilityId;
    })
    console.log(this.dispatch_list)
    this.dispatchSampleService.dispatchSamples(this.dispatch_list)
      .subscribe((response) => {
        Swal.fire('', 'Successfully dispatched samples', 'success');
        this.cd4CollectedSamplesList();
      }, (error) => {
        this.error = error.error.details[0].description;
      });
   }

 
  
}
