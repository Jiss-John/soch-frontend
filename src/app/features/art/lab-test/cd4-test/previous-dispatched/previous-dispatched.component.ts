import { Component, OnInit } from '@angular/core';
import { DispatchSampleServiceService } from '@global/services/art-lab/dispatchSamples/dispatch-sample-service.service';
import { DispatchSample } from '../../../../../global-module/Interfaces/dispatchSample';


@Component({
  selector: 'app-previous-dispatched',
  templateUrl: './previous-dispatched.component.html',
  styleUrls: ['./previous-dispatched.component.css']
})
export class PreviousDispatchedComponent implements OnInit {
  cols: { field: string; header: string; }[];

  dispatch_list: DispatchSample[] = [];
  totalRecords: number;

  constructor(private dispatchSampleService: DispatchSampleServiceService) { }

  ngOnInit() {
    this.previousDispatchedSampleList();
    this.cols = [
      { field: 'beneficiaryName', header: 'Beneficiary Name' },
      { field: 'age', header: 'Age' },
      { field: 'gender', header: 'Gender' },
      { field: 'preArtNo', header: 'Pre ART No' },
      { field: 'artNo', header: 'ART No' },
      { field: 'sampleCollectedDate', header: 'Sample Collected Date' },
      { field: 'dispatchDate', header: 'Dispatch Date' },
    ];
  }

  previousDispatchedSampleList() {
    this.dispatchSampleService.previousDispatchedCd4SampleList()
      .subscribe((data) => {
        this.dispatch_list = data,
        console.log(this.dispatch_list)
          this.totalRecords = this.dispatch_list.length;
      }, (error) => {
        console.log(error);
      });
  }

}
