import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Cd4TestComponent } from './cd4-test.component';

describe('Cd4TestComponent', () => {
  let component: Cd4TestComponent;
  let fixture: ComponentFixture<Cd4TestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Cd4TestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Cd4TestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
