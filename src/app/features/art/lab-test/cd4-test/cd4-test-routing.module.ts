import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DispatchCollectedSamplesComponent } from './dispatch-collected-samples/dispatch-collected-samples.component';
import { PreviousDispatchedComponent } from './previous-dispatched/previous-dispatched.component';
import { RecordResultComponent } from './record-result/record-result.component';
import { TestResultComponent } from './test-result/test-result.component';
import { Cd4TestComponent } from './cd4-test.component';
import { SampleCollectionComponent } from './sample-collection/sample-collection.component';

const mainChildRoutes: Routes = [
  { redirectTo: 'sampleCollection', path: '', pathMatch: 'full'},
  { path: 'dispatchcollectedsamples', component: DispatchCollectedSamplesComponent},
  { path: 'previousdispatched', component: PreviousDispatchedComponent},
  { path: 'recordresult', component: RecordResultComponent},
  { path: 'testresult', component: TestResultComponent},
  {path:'sampleCollection',component:SampleCollectionComponent}

];
const mainRoutes: Routes = [
  // { redirectTo: '', path: '', pathMatch: 'full' },
  { path: '', component: Cd4TestComponent, children: mainChildRoutes }
];

@NgModule({
  imports: [RouterModule.forChild(mainRoutes)],
  exports: [RouterModule]
})
export class Cd4TestRoutingModule { }
