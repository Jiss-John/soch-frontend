import { Component, OnInit, ViewChild, Inject, ɵConsole } from '@angular/core';
import { MatTableDataSource, MatTable } from '@angular/material/table';
import { SampleCollectionService } from '@global/services/art-lab/sampleCollection/sample-collection.service';
import { ArtSampleCollection } from '@global/interfaces/artSampleCollection';
import Swal from 'sweetalert2';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-sample-collection',
  templateUrl: './sample-collection.component.html',
  styleUrls: ['./sample-collection.component.css']
})
export class SampleCollectionComponent implements OnInit {

  @ViewChild('table1', { static: true }) table1: MatTable<any>;
  @ViewChild('table2', { static: true }) table2: MatTable<any>;


  beneficiary_list: ArtSampleCollection[] = [];


  ELEMENT_DATA: any = [];


  ELEMENT_DATA_Samples: ArtSampleCollection[] = [
  ];


  tests = [
    { value: 'CD4', viewValue: 'CD4' },
    { value: 'Viral Load', viewValue: 'Viral' },
    { value: 'Viral Load&CD4', viewValue: 'Viral & CD4' }
  ];

  test_types = [
    { value: 'routine', viewValue: 'Routine' },
    { value: 'targeted', viewValue: 'Targeted' },
    { value: 'repeat', viewValue: 'Repeat' },
    { value: 'adherence', viewValue: 'Adherence' }
  ];

  columns = ['beneficiaryName', 'age', 'gender', 'preArtNo', 'artNo'];
  displayedColumns = this.columns.concat(['op']);
  displayedColumns2 = [ 'beneficiaryName', 'preArtNo', 'artNo', 'test', 'barcode', 'testType', 'op1'];
  dataSource = new MatTableDataSource(this.ELEMENT_DATA);
  dataSource2 = new MatTableDataSource(this.ELEMENT_DATA_Samples);

  item: any;
  barcode: string;
  testname: any = null;
  display: boolean = false;
  testType: string;


  public testData: any = {};
  public testDataArray: any[] = [];
  error: any;
  user: any;
  closeResult: string;
  date: string;
  i: number;
  l: number;
  error1: string;
  error2: string;
  test: string;

  constructor(
    private sampleCollectionService: SampleCollectionService,
    private dialog: MatDialog,
    private datePipe: DatePipe
  ) { }

  ngOnInit() {
    this.getBeneficiaries();
    this.date = this.datePipe.transform(Date.now(), 'yyyy-MM-dd')
  }

  addItem(row: any, index: any) {
    // console.log(row, index);
    //row['testname'] = 'CD4';
    this.ELEMENT_DATA_Samples.push(row);
    this.ELEMENT_DATA.splice(index, 1);
    this.testDataArray.push({})
    this.table1.renderRows();
    // console.log(this.ELEMENT_DATA_Samples);
    this.dataSource2 = new MatTableDataSource(this.ELEMENT_DATA_Samples);
    //this.table2.renderRows();
    //debugger;
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  applyFilter2(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource2.filter = filterValue.trim().toLowerCase();
  }

  removeItem(row: any, index: any) {
    this.ELEMENT_DATA.push(row);
    this.ELEMENT_DATA_Samples.splice(index, 1);
    this.table2.renderRows();
    this.table1.renderRows();
  }
  dis(val, row) {
    row.test = val;
    if (val === 'viral' || val === 'viral&CD4') {
      this.display = true;
    }
    else { this.display = false; }
  }
  barcodeUpdate(temp, row) {
    row.barcode = temp;
  }
  test_typeUpdate(temp, row) {
    row.testType = temp;
  }

  //method to get all beneficiaries
  getBeneficiaries() {
    this.sampleCollectionService.getBeneficiaries()
      .subscribe((data) => {
        this.ELEMENT_DATA = data;
        this.dataSource = new MatTableDataSource(this.ELEMENT_DATA);
        console.log(this.ELEMENT_DATA)
      }, (error) => {
        console.log(error);
      });
  }


  openDialog(): void {
    const dialogRef = this.dialog.open(DialogCollectionDatePicker, {
      width: '250px',
      data: { date: this.date }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(result)
      this.date = result;
      this.user = JSON.parse(localStorage.getItem('currentUser'));
      this.dataSource2.data.forEach(e => {
        e.facilityId = this.user.facilityId,
          e.sampleCollectedDate = this.date;
      })

      if (result) {
        this.i = this.dataSource2.data.length;
        this.l = 0;
        this.dataSource2.data.forEach(e => {
          console.log(this.l)
          console.log(this.dataSource2.data[this.l].barcode);
          this.barcode = this.dataSource2.data[this.l].barcode;
          this.testType = this.dataSource2.data[this.l].testType;
          this.test=this.dataSource2.data[this.l].test;
          if (this.barcode == null&&this.test!='CD4' || this.testType == null&&this.test!='CD4') {
            this.error1 = "Barcode and test types are mandatory";
            console.log(this.error1)
            this.l++;
          }
          else {
            this.sampleCollectionService.addSamples(this.dataSource2.data)
              .subscribe((response) => {
                Swal.fire('', 'Successfully added samples!', 'success');
                this.dataSource2 = new MatTableDataSource([]);
                this.ELEMENT_DATA_Samples = [];
              }, (error) => {
                this.error = error.error.details[0].description;
              });
            this.error = null;
            this.error1 = null;
          }
        })

      }
    });
  }
}

export interface DialogData {
  date: Date;
  noClick: boolean;
}

@Component({
  selector: 'dialog-collection-datepicker',
  templateUrl: 'dialog-collection-datepicker.html',
})
export class DialogCollectionDatePicker {

  dailogdata: DialogData;
  constructor(
    public dialogRef: MatDialogRef<DialogCollectionDatePicker>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) { }

  onNoClick(): void {
    this.dialogRef.close();
  }



}


