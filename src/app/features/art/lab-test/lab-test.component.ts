import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-lab-test',
  templateUrl: './lab-test.component.html',
  styleUrls: ['./lab-test.component.css']
})
export class LabTestComponent implements OnInit {

  links = [
    { label: 'CD4 Test', route: 'cd4-test' }, 
    { label: 'Viral Load Test', route: 'viralload-test' }
  ];

  activeLink = this.links[0];
  
  constructor() { }

  ngOnInit() {
  }

}
