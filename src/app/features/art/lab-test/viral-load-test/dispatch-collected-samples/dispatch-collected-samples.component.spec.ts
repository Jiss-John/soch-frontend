import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DispatchCollectedSamplesComponent } from './dispatch-collected-samples.component';

describe('DispatchCollectedSamplesComponent', () => {
  let component: DispatchCollectedSamplesComponent;
  let fixture: ComponentFixture<DispatchCollectedSamplesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DispatchCollectedSamplesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DispatchCollectedSamplesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
