import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViralLoadTestComponent } from './viral-load-test.component';
import { TestResultComponent } from './test-result/test-result.component';
import { SampleCollectionComponent } from '../cd4-test/sample-collection/sample-collection.component';
import { DispatchCollectedSamplesComponent } from './dispatch-collected-samples/dispatch-collected-samples.component';
import { PreviousDispatchedComponent } from './previous-dispatched/previous-dispatched.component';


const mainChildRoutes: Routes = [
  { redirectTo: 'dispatchcollectedsamples', path: '', pathMatch: 'full'},
  { path: 'dispatchcollectedsamples', component: DispatchCollectedSamplesComponent},
  { path: 'previousdispatched', component: PreviousDispatchedComponent},
  { path: 'testresult', component: TestResultComponent}


];
const mainRoutes: Routes = [
  // { redirectTo: '', path: '', pathMatch: 'full' },
  { path: '', component: ViralLoadTestComponent, children: mainChildRoutes }
];


@NgModule({
  imports: [RouterModule.forChild(mainRoutes)],
  exports: [RouterModule]
})
export class ViralLoadTestRoutingModule { }
