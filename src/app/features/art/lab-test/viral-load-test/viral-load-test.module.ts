import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { ViralLoadTestRoutingModule } from './viral-load-test-routing.module';
import { DispatchCollectedSamplesComponent } from './dispatch-collected-samples/dispatch-collected-samples.component';
import { ViralLoadTestComponent } from './viral-load-test.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatTabsModule, MatTableModule, MatCheckboxModule, MatDatepickerModule, MatDialogModule, MatNativeDateModule, MatFormFieldModule, MatInputModule } from '@angular/material';
import { TestResultComponent } from './test-result/test-result.component';
import { PreviousDispatchedComponent } from './previous-dispatched/previous-dispatched.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { TableModule } from 'primeng/table';




@NgModule({
  declarations: [
    DispatchCollectedSamplesComponent,
    ViralLoadTestComponent,
    TestResultComponent,
    PreviousDispatchedComponent
  ],
  imports: [
    CommonModule,
    ViralLoadTestRoutingModule,
    NgbModule,
    MatTabsModule,
    MatTableModule,
    TableModule,
    MatCheckboxModule,
    FontAwesomeModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatDialogModule,
    MatCheckboxModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    FormsModule,
    ReactiveFormsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatDialogModule,
    MatCheckboxModule,
    MatFormFieldModule, 
    MatInputModule,
  ]
})
export class ViralLoadTestModule { }
