import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-viral-load-test',
  templateUrl: './viral-load-test.component.html',
  styleUrls: ['./viral-load-test.component.css']
})
export class ViralLoadTestComponent implements OnInit {
  links = [
    { label: 'Dispatch Collected samples', route: 'dispatchcollectedsamples' }, 
    { label: 'Previous Dispatched', route: 'previousdispatched' },
    { label: 'Test Result', route: 'testresult' },
   
  ];

  activeLink = this.links[0];
  constructor() { }

  ngOnInit() {
  }

}
