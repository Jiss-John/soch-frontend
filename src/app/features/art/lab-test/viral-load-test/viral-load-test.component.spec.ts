import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViralLoadTestComponent } from './viral-load-test.component';

describe('ViralLoadTestComponent', () => {
  let component: ViralLoadTestComponent;
  let fixture: ComponentFixture<ViralLoadTestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViralLoadTestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViralLoadTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
