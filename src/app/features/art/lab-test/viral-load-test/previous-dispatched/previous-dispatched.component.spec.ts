import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreviousDispatchedComponent } from './previous-dispatched.component';

describe('PreviousDispatchedComponent', () => {
  let component: PreviousDispatchedComponent;
  let fixture: ComponentFixture<PreviousDispatchedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreviousDispatchedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreviousDispatchedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
