import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LabTestComponent } from './lab-test.component';
import { AuthGuard } from '@global/services/auth.guard';


const mainChildRoutes:Routes=[
  { redirectTo: 'cd4-test', path: '', pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'cd4-test', loadChildren: () => import('./cd4-test/cd4-test.module').then(m => m.Cd4TestModule)},
  { path: 'viralload-test', loadChildren: () => import('./viral-load-test/viral-load-test.module').then(m => m.ViralLoadTestModule)},
]

const mainRoutes: Routes = [
  { path: '', component: LabTestComponent, children: mainChildRoutes }
];

@NgModule({
  imports: [RouterModule.forChild(mainRoutes)],
  exports: [RouterModule]
})

export class LabTestRoutingModule{}
