import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ArtRoutingModule } from './art-routing.module';
import { ArtComponent } from './art.component';
import { LabTestComponent } from './lab-test/lab-test.component';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { BeneficiaryArtModule } from './beneficiary-art/beneficiary-art.module';
import { LabTestModule } from './lab-test/lab-test.module';
import { MatDatepickerModule, MatNativeDateModule, MatDialogModule, MatCheckboxModule, MatFormFieldModule, MatInputModule } from '@angular/material';



@NgModule({
  declarations: [ArtComponent],
  imports: [
    CommonModule,
    ArtRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatDialogModule,
    MatCheckboxModule,
    MatFormFieldModule,
    NgMultiSelectDropDownModule,
        MatInputModule,
    
    MatInputModule
   
  ]
})
export class ArtModule { }
