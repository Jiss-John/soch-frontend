import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TransferInComponent } from './transfer-in/transfer-in.component';
import { BeneficiaryArtComponent } from './beneficiary-art.component';
import { TransferOutComponent } from './transfer-out/transfer-out.component';
import { LacBeneficiaryComponent } from './lac-beneficiary/lac-beneficiary.component';
import { DispenseComponent } from './dispense/dispense.component';
import { ReferralComponent } from './referral/referral.component';
import { AssessmentComponent } from './assessment/assessment.component';
import { ArtBeneficiaryComponent } from './list/list.component';
import { AssessComponent } from "./assess/assess.component";
import { BeneficiaryDispenseComponent } from './dispense/beneficiary-dispense/beneficiary-dispense.component';
import { PepDispenseComponent } from './dispense/pep-dispense/pep-dispense.component';


const mainChildRoutes: Routes = [
  { redirectTo: 'transfer-in', path: '', pathMatch: 'full'},
  { path: 'transfer-in', component: TransferInComponent},
  { path: 'transfer-out', component: TransferOutComponent},
  { path: 'lac-beneficiary', component: LacBeneficiaryComponent},
  { path: 'refferal', component: ReferralComponent},
  { path: 'assessment', component: AssessmentComponent},
  { path: 'list', component: ArtBeneficiaryComponent},
  { path: 'assess', component: AssessComponent},
  { path: 'dispense', component: DispenseComponent,
  children : [
    {
      path: 'beneficiary-dispense',
      component: BeneficiaryDispenseComponent
      }
  ]
}
];

const mainRoutes: Routes = [
  { redirectTo: '', path: '', pathMatch: 'full' },
  { path: '', component: BeneficiaryArtComponent, children: mainChildRoutes }
];

// @NgModule({
//   imports: [RouterModule.forChild(mainRoutes)],
//   exports: [RouterModule]
// })

@NgModule({
  imports: [RouterModule.forChild(mainRoutes)],
  exports: [RouterModule]
})
export class BeneficiaryArtRoutingModule { }
