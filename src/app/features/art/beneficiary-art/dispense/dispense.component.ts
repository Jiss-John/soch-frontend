import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatTabGroup } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { User } from '@global/interfaces/userInterface';
import { BeneficiaryQueue } from '@global/Interfaces/beneficiaryQueueInteface';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { ProductDispenseApiService } from '@global/services/productDispense/product-dispense-api.service';
import { ProductDispenseService } from '@global/services/productDispense/product-dispense.service';


export interface UserData {
  id: string;
  name: string;
  progress: string;
  color: string;
  position: number;
}

const COLORS: string[] = [
  'maroon', 'red', 'orange', 'yellow', 'olive', 'green', 'purple', 'fuchsia', 'lime', 'teal',
  'aqua', 'blue', 'navy', 'black', 'gray'
];
const NAMES: string[] = [
  'Maia', 'Asher', 'Olivia', 'Atticus', 'Amelia', 'Jack', 'Charlotte', 'Theodore', 'Isla', 'Oliver',
  'Isabella', 'Jasper', 'Cora', 'Levi', 'Violet', 'Arthur', 'Mia', 'Thomas', 'Elizabeth'
];

@Component({
  selector: 'app-dispense',
  templateUrl: './dispense.component.html',
  styleUrls: ['./dispense.component.css']
})
export class DispenseComponent implements OnInit  {

  displayedColumns: string[] = ['select', 'id', 'name', 'progress', 'color', 'action'];
  displayedColumnsQueueList: string[] = ['beneficiaryUid', 'firstname', 'artStartDate',
  'age', 'gender' , 'preArtNumber' , 'artNumber' , 'action'];
  benificiaryQueueDatasource: MatTableDataSource<BeneficiaryQueue>;
  selection = new SelectionModel<BeneficiaryQueue>(true, []);
  user: any;
  assignedTo: any;
  beneficiaryList: BeneficiaryQueue[] = [];
  beneficiaryList1: any;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  userInfo: any;
  flag: boolean;
  dataSource:any;

  constructor( private beneficiaryservice: ProductDispenseApiService, private router: Router, private productDispenseService: ProductDispenseService) {
  }

  ngOnInit() {
    // this.dataSource.paginator = this.paginator;
    // this.dataSource.sort = this.sort;
    this.user = JSON.parse(localStorage.getItem('currentUser'));
    this.assignedTo = this.user.userId;
     this.getQueueList();
  }

  applyFilter(filterValue: string) {
    this.benificiaryQueueDatasource.filter = filterValue.trim().toLowerCase();

    if (this.benificiaryQueueDatasource.paginator) {
      this.benificiaryQueueDatasource.paginator.firstPage();
    }
  }
  getQueueList() {
     this.beneficiaryservice.getBeneficiaryDetails(this.assignedTo)
    .subscribe((response) => {
      this.beneficiaryList = response;
      console.log(response);
      this.benificiaryQueueDatasource = new MatTableDataSource(this.beneficiaryList);
    }, (error) => {
      console.log(error);
    });
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.benificiaryQueueDatasource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.benificiaryQueueDatasource.data.forEach(row => this.selection.select(row));
  }

  dispense(row: BeneficiaryQueue, group: MatTabGroup) {
    // this.flag=true;
    this.productDispenseService.setBeneficiary(row);
    group.selectedIndex = 2;
    this.router.navigateByUrl('art/beneficiary/dispense/beneficiary-dispense');
  }

  // /** The label for the checkbox on the passed row */
  // checkboxLabel(row?: UserData): string {
  //   if (!row) {
  //     return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
  //   }
  //   return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
  // }

//}

// function createNewUser(id: number): UserData {
//   const name = NAMES[Math.round(Math.random() * (NAMES.length - 1))] + ' ' +
//       NAMES[Math.round(Math.random() * (NAMES.length - 1))].charAt(0) + '.';

//   return {
//     id: id.toString(),
//     name: name,
//     progress: Math.round(Math.random() * 100).toString(),
//     color: COLORS[Math.round(Math.random() * (COLORS.length - 1))],
//     position: 0
//   };
 }
