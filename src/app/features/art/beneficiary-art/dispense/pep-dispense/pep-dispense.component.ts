import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { faSearch, faEdit, faTrash, faEye, faAngleDown } from '@fortawesome/free-solid-svg-icons';
import { User } from '../../../../../global-module/Interfaces/userInterface';
import { UserApiService } from '@global/services/User/user-api.service';
import { UserService } from '@global/services/User/user.service';
import { Router } from '@angular/router';
import { PepDispenseService } from '@global/services/pep-dispense.service';
import Swal from 'sweetalert2';
import { ProductDispenseService } from '@global/services/productDispense/product-dispense.service';
import { BeneficiaryQueue } from '@global/Interfaces/beneficiaryQueueInteface';

@Component({
  selector: 'app-pep-dispense',
  templateUrl: './pep-dispense.component.html',
  styleUrls: ['./pep-dispense.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class PepDispenseComponent implements OnInit {
  user_list: User[];
  cols: any[];
  searchIcon = faSearch;
  edit_icon = faEdit;
  trash_icon = faTrash;
  view_icon = faEye;
  down_arrow = faAngleDown;
  totalRecords: any;
  public popoverTitle: string = '';
  public popoverMessage: string = 'Do you want to delete user?';
  public confirmClicked: boolean = false;
  public cancelClicked: boolean = false;
  benificiaryData = new BeneficiaryQueue();


  constructor(private _userApiService: UserApiService,
    private _pepService: PepDispenseService,
    private _userService: UserService,
    private router: Router,
    private productDispenseService: ProductDispenseService) { }

  ngOnInit() {
    this.getUsers();
    this.cols = [
      { field: 'firstname', header: 'Name' },
      { field: 'userName', header: 'User Name' },
      { field: 'email', header: 'Email' },
      { field: 'roleDto', header: 'Role' },
      { field: 'mobileNumber', header: 'Mobile' },
      { field: 'isActive', header: 'Status' },
    ];
  }

  toggleDetails(user) {
    console.log('asdfsdf', JSON.stringify(user));
  }
  getUsers(): void {
    this._userApiService.getAllUsers().subscribe((Data) => {
      this.user_list = Data;
      this.totalRecords = this.user_list.length;
    }, (error) => {
      console.log(error);
    }
    );
  }
  dispenseUser(user_row: User) {
    console.log(user_row);
    this._pepService.dispenseUser(user_row)
      .subscribe((response) => {
        this.productDispenseService.setBeneficiary(response);
        this.router.navigateByUrl('art/beneficiary/dispense/beneficiary-dispense');
      }, (error) => {
        console.log(error);
      });
  }


}
