import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PepDispenseComponent } from './pep-dispense.component';

describe('PepDispenseComponent', () => {
  let component: PepDispenseComponent;
  let fixture: ComponentFixture<PepDispenseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PepDispenseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PepDispenseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
