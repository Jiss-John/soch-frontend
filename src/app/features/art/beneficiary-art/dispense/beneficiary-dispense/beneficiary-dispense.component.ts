import { Component, OnInit, AfterViewInit } from '@angular/core';
import { BeneficiaryProductService } from '../../beneficiary-product.service';
import { ProductDispense } from '@global/Interfaces/productAddDispense';
import { ProductDispenseApiService } from '@global/services/productDispense/product-dispense-api.service';
import { DatePipe } from '@angular/common';
import Swal from 'sweetalert2';
import { ProductDispenseService } from '@global/services/productDispense/product-dispense.service';
@Component({
  selector: 'app-beneficiary-dispense',
  templateUrl: './beneficiary-dispense.component.html',
  styleUrls: ['./beneficiary-dispense.component.css']
})
export class BeneficiaryDispenseComponent implements OnInit, AfterViewInit {
  product: any = {};
  productList: any[] = [];
  filterProductsList: any[];
  date = new Date().toDateString();
  error: any;
  dispenseQty: any;
 products = [];
  divs: number[] = [];
  productObj = new ProductDispense();
  userInfo: any;
  beneficiaryObj: any;
  constructor(private beneficiaryProductService: BeneficiaryProductService,
     private productdispenseapiService: ProductDispenseApiService,
     public datepipe: DatePipe, private productDispenseService: ProductDispenseService) {
   }

 ngOnInit() {
  this.beneficiaryObj = this.productDispenseService.getBeneficiary();
  console.log(this.beneficiaryObj);
  }

  ngAfterViewInit(){

  }

  filterProducts(event) {
    const query = event.query;
    this.beneficiaryProductService.getProductDetails().then(products => {
        this.products = products;
        this.filterProductsList = this.getProducts(query, products);
    });
}

getProducts(query, products: any[]): any[] {
  const filtered: any[] = [];
  for (let i = 0; i < products.length; i++) {
      const prdct = products[i];
      if (prdct.product.toLowerCase().indexOf(query.toLowerCase()) == 0) {
          filtered.push(prdct);
      }
  }
  return filtered;
}
createDiv(): void {
  this.divs.push(this.divs.length);
}
add(value: any , index: any){
  this.productList[index].dispenseQty = value;
  //this.productList[y].push({"dispenseQty":x});

}

deleteProduct(product) {
 const indexToDelete = this.productList.indexOf(product);
 if (indexToDelete !== -1) {
   this.productList.splice(indexToDelete, 1);
   this.divs.pop();
 }
}
onSubmit() {
this.userInfo = JSON.parse(localStorage.getItem('currentUser'));
this.productObj.dispenseDate = this.datepipe.transform(this.date, 'dd/MM/yyyy');
this.productObj.dispensedTo = '';
this.productObj.facilityId = this.userInfo.facilityId;
this.productObj.beneficiaryId = this.beneficiaryObj.beneficiaryId;
this.productObj.facProdStockTransId = 1;
   this.productList.forEach(element =>
   element.regimenId =  this.beneficiaryObj.regimenId 
   );
this.productObj.dispensationItemDtoList = this.productList;
this.productdispenseapiService.dispenseProduct(this.productObj)
        .subscribe((response) => {
          Swal.fire('', 'Successfully saved changes!', 'success');
        }, (error) => {
          console.log(error);
         // this.error = error.error.details[0].description;
          // this.duplicateCheck=this.error;
        });
}

Cancel(){

}
}




