import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BeneficiaryDispenseComponent } from './beneficiary-dispense.component';

describe('BeneficiaryDispenseComponent', () => {
  let component: BeneficiaryDispenseComponent;
  let fixture: ComponentFixture<BeneficiaryDispenseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BeneficiaryDispenseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BeneficiaryDispenseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
