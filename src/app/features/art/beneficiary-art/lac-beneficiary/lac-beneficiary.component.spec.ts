import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LacBeneficiaryComponent } from './lac-beneficiary.component';

describe('LacBeneficiaryComponent', () => {
  let component: LacBeneficiaryComponent;
  let fixture: ComponentFixture<LacBeneficiaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LacBeneficiaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LacBeneficiaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
