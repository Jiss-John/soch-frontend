
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
  })
export class BeneficiaryProductService {

    constructor(private http: HttpClient) { }

    getProductDetails() {
    return this.http.get<any>('assets/beneficiary-product.json')
      .toPromise()
      .then(res => <any[]>res)
      .then(data => data);
    }
  
    }
