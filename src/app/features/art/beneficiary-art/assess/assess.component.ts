import { Component, OnInit } from '@angular/core';
import { faSearch, faEdit, faTrash, faEye } from '@fortawesome/free-solid-svg-icons';
import { Router } from '@angular/router';
import { BeneficiaryTI } from '@global/interfaces/beneficiaryTIRegistrationInterface';
import { BeneficiaryTIApiService } from '@global/services/TI-Beneficiary/beneficiary-ti-api.service';
import { BeneficiaryTIService } from '@global/services/TI-Beneficiary/beneficiary-ti.service';

@Component({
  selector: 'app-assess',
  templateUrl: './assess.component.html',
  styleUrls: ['./assess.component.css']
})
export class AssessComponent implements OnInit {

  beneficiaryTI_list: BeneficiaryTI[];
  cols: any[];
  searchIcon = faSearch;
  edit_icon = faEdit;
  trash_icon = faTrash;
  view_icon = faEye;
  totalRecords: any;
  public popoverTitle: string = '';
  public popoverMessage: string = 'Do you want to delete beneficiary?';
  public confirmClicked: boolean = false;
  public cancelClicked: boolean = false;
  userInfo: any;

  constructor(private _beneficiaryTIApiService: BeneficiaryTIApiService, private _beneficiaryTIService: BeneficiaryTIService, private router: Router) { }

  ngOnInit() {

    this.getTIBeneficiaries();

    this.cols = [
      { field: 'uidNumber', header: 'Beneficiary Id', width: '15%' },
      { field: 'firstName', header: 'First Name', width: '10%' },
      { field: 'lastName', header: 'Last Name', width: '10%' },
      { field: 'age', header: 'Age  ', width: '10%' },
      { field: 'mobileNumber', header: 'Mobile No:', width: '10%' },
      { field: 'gender', header: 'Gender', width: '10%' },
      { field: 'dateOfRegistration', header: 'Date Of Reg:', width: '20%' },
      { field: 'clientStatus', header: 'Current Status', width: '15%' },
      { field: 'beneficiaryActiveStatus', header: 'Beneficiary Active Status', width: '15%' },
    ];
  }


  getTIBeneficiaries(): void {
    this._beneficiaryTIApiService.getAllBeneficiariesTI().subscribe((Data) => {
      this.beneficiaryTI_list = Data;
      this.totalRecords = this.beneficiaryTI_list.length;
    }, (error) => {
      console.log(error);
    }
    );

  }


  disableBeneficiaryTI(beneficiary_row: BeneficiaryTI): void {
    this.userInfo = JSON.parse(localStorage.getItem('currentUser'));
    beneficiary_row.facilityId = this.userInfo ? this.userInfo.facilityId : 0;
    beneficiary_row.userId = this.userInfo ? this.userInfo.userId : 0;
    this._beneficiaryTIApiService.disableBeneficiaryTI(beneficiary_row).subscribe((response) => {
      this.getTIBeneficiaries();
    }, (error) => {
      console.log(error);
    });

  }


  artAssessment(beneficiary_row: BeneficiaryTI) {
    this._beneficiaryTIService.setBeneficiaryTI(beneficiary_row);
    this.router.navigateByUrl('/art/beneficiary/assessment');
  }

  addBeneficiaryTi() {
    let beneficiaryti: BeneficiaryTI;
    this._beneficiaryTIService.setBeneficiaryTI(beneficiaryti);
    this.router.navigateByUrl('/ti/beneficiary/add');
  }

}
