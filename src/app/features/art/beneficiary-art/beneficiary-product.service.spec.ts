import { TestBed } from '@angular/core/testing';

import { BeneficiaryProductService } from './beneficiary-product.service';

describe('BeneficiaryProductService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BeneficiaryProductService = TestBed.get(BeneficiaryProductService);
    expect(service).toBeTruthy();
  });
});
