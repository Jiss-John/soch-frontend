import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup,ReactiveFormsModule, FormControl } from '@angular/forms';
import {MatDatepicker} from '@angular/material';
import { DatePipe } from '@angular/common';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { BeneficiaryARTAssessment } from '@global/Interfaces/beneficiaryARTAssessment';

@Component({
  selector: 'app-assessment',
  templateUrl: './assessment.component.html',
  styleUrls: ['./assessment.component.css']
})
export class AssessmentComponent implements OnInit {
  myForm: FormGroup;
  submitted = false; 
  beneficiaryARTAssessment = new BeneficiaryARTAssessment();
  dropdownList=[];
  contraceptionList = [];
  purposesList = [];
  vitaminNineMon = false;

  
  dropdownSettings: IDropdownSettings;
  ///assesmentFormGroupassesmentFormGroup:FormGroup;
  constructor(private formBuilder: FormBuilder, private datePipe: DatePipe,) { }

  ngOnInit() {
    this.artBeneficiary();
    this.myForm.patchValue({
      assessmentDate: this.datePipe.transform(Date.now(), 'yyyy-MM-dd'),
      dateTransferredIn:this.datePipe.transform(Date.now(), 'yyyy-MM-dd'),
      patientCorrespondingDate: this.datePipe.transform(Date.now(), 'yyyy-MM-dd'),
      patientOptOutCorrespondingDate: this.datePipe.transform(Date.now(), 'yyyy-MM-dd'),
      patientOnMedicalAdviceCorrespondingDate: this.datePipe.transform(Date.now(), 'yyyy-MM-dd'),
      patientLastVisitCorrespondingDate:this.datePipe.transform(Date.now(), 'yyyy-MM-dd'),
      cptDateInitiated: this.datePipe.transform(Date.now(), 'yyyy-MM-dd'),
      dateNgoCareAndSupport: this.datePipe.transform(Date.now(), 'yyyy-MM-dd'),
      immuneAgeBirth: this.datePipe.transform(Date.now(), 'yyyy-MM-dd'),
      immuneSixWeeks: this.datePipe.transform(Date.now(), 'yyyy-MM-dd'),
      immuneTenWeeks: this.datePipe.transform(Date.now(), 'yyyy-MM-dd'),
      immuneFourteenWeeks: this.datePipe.transform(Date.now(), 'yyyy-MM-dd'),
      immuneThreeMonth: this.datePipe.transform(Date.now(), 'yyyy-MM-dd'),
      immuneSixteenMonth: this.datePipe.transform(Date.now(), 'yyyy-MM-dd'),
      immuneFiveYears: this.datePipe.transform(Date.now(), 'yyyy-MM-dd'),
      immuneTenYears: this.datePipe.transform(Date.now(), 'yyyy-MM-dd'),
      immuneFifteenYears: this.datePipe.transform(Date.now(), 'yyyy-MM-dd'),
      vitaminDateNine: this.datePipe.transform(Date.now(), 'yyyy-MM-dd'),
      vitaminDateEighteen: this.datePipe.transform(Date.now(), 'yyyy-MM-dd'),
      vitaminDateTwentyFour: this.datePipe.transform(Date.now(), 'yyyy-MM-dd'),
      vitaminDateThirty: this.datePipe.transform(Date.now(), 'yyyy-MM-dd'),
      vitaminDateThirtySix: this.datePipe.transform(Date.now(), 'yyyy-MM-dd'),
      vitaminDateFourtyTwo: this.datePipe.transform(Date.now(), 'yyyy-MM-dd'),
      vitaminDateFourtyEight: this.datePipe.transform(Date.now(), 'yyyy-MM-dd'),
      vitaminDateFiftyFour: this.datePipe.transform(Date.now(), 'yyyy-MM-dd'),
      vitaminSixtyMon: this.datePipe.transform(Date.now(), 'yyyy-MM-dd'),
      vitaminDateSixty: this.datePipe.transform(Date.now(), 'yyyy-MM-dd'),
      dateNextVisit: this.datePipe.transform(Date.now(), 'yyyy-MM-dd'),
      dateOfLinkingLac: this.datePipe.transform(Date.now(), 'yyyy-MM-dd'),
      nextVisitDate: this.datePipe.transform(Date.now(), 'yyyy-MM-dd'),
    });
    this.dropdownList = [
      {
        id_item: 1,
      name: "Diabetes",
      
    },{
      id_item: 2,
      name: "Hypertension",
      
    },{
      id_item: 3,
      name: "cardiovascular disease",
      
    },{
      id_item: 4,
      name: "STI ",
      
    }]
    this.contraceptionList = [
      {
        id_item: 1,
        name : "Condoms"
      },
      {
        id_item: 2,
        name: "Oral contraceptives"
      },
      {
        id_item: 3,
        name: "IUD"
      },
      {
        id_item: 4,
        name: "Tubal Ligation"
      },
      {
        id_item: 5,
        name: "Vasectomy"
      },
      {
        id_item: 6,
        name: "None"
      }
    ]
    this.purposesList = [
      {
        id_item: 1,
        name: "Adherence"
      },
      {
        id_item: 2,
        name: "Retention"
      },
      {
        id_item: 3,
        name: "Psychosocial support"
      },
      {
        id_item: 4,
        name: "Others"
      }
    ]

    this.dropdownSettings = {
      singleSelection: false,
       idField: 'id_item',
      textField: 'name',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 4,
      allowSearchFilter: true
    };
      
    }
  
  

  artBeneficiary() {
    this.myForm = this.formBuilder.group({
      beneficiaryName: ['', [Validators.required, Validators.maxLength(99)]],
      beneficiaryUId: ['', Validators.required],
      hivCareRegistrationNumber: ['', Validators.required],
      artRegistrationNumber: ['', Validators.required],
      assessmentDate: [''],
      nameOfLac:  ['', [Validators.required, Validators.maxLength(99)]],
      LacRegistrationNumber: ['', Validators.required],
      treatmentStatusAtRegistration: ['', Validators.required],
      artCentreCode: ['', Validators.required],
      artCentreAddress: ['', Validators.required],
      gender: ['', Validators.required],
      age: ['', Validators.required],
      dob: ['', Validators.required],
      category: ['', Validators.required],
      beneficiaryPhoneNumber: ['', Validators.required],
      beneficiaryAddress:['', Validators.required],
      beneficiaryCity:['', Validators.required],
      beneficiaryTaluk:['', Validators.required],
      beneficiaryDistrict: ['', Validators.required],
      beneficiaryState:['', Validators.required],
      caregiversName: ['', Validators.required],
      caregiversAddress: ['', Validators.required],
      entryPoint: ['', Validators.required],
      beneficiaryTransferredFrom: ['', Validators.required],
      dateTransferredIn: [''],
      nameOfPreviousClinic: ['', Validators.required],
      riskFactorHiv:  ['', [Validators.required, Validators.maxLength(99)]],
      iduSubstitutionTherapy:[''],
      education: ['', Validators.required],
      occupationalStatus: ['', Validators.required],
      maritalStatus: ['', Validators.required],
      monthlyIncome: ['', Validators.required],
      familyId: ['', Validators.required],
      familyMemberUid:  ['', Validators.required],
      relationToPatient: ['', Validators.required],
      parentAge: ['', Validators.required],
      parentSex: ['', Validators.required],
      parentAlive: ['', Validators.required],
      hivStatus: ['', Validators.required],
      parentOnArt: ['', Validators.required],
      parentHivCare: ['', Validators.required],
      patientDied:  ['', Validators.required],
      patientCorrespondingDate: [''],
      patientOptOut: ['', Validators.required],
      patientOptOutCorrespondingDate: [''],
      onMedicalAdvice: ['', Validators.required],
      patientOnMedicalAdviceCorrespondingDate: [''],
      lostFollowUp: ['', Validators.required],
      patientLastVisitCorrespondingDate: [''],
      habitAlcoholUse: ['', Validators.required],
      habitSmoking: ['', Validators.required],
      tobaccoUse:  ['', Validators.required],
      patientHbv: ['', Validators.required],
      patientHcv: ['', Validators.required],
      hbvStatus: ['', Validators.required],
      hcvStatus: ['', Validators.required],
      otherAilments: ['', Validators.required],
      detailOtherAilments: ['', Validators.required],
      otherExistingConditions:  ['', Validators.required],
      medicationOtherThanArt: ['', Validators.required],
      cptDateInitiated: ['', Validators.required],
      drugAllergy: ['', Validators.required],
      contraception: ['', Validators.required],
      obstetricHistory: ['', Validators.required],
      lastMenstrualPeriod: [''],
      pregnantNow: ['', Validators.required],
      referredPptct: ['', Validators.required],
      deliveryOutcome:['', Validators.required], 
      papSmear: ['', Validators.required],
      otherRemarks: ['', Validators.required],
      dateNgoCareAndSupport: ['', Validators.required],
      instituteNameNgo: ['', Validators.required],
      purposesLinkageNgo: ['', Validators.required],
      stayingWith: ['', Validators.required],
      guardianCaregiver: ['', Validators.required],
      guardianSex: ['', Validators.required],
      guardianAge: ['', Validators.required],
      guardianEducation: ['', Validators.required],
      birthHistory: ['', Validators.required],
      birthWeight: ['', Validators.required],
      neonatalComplications: ['', Validators.required],
      infantFeeding: ['', Validators.required],
      forBreastFeed: ['', Validators.required],
      firstDbs: [''],
      secondDbs: [''],
      thirdWbb: [''],
      fourthAntibodyTest: [''],
      milestoneAchieved: [''],
      immuneAgeBirth: [''],
      immuneSixWeeks: [''],
      immuneTenWeeks: [''],
      immuneFourteenWeeks: [''],
      immuneThreeMonth: [''],
      immuneFiveYears: [''],
      immuneSixteenMonth: [''],
      immuneTenYears: [''],
      immuneFifteenYears: [''],
      vitaminNineMon: [''],
      vitaminDateNine: [''],
      vitaminEighteenMon: [''],
      vitaminDateEighteen: [''],
      vitaminTwentyFourMon: [''],
      vitaminDateTwentyFour: [''],
      vitaminThirtyMon: [''],
      vitaminDateThirty: [''],
      vitaminThirtySixMon: [''],
      vitaminDateThirtySix: [''],
      vitaminFourtyTwoMon: [''],
      vitaminDateFourtyTwo: [''],
      vitaminFourtyEightMon: [''],
      vitaminDateFourtyEight: [''],
      vitaminFiftyFourMon: [''],
      vitaminDateFiftyFour: [''],
      vitaminSixtyMon: [''],
      vitaminDateSixty: [''],
      dateNextVisit: [''],
      tbTreatment: [''],
      pregnancy: [''],
      condomsGiven: [''],
      counsellingNotes: [''],
      beneficiaryAccountName: [''],
      beneficiaryAccountNumber: [''],
      beneficiaryBankIfsc: [''],
      cancelledCheque: [''],
      remarks: [''],
      dateOfLinkingLac: [''],
      nextVisitDate: [''],
      chooseNextInQueue: [''],
      chooseUserInQueue: [''],










    })

  }
  // date = new FormControl(new Date());
  // serializedDate = new FormControl((new Date()).toISOString()); 

  
   get beneficiaryArt() { return this.myForm.controls; }
  saveArtAssessment() {
    this.submitted = true;
     if (this.myForm.invalid) {
    //  this.beneficiaryARTAssessment = this.myForm.value;
    //  console.log(this.beneficiaryARTAssessment)
    //    return;
    }

  }
  viewWorkload(){

  }
  onReset(){

  }
  onCancel(){

  }
}
