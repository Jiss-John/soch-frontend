import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableModule } from 'primeng/table';
import { RadioButtonModule } from 'primeng/radiobutton';
import { BeneficiaryArtRoutingModule } from './beneficiary-art-routing.module';
import { TransferInComponent } from './transfer-in/transfer-in.component';
import { BeneficiaryArtComponent } from './beneficiary-art.component';
import { TransferOutComponent } from './transfer-out/transfer-out.component';
import { LacBeneficiaryComponent } from './lac-beneficiary/lac-beneficiary.component';
import { DispenseComponent } from './dispense/dispense.component';
import { ReferralComponent } from './referral/referral.component';
import { MatTableModule, MatPaginatorModule,
         MatSortModule, MatCheckboxModule,
         MatMenuModule, MatIconModule, MatTabsModule, MatDatepickerModule, MatNativeDateModule, MatDialogModule, MatFormFieldModule, MatInputModule } from '@angular/material';
import { ReactiveFormsModule, FormsModule  } from '@angular/forms';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ConfirmationPopoverModule } from 'angular-confirmation-popover';
import { ArtBeneficiaryComponent } from './list/list.component';
import { AssessComponent } from "./assess/assess.component";
import { AssessmentComponent } from './assessment/assessment.component';

import {AutoCompleteModule} from 'primeng/autocomplete';
import { HttpClientModule } from '@angular/common/http';
import { BeneficiaryDispenseComponent } from './dispense/beneficiary-dispense/beneficiary-dispense.component';
import { PepDispenseComponent } from './dispense/pep-dispense/pep-dispense.component';


@NgModule({
  declarations: [
    TransferInComponent,
    TransferOutComponent,
    LacBeneficiaryComponent,
    DispenseComponent,
    ReferralComponent,
    ArtBeneficiaryComponent,
    AssessComponent,
    BeneficiaryArtComponent,
    AssessmentComponent,
    BeneficiaryDispenseComponent,
    PepDispenseComponent
  ],
  imports: [
    CommonModule,
    BeneficiaryArtRoutingModule,
    MatTabsModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatCheckboxModule,
    MatMenuModule,
    MatIconModule,
    BeneficiaryArtRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    TableModule,
    RadioButtonModule,
    NgMultiSelectDropDownModule,
    FontAwesomeModule,
    ConfirmationPopoverModule,
    AutoCompleteModule,
    HttpClientModule
  ]
})
export class BeneficiaryArtModule { }
