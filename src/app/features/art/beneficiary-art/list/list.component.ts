import { Component, OnInit } from '@angular/core';
import { faSearch, faEdit, faTrash, faEye } from '@fortawesome/free-solid-svg-icons';
import { Router } from '@angular/router';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { artBeneficiary } from '@global/interfaces/artBeneficiary';
import { BeneficiaryjsonService } from './beneficiaryjson.service';
import { BeneficiaryArtApiService } from '@global/services/Art-Beneficiary/beneficiary-art-api.service';

@Component({
  selector: 'app-art-beneficiary-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ArtBeneficiaryComponent implements OnInit {
  constructor(private _beneficiaryArtApiService: BeneficiaryArtApiService,
              private router: Router,
              private _jsonservice: BeneficiaryjsonService) { }

  beneficiaryArt_list: artBeneficiary[];
  cols: any[];
  searchIcon = faSearch;
  edit_icon = faEdit;
  trash_icon = faTrash;
  view_icon = faEye;
  totalRecords: any;
  public popoverTitle: string = '';
  public popoverMessage: string = 'Do you want to delete beneficiary?';
  public confirmClicked: boolean = false;
  public cancelClicked: boolean = false;
  userInfo: any;
  val2: string = 'Name';
  selectedItemsForFilter = [];
   filterItems = [
    { item_id: 1, item_text: 'UID' },
    { item_id: 2, item_text: 'Mobile Number' },
    { item_id: 3, item_text: 'Beneficiary Status' },
    { item_id: 4, item_text: 'Location' }
  ];
  dropdownSettings: IDropdownSettings;
  filterShow: boolean = false;
  userobj = localStorage.getItem('currentUser') ? localStorage.getItem('currentUser') : '';
  userid: number;
  artBenJSON = [];

  ngOnInit() {
    if (this.userobj !== '') {
      this.userid = JSON.parse(this.userobj).userId;
      this.getArtBeneficiaries(this.userid);
    }
    this._jsonservice.getjson().subscribe( (data) => {
      console.log(data);
    });
    this.selectedItemsForFilter = [];
    this.cols = [
      { field: 'id', header: 'Serial No', width: 'auto' },
      { field: 'beneficiaryUid', header: 'UID No', width: 'auto' },
      { field: 'beneficiaryName', header: 'Beneficiary Name', width: 'auto' },
      { field: 'age', header: 'Age  ', width: 'auto' },
      { field: 'gender', header: 'Gender', width: 'auto' },
      { field: 'contactNumber', header: 'Contact No:', width: 'auto' },
      { field: 'preArtNo', header: 'Pre Art No', width: 'auto' },
      { field: 'artNo', header: 'Art No', width: 'auto' },
      { field: 'beneficiaryStatus', header: 'Status', width: 'auto' },
    ];
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'item_id',
      textField: 'item_text',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 2,
      enableCheckAll: false,
      allowSearchFilter: true
    };
  }

  toggleFilter() {
    this.filterShow = !this.filterShow;
  }
  artBeneficiaryQueueFilter(dt, searchinput) {
    dt.filterGlobal(searchinput.value, 'id', 'in');
  }

  selectedValueFromRadioButton(){
    console.log(this.val2);
  }
  onItemSelect(ev){
    console.log(ev);
  }
  clearInput() {
    this.selectedItemsForFilter = [];
  }
  getArtBeneficiaries(userId): void {
    this._beneficiaryArtApiService.getAllBeneficiariesArt(userId).subscribe((Data) => {
      this.beneficiaryArt_list = Data;
      this.totalRecords = this.beneficiaryArt_list.length;
    }, (error) => {
      console.log(error);
    }
    );
  }

 dispense() {
    this.router.navigateByUrl('/art/beneficiary/dispense');
  }
}
