import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArtBeneficiaryComponent } from './list.component';

describe('ListComponent', () => {
  let component: ArtBeneficiaryComponent;
  let fixture: ComponentFixture<ArtBeneficiaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArtBeneficiaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArtBeneficiaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
