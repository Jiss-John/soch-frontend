import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BeneficiaryjsonService {

  constructor(private httpClient: HttpClient) { }
  getjson() {
    return this.httpClient.get('assets/json/todaydispensation.json');
  }
}
