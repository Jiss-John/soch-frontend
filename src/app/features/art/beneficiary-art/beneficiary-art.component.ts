import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-beneficiary-art',
  templateUrl: './beneficiary-art.component.html',
  styleUrls: ['./beneficiary-art.component.css']
})
export class BeneficiaryArtComponent implements OnInit {

  links = [
    { label: 'Beneficiary List', route: 'list' },
    { label: 'Transfer In', route: 'transfer-in' },
    { label: 'Transfer Out', route: 'transfer-out' },
    { label: 'LAC Beneficiary', route: 'lac-beneficiary' },
    { label: 'Dispense', route: 'dispense' },
    { label: 'Assessment', route:'assess'},
    { label: 'Refferal', route: 'refferal' }
  ];
  activeLink = this.links[0];

  constructor() { }

  ngOnInit() {
  }

}
