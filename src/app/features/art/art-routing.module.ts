import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '@global/services/auth.guard';
import { ArtComponent } from './art.component';
import { LabTestComponent } from './lab-test/lab-test.component';


const mainChildRoutes: Routes = [
  { redirectTo: 'beneficiary', path: '', pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'beneficiary', loadChildren: () => import('./beneficiary-art/beneficiary-art.module').then(m => m.BeneficiaryArtModule)},
  { path: 'lab', loadChildren: () => import('./lab-test/lab-test.module').then(m => m.LabTestModule)}
];


const mainRoutes: Routes = [
  // { redirectTo: '', path: '', pathMatch: 'full' },
  { path: '', component: ArtComponent, children: mainChildRoutes }
];

@NgModule({
  imports: [RouterModule.forChild(mainRoutes)],
  exports: [RouterModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ArtRoutingModule { }
