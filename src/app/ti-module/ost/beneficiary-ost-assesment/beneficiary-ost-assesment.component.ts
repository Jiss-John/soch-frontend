import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';
import { BeneficiaryRVAssessment } from '@global/interfaces/beneficiaryRVAssessment';
import { Pattern } from '@shared';
import { BeneficiaryTI } from '@global/interfaces/beneficiaryTIRegistrationInterface';
import { BeneficiaryApiService } from '@global/services/TI-Beneficiary/beneficiary-api.service';
import { StorageMap, LocalStorage } from '@ngx-pwa/local-storage';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'beneficiary-ost-assesment',
  templateUrl: './beneficiary-ost-assesment.component.html',
  styleUrls: ['./beneficiary-ost-assesment.component.css']
})
export class BeneficiaryOSTAssesmentComponent implements OnInit {

  R_V_AssesmentForm: FormGroup;
  submitted = false;
  beneficiaryRVAssessment = new BeneficiaryRVAssessment();
  pattern = new Pattern();
  searchValue = new FormControl();
  beneficiary = new BeneficiaryTI();
  hrgPrimaryCategory: string;
  hrgSecondaryCategory: string;
  beneficiaryId: any;
  title: string;
  userInfo: any;


  constructor(private router: Router,
    private datePipe: DatePipe,
    private formBuilder: FormBuilder,
    private beneficiaryApiService: BeneficiaryApiService,

    private route: ActivatedRoute,
    private localStorage: StorageMap
  ) {
    this.beneficiaryId = this.route.snapshot.paramMap.get('uid');

  }

  ngOnInit() {

    this.userInfo = JSON.parse(localStorage.getItem('currentUser'));
    this.title =  this.userInfo && this.userInfo.facilityTypeId == 10 ? "OST": "R & V";

    this.rvBeneficiary();
    this.R_V_AssesmentForm.patchValue({
      dueDateOfAssessment: this.datePipe.transform(Date.now(), 'yyyy-MM-dd'),
      assessmentDate: this.datePipe.transform(Date.now(), 'yyyy-MM-dd')
    });

    this.localStorage.get("ti_beneficiary_list").subscribe((list: any) => {

      let beneficiaryList = list ? JSON.parse(list) : [];

      console.log("### createOfflineBeneficiary", beneficiaryList);

      let filterBeneficiary = beneficiaryList.filter(o => {
        return o['uidNumber'] == this.beneficiaryId
      });

      this.beneficiary = filterBeneficiary.length > 0 ? filterBeneficiary[0] : null;
      console.log("### 2 createOfflineBeneficiary", this.beneficiary);
      this.rvBeneficiary();
    });
  }

  rvBeneficiary() {
    this.R_V_AssesmentForm = this.formBuilder.group({
      beneficiaryName: [this.beneficiary.firstName + ' ' + this.beneficiary.lastName, [Validators.required, Validators.maxLength(99)]],
      mobileNumber: [this.beneficiary.mobileNumber, [Validators.required, Validators.pattern(this.pattern.mobilePattern)]],
      beneficiaryUId: [this.beneficiaryId],
      beneficiaryId: [this.beneficiary.id],
      assessmentDate: [''],
      beneficiaryActivityStatus: [this.beneficiary.beneficiaryActivityStatus, [Validators.required]],

      highNumberOfSexualEncounters: ['', [Validators.required]],
      condomRequirementPerWeekTi: ['', [Validators.required]],
      firstYearInSexWorkAndBelowAgeOf25Years: ['', [Validators.required]],
      stiReportedInLastThreeMonthsTi: ['', [Validators.required]],
      alcohol: ['', [Validators.required]],
      unsafeSex: ['', [Validators.required]],
      violence: ['', [Validators.required]],
      lowCondomUse: ['', [Validators.required]],

      highNumberOfDrugUsingPartners: ['', [Validators.required]],
      sharingOfNeedlesSyringes: ['', [Validators.required]],
      injectingGreaterThanThreeTimes: ['', [Validators.required]],
      stiReportedInLastThreeMonthsIdu: ['', [Validators.required]],
      useOfAlcoholAndOtherDrugsApartFromInjections: ['', [Validators.required]],
      unsafeSexSexWithNonRegularPartner: ['', [Validators.required]],
      mobilityFromOneHotspotToAnother: ['', [Validators.required]],
      condomRequirementPerWeekIdu: ['', [Validators.required]],
      needlesSyringesRequiremenPerWeek: ['', [Validators.required]],

      
      // ostCode: ['']
    }
    );
  }

  get beneficiaryRV() { return this.R_V_AssesmentForm.controls; }

  //For searching based on UID (Beneficiary Id) and Mobile Number
  searchBeneficiary() {
    console.log(this.searchValue.value)
    this.beneficiaryApiService.getBeneficiaryBysearchValue(this.searchValue.value).subscribe((Data) => {
      this.beneficiary = Data;
      this.beneficiaryId = this.beneficiary.id;
      //This is used to display values in text fields (using formControlName) without ngModel- donot remove this
      this.R_V_AssesmentForm.patchValue({
        //assigning values to formControl variable from the object that is subscribed value from api call.
        beneficiaryName: this.beneficiary.firstName,
        beneficiaryUId: this.beneficiary.uidNumber,
        beneficiaryId: this.beneficiary.id,
        mobileNumber: this.beneficiary.mobileNumber,
        beneficiaryActivityStatus: this.beneficiary.beneficiaryActivityStatus,

      });
      this.hrgPrimaryCategory = this.beneficiary.hrgPrimaryCategory;
      this.hrgSecondaryCategory = this.beneficiary.hrgSecondaryCategory;
      if (this.hrgPrimaryCategory == 'IDU' || this.hrgSecondaryCategory == 'IDU') {
        this.R_V_AssesmentForm.patchValue({
          highNumberOfSexualEncounters: 0,
          condomRequirementPerWeekTi: 0,
          firstYearInSexWorkAndBelowAgeOf25Years: 0,
          stiReportedInLastThreeMonthsTi: 0,
          alcohol: 0,
          unsafeSex: 0,
          violence: 0,
          lowCondomUse: 0
        });
      }
      else {
        this.R_V_AssesmentForm.patchValue({
          highNumberOfDrugUsingPartners: 0,
          sharingOfNeedlesSyringes: 0,
          injectingGreaterThanThreeTimes: 0,
          stiReportedInLastThreeMonthsIdu: 0,
          useOfAlcoholAndOtherDrugsApartFromInjections: 0,
          unsafeSexSexWithNonRegularPartner: 0,
          mobilityFromOneHotspotToAnother: 0,
          condomRequirementPerWeekIdu: 0,
          needlesSyringesRequiremenPerWeek: 0
        });
      }

    }, (error) => {
      Swal.fire(error.error.details[0].description + "  !use advanced search");
      this.onReset();
    }
    );
  }

  showErrorPopup(error) {
    console.log("####Service Error-:" + error);
    Swal.fire('', error, 'error');
  }

  // Function works on cancel button to navigate to benificiary list
  onCancel() {
    this.router.navigateByUrl("ti/beneficiary/list");
  }


  //For saving the value to DB
  saveRVAssesment() {
    this.submitted = true;

    this.saveOfflineBeneficiary()
    if (this.R_V_AssesmentForm.invalid) {
      this.beneficiaryRVAssessment = this.R_V_AssesmentForm.value;
      // console.log(this.beneficiaryRVAssessment)
      return;
    }
    else {
      this.beneficiaryRVAssessment = this.R_V_AssesmentForm.value;

      //  console.log(this.beneficiaryRVAssessment);
      this.beneficiaryApiService.saveRVassessmentDetails(this.beneficiaryRVAssessment)
        .subscribe((response) => {
          Swal.fire('', 'Successfully saved changes!', 'success');
          this.router.navigateByUrl("ti/beneficiary/list");
        }, (error) => {
          console.log(error);
        });
    }
  }
  /**
   * Saves offline beneficiary
   */
  saveOfflineBeneficiary() {
    this.beneficiary.beneficiaryActivityStatus = this.R_V_AssesmentForm.value.beneficiaryActivityStatus;
    let beneficiaryList;
    this.localStorage.get("ti_beneficiary_list").subscribe((list: any) => {
      beneficiaryList = list? JSON.parse(list) : [];
      beneficiaryList.forEach(o => {
        if (o.uidNumber === this.beneficiaryId) {
          o.beneficiaryActivityStatus = this.R_V_AssesmentForm.value.beneficiaryActivityStatus;
        }
      });
      // this.localStorage.delete('ti_beneficiary_list');
      this.localStorage.set("ti_beneficiary_list", JSON.stringify(beneficiaryList)).subscribe((x) => {
      });
      console.log('main', this.localStorage.get("ti_beneficiary_list"));
    });
  }
  //Method to reset the form
  onReset() {
    this.submitted = false;
    this.R_V_AssesmentForm.reset();
    this.hrgPrimaryCategory = null;
    this.hrgSecondaryCategory = null;
  }

}
