import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BeneficiaryOSTAssesmentComponent } from './beneficiary-ost-assesment.component';


describe('BeneficiaryOSTAssesmentComponent', () => {
  let component: BeneficiaryOSTAssesmentComponent;
  let fixture: ComponentFixture<BeneficiaryOSTAssesmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BeneficiaryOSTAssesmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BeneficiaryOSTAssesmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
