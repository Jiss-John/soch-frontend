import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BeneficiaryPrescribeComponent } from './beneficiary-prescribe.component';

describe('BeneficiaryPrescribeComponent', () => {
  let component: BeneficiaryPrescribeComponent;
  let fixture: ComponentFixture<BeneficiaryPrescribeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BeneficiaryPrescribeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BeneficiaryPrescribeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
