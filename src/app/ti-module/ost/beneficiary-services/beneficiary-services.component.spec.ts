import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BeneficiaryOSTServicesComponent } from './beneficiary-services.component';

describe('BeneficiaryOSTServicesComponent', () => {
  let component: BeneficiaryOSTServicesComponent;
  let fixture: ComponentFixture<BeneficiaryOSTServicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BeneficiaryOSTServicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BeneficiaryOSTServicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
