import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BeneficiaryFollowupComponent } from './beneficiary-followup.component';

describe('BeneficiaryFollowupComponent', () => {
  let component: BeneficiaryFollowupComponent;
  let fixture: ComponentFixture<BeneficiaryFollowupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BeneficiaryFollowupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BeneficiaryFollowupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
