import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DispensationTodayComponent } from './dispensation-today.component';

describe('DispensationTodayComponent', () => {
  let component: DispensationTodayComponent;
  let fixture: ComponentFixture<DispensationTodayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DispensationTodayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DispensationTodayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
