import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BeneficiaryDispensationComponent } from './beneficiary-dispensation.component';

describe('BeneficiaryDispensationComponent', () => {
  let component: BeneficiaryDispensationComponent;
  let fixture: ComponentFixture<BeneficiaryDispensationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BeneficiaryDispensationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BeneficiaryDispensationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
