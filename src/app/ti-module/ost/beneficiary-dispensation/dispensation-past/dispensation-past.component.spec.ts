import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DispensationPastComponent } from './dispensation-past.component';

describe('DispensationPastComponent', () => {
  let component: DispensationPastComponent;
  let fixture: ComponentFixture<DispensationPastComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DispensationPastComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DispensationPastComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
