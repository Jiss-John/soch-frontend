import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DispensationFutureComponent } from './dispensation-future.component';

describe('DispensationFutureComponent', () => {
  let component: DispensationFutureComponent;
  let fixture: ComponentFixture<DispensationFutureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DispensationFutureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DispensationFutureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
