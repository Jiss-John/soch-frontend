import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DispensationTransitComponent } from './dispensation-transit.component';

describe('DispensationTransitComponent', () => {
  let component: DispensationTransitComponent;
  let fixture: ComponentFixture<DispensationTransitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DispensationTransitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DispensationTransitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
