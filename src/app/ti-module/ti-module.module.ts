import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BeneficiaryRegistrationComponent } from './ngo/beneficiary-registration/beneficiary-registration.component';
import { BeneficiaryListComponent } from './ngo/beneficiary-list/beneficiary-list.component';
import { BeneficiaryScreeningComponent } from './ngo/beneficiary-screening/beneficiary-screening.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MatDatepickerModule, MatNativeDateModule, MatDialogModule, MatCheckboxModule, MatFormFieldModule, MatInputModule, MatTabsModule, MatTableDataSource, MatTableModule, MatPaginator, MatSort, MatPaginatorModule, MatSortModule, MatMenuModule, MatIconModule } from '@angular/material';
import { TableModule } from 'primeng/table';
import { RadioButtonModule } from 'primeng/radiobutton';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { ConfirmationPopoverModule } from 'angular-confirmation-popover';
import { BeneficiaryHomeComponent } from './beneficiary-home/beneficiary-home.component';
import { ViewBeneficiaryComponent } from './ngo/beneficiary-view/view-beneficiary.component';
import { BeneficiaryAssesmentComponent } from './ngo/beneficiary-assesment/beneficiary-assesment.component';
import { TiRoutingModule } from './ti-routing.module';
import { BeneficiaryEditComponent } from './ngo/beneficiary-edit/beneficiary-edit.component';
import { BeneficiaryFollowupComponent } from './ost/beneficiary-followup/beneficiary-followup.component';
import { BeneficiaryCounsellingComponent } from './ngo/beneficiary-counselling/beneficiary-counselling.component';
import { BeneficiaryReferralComponent } from './ngo/beneficiary-referral/beneficiary-referral.component';
import { TransitComponent } from './ngo/beneficiary-referral/transit/transit.component';
import { OutwardComponent } from './ngo/beneficiary-referral/outward/outward.component';
import { InwardComponent } from './ngo/beneficiary-referral/inward/inward.component';
import { SettingsComponent } from './ngo/beneficiary-referral/settings/settings.component';
import { TransferOutComponent } from './ngo/beneficiary-referral/transfer-out/transfer-out.component';
import { TransferInComponent } from './ngo/beneficiary-referral/transfer-in/transfer-in.component';
import { TransferredOutComponent } from './ngo/beneficiary-referral/transfer-out/transferred-out/transferred-out.component';
import { DueForTransferComponent } from './ngo/beneficiary-referral/transfer-out/due-for-transfer/due-for-transfer.component';
import { BeneficiaryStiComponent } from './ngo/beneficiary-sti/beneficiary-sti.component';
import { BeneficiaryServicesComponent } from './ngo/beneficiary-services/beneficiary-services.component';
import { BeneficiaryDistributionComponent } from './ngo/beneficiary-distribution/beneficiary-distribution.component';
import { BeneficiaryDispensationComponent } from './ost/beneficiary-dispensation/beneficiary-dispensation.component';
import { DispensationFutureComponent } from './ost/beneficiary-dispensation/dispensation-future/dispensation-future.component';
import { DispensationPastComponent } from './ost/beneficiary-dispensation/dispensation-past/dispensation-past.component';
import { DispensationTodayComponent } from './ost/beneficiary-dispensation/dispensation-today/dispensation-today.component';
import { DispensationTransitComponent } from './ost/beneficiary-dispensation/dispensation-transit/dispensation-transit.component';
import { BeneficiaryPrescribeComponent } from './ost/beneficiary-prescribe/beneficiary-prescribe.component';
import { BeneficiaryOSTAssesmentComponent } from './ost/beneficiary-ost-assesment/beneficiary-ost-assesment.component';
import { BeneficiaryOSTServicesComponent } from './ost/beneficiary-services/beneficiary-services.component';

@NgModule({
  declarations: [
    BeneficiaryAssesmentComponent,
    BeneficiaryRegistrationComponent,
    BeneficiaryListComponent,
    BeneficiaryScreeningComponent,
    BeneficiaryHomeComponent,
    ViewBeneficiaryComponent,
    BeneficiaryEditComponent,
    BeneficiaryFollowupComponent,
    BeneficiaryCounsellingComponent,
    TransitComponent,
    OutwardComponent,
    InwardComponent,
    SettingsComponent,
    TransferOutComponent,
    BeneficiaryReferralComponent,
    TransferInComponent,
    TransferredOutComponent,
    DueForTransferComponent,
    BeneficiaryStiComponent,
    BeneficiaryServicesComponent,
    BeneficiaryDistributionComponent,
    BeneficiaryDispensationComponent,
    DispensationFutureComponent,
    DispensationPastComponent,
    DispensationTodayComponent,
    DispensationTransitComponent,
    BeneficiaryFollowupComponent,
    BeneficiaryDispensationComponent,
    BeneficiaryPrescribeComponent,
    BeneficiaryOSTAssesmentComponent,
    BeneficiaryOSTServicesComponent
  ],
  imports: [
    CommonModule,
    TiRoutingModule,
    ReactiveFormsModule,
    TableModule,
    RadioButtonModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatDialogModule,
    MatCheckboxModule,
    MatFormFieldModule,
    FontAwesomeModule,
    NgMultiSelectDropDownModule.forRoot(),
    ConfirmationPopoverModule.forRoot({
      confirmButtonType: 'danger'
    }),
    MatInputModule,
    MatTabsModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatCheckboxModule,
    MatMenuModule,
    MatIconModule,
    TableModule,
    RadioButtonModule,
  ],
 
})
export class  TIModule { }
