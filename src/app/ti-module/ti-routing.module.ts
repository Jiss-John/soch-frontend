import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BeneficiaryListComponent } from './ngo/beneficiary-list/beneficiary-list.component';
import { BeneficiaryAssesmentComponent } from './ngo/beneficiary-assesment/beneficiary-assesment.component';
import { BeneficiaryHomeComponent } from './beneficiary-home/beneficiary-home.component';
import { BeneficiaryRegistrationComponent } from './ngo/beneficiary-registration/beneficiary-registration.component';
import { ViewBeneficiaryComponent } from './ngo/beneficiary-view/view-beneficiary.component';
import { BeneficiaryScreeningComponent } from './ngo/beneficiary-screening/beneficiary-screening.component';
import { BeneficiaryEditComponent } from './ngo/beneficiary-edit/beneficiary-edit.component';
import { BeneficiaryCounsellingComponent } from './ngo/beneficiary-counselling/beneficiary-counselling.component';
import { OutwardComponent } from './ngo/beneficiary-referral/outward/outward.component';
import { InwardComponent } from './ngo/beneficiary-referral/inward/inward.component';
import { SettingsComponent } from './ngo/beneficiary-referral/settings/settings.component';
import { TransferOutComponent } from './ngo/beneficiary-referral/transfer-out/transfer-out.component';
import { TransitComponent } from './ngo/beneficiary-referral/transit/transit.component';
import { BeneficiaryReferralComponent } from './ngo/beneficiary-referral/beneficiary-referral.component';
import { TransferInComponent } from './ngo/beneficiary-referral/transfer-in/transfer-in.component';
import { TransferredOutComponent } from './ngo/beneficiary-referral/transfer-out/transferred-out/transferred-out.component';
import { DueForTransferComponent } from './ngo/beneficiary-referral/transfer-out/due-for-transfer/due-for-transfer.component';
import { BeneficiaryStiComponent } from './ngo/beneficiary-sti/beneficiary-sti.component';
import { BeneficiaryServicesComponent } from './ngo/beneficiary-services/beneficiary-services.component';
import { BeneficiaryDistributionComponent } from './ngo/beneficiary-distribution/beneficiary-distribution.component';
import { BeneficiaryFollowupComponent } from './ost/beneficiary-followup/beneficiary-followup.component';
import { BeneficiaryDispensationComponent } from './ost/beneficiary-dispensation/beneficiary-dispensation.component';
import { BeneficiaryPrescribeComponent } from './ost/beneficiary-prescribe/beneficiary-prescribe.component';
import { BeneficiaryOSTAssesmentComponent } from './ost/beneficiary-ost-assesment/beneficiary-ost-assesment.component';
import { BeneficiaryOSTServicesComponent } from './ost/beneficiary-services/beneficiary-services.component';


const mainChildRoutes: Routes = [
  { path: 'add', component: BeneficiaryRegistrationComponent },
  { path: 'list', component: BeneficiaryListComponent },
  { path: 'view', component: ViewBeneficiaryComponent },
  { path: 'screening/:uid', component: BeneficiaryScreeningComponent },
  { path: 'r&vAssessment/:uid', component: BeneficiaryAssesmentComponent },
  { path: 'edit', component: BeneficiaryEditComponent },
  { path: 'counselling/:uid', component: BeneficiaryCounsellingComponent },
  { path: 'sti/:uid', component: BeneficiaryStiComponent },
  { path: 'services', component: BeneficiaryServicesComponent },
  { path: 'distribution/:uid', component: BeneficiaryDistributionComponent },
  // { path: 'distribution', component: BeneficiaryDistributionComponent },
  { path: '', component: BeneficiaryListComponent },
  {
    //path: 'referral/:uid', component: BeneficiaryReferralComponent,
    path: 'referral', component: BeneficiaryReferralComponent,
    children: [
      { path: 'outward', component: OutwardComponent },
      { path: 'inward', component: InwardComponent },
      {
        path: 'transferout', component: TransferOutComponent,
        children: [
          { path: 'duetransfer', component: DueForTransferComponent },
          { path: 'transferredout', component: TransferredOutComponent },
          { path: '', component: DueForTransferComponent },
        ]
      },
      { path: 'transferin', component: TransferInComponent },
      { path: 'transit', component: TransitComponent },
      { path: '', component: OutwardComponent },
    ]

  },
  { path: 'followup/:uid', component: BeneficiaryFollowupComponent },
  { path: 'dispense/:uid', component: BeneficiaryDispensationComponent },
  { path: 'prescribe/:uid', component: BeneficiaryPrescribeComponent },
  { path: 'ostAssessment/:uid', component: BeneficiaryOSTAssesmentComponent },
  { path: 'pendingServices', component: BeneficiaryOSTServicesComponent },
];

const mainRoutes: Routes = [
  { path: "beneficiary", component: BeneficiaryHomeComponent, children: mainChildRoutes }
];


@NgModule({
  imports: [RouterModule.forChild(mainRoutes)],
  exports: [RouterModule]
})
export class TiRoutingModule { }
