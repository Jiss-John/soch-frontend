import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import * as moment from 'moment';
import { DatePipe } from '@angular/common';
import { BeneficiaryTI } from '@global/interfaces/beneficiaryTIRegistrationInterface';
import { hrgsecondarycategory } from '@global/interfaces/hrgsecondarycategory';
import { Pattern } from '@shared';
import { Facility } from '@global/interfaces/facilityInterface';
import { FacilityApiService } from '@global/services/Facility/facility-api.service';
import { BeneficiaryTIApiService } from '@global/services/TI-Beneficiary/beneficiary-ti-api.service';
import { BeneficiaryTIService } from '@global/services/TI-Beneficiary/beneficiary-ti.service';
import { FacilityType } from '@global/interfaces/facilityTypeInterface';
import { LocalStorage, StorageMap } from '@ngx-pwa/local-storage';
@Component({
  selector: 'beneficiary-edit',
  templateUrl: './beneficiary-edit.component.html',
  styleUrls: ['./beneficiary-edit.component.css']
})
export class BeneficiaryEditComponent implements OnInit {

  editForm: FormGroup;
  submitted = false;
  benefEditTiObj = new BeneficiaryTI();
  id: number;
  msg: string;
  code: any;
  occupationList: any[];
  childrenAgesList: any = [];
  childrenAgeGenderMapping: any = [];
  age: any;
  gender: any;
  dropdownSettings: IDropdownSettings;
  daysAvailableList: (string | { 'id': string; 'dayValue': string; })[];
  otherDetailsList: any = [];
  maxOfId: any;
  beneficiaryId: string;
  date: Date;
  orwNameList: BeneficiaryTI[];
  totalOrwNameRecords: number;
  peNameList: any;
  totalPeNameRecords: number;
  numberOfYearsInSexWork: number[];
  averageNumberOfSexualActsPerWeek: number[];
  hrgcategory: string[];
  subCategory: string[];
  hrgcategories: hrgsecondarycategory[];
  subcategoryList: string[];
  datePipeString: any;
  setRegistrationDate: string;
  events: string[] = [];
  pattern = new Pattern();
  facilities_list: Facility[];
  totalFacilityRecords: number;
  name1: any;
  name2: any;
  sunday: boolean;
  isAlcoholConsumed: boolean;
  isCondomUse: boolean;
  isSexOtherThanSpouse: boolean;
  userInfo: any;
  isTransit: boolean;
  isOutward: boolean;
  totalRecords: number;
  facilityNames_list: Facility[];
  facilityTypes_list: FacilityType[];
  referredFacility_list: FacilityType[];
  referredTo_list: Facility[];
  isUserTIOST: boolean;
  constructor(private localStorage: StorageMap,
    private formBuilder: FormBuilder,
    private _beneficiaryTIApiService: BeneficiaryTIApiService,
    private _beneficiaryTIService: BeneficiaryTIService,
    private router: Router,
    private datePipe: DatePipe,
    private _facilityApiService: FacilityApiService) { }

  ngOnInit() {

    this.userInfo = JSON.parse(localStorage.getItem('currentUser'));
    this.isUserTIOST = this.userInfo && this.userInfo.facilityTypeId == 10 ? true : false;

    this.isTransit = true; // to do get from list action if transit
    this.isOutward = true;

    this.getReferredFacility();
    this.getReferredToList();
    this.benefEditTiObj.referralFacility = 0;

    this.editForm = this.formBuilder.group({
      orwCode: [''],
      peCode: [''],
      peName: [''],
      orwName: [''],
      //facilityCode: ['', Validators.required],
      // facility:[''],
      //  beneficiaryId:['', Validators.required],
      dateOfRegistration: ['', Validators.required],
      firstName: ['', [Validators.required, Validators.maxLength(99), Validators.pattern]],
      middleName: ['', [Validators.maxLength(99), Validators.pattern]],
      lastName: ['', [Validators.required, Validators.maxLength(99), Validators.pattern]],
      dateOfBirth: [''],
      age: ['', [Validators.required, Validators.pattern(this.pattern.beneficiaryTiAgePattern)]],
      gender: ['', Validators.required],
      maritalStatus: ['', Validators.required],
      regularPartners: ['', [Validators.required, Validators.maxLength(99)]],
      numberOfPartners: ['', [Validators.required, Validators.pattern(this.pattern.beneficiaryTiNumberOfPartnersPattern)]],
      employmentStatus: ['', Validators.required],
      educationLevel: [''],
      pincode: ['', [Validators.required, Validators.pattern(this.pattern.pinCodePattern), Validators.minLength(6)]],
      block: ['', Validators.maxLength(99)],
      houseNo: ['', Validators.maxLength(99)],
      mobileNumber: ['', [Validators.required, Validators.pattern(this.pattern.mobilePattern), Validators.minLength(10)]],
      clientStatus: ['', Validators.required],
      viewPastActivity: [''],
      hrgPrimaryCategory: ['', Validators.required],
      hrgSecondaryCategory: [''],
      subCategory: [''],
      averageNumberofSexualActsUponRegistration: [''],
      numberOfYearsInSexWork: [''],
      alcoholConsume: ['', Validators.required],
      howManyDaysAWeekIfAlcoholConsuming: ['', Validators.pattern(this.pattern.beneficiaryTiIfYesHowManyDaysAWeekPattern)],
      occupation: ['', Validators.required],
      registrationPlace: [''],
      durationOfTheStays: [''],
      stayingWith: [''],
      typeOfMobility: [''],
      condomUsage: [''],
      sexOtherThanSpouse: [''],
      noOfSexualActivity: ['', Validators.maxLength(99)],
      addiction: [''],
      //consentDocumented: ['', Validators.required],
      // availableDays: [''],
      childrenAge: ['', Validators.pattern(this.pattern.agePattern)],
      childrenGender: ['',],
      withinTheDistrictNoOfTimes: [''],
      withinTheDistrictNoOfDays: [''],
      withinTheStateNoOfTimes: [''],
      withinTheStateNoOfDays: [''],
      outsideTheStateNoOfTimes: [''],
      outsideTheStateNoOfDays: [''],
      lastRiskVulnerabilityAssessment: [''],
      tbStatus: [''],
      currentSyphilis: [''],
      currentHivStatus: [''],
      nextFollowUpDate: [''],
      dispensationStatus: [''],
      dispensedLastDate: [''],
      currentDose: [''],
      ostInitiated: [''],
      ostNumber: [''],
      otherDetails: [''],
      sunday: [''],
      monday: [''],
      tuesday: [''],
      wednesday: [''],
      thursday: [''],
      friday: [''],
      saturday: [''],
      id: [''],
      uidNumber: [''],
      childrenAgeGenderMapping: [''],
      // alcoholConsume: [''],
      //alcoholConsumeN: [''],
      isTransit: [''],
      outwardReferal: [''],
      transitStartDate: [''],
      transitEndDate: [''],
      referralStatus: [''],
      referredFacility: [''],
      referredTo: [''],
    });

    // this.editForm = this.formBuilder.group({
    //   referralFacility: ['', Validators.required],//if outward mandatory
    //   referredTo: ['', Validators.required],//if outward mandatory
    // });

    //this.benefEditTiObj.numberOfPartners = 1;


    this.occupationList = ['Industrial worker', 'Construction worker', 'Daily Wages/Mathadi Kamgaar/ Auto',
      'Sand quarry worker', 'Skilled workers (Loom,Furniture, Jewellery, Zari etc.)', 'Hotel worker',
      'Dairy workers', 'Hammal / Labour', 'Farm worker/ Agriculture', 'Brick Kiln workers', 'Hawkers', 'Carpenter',
      'Mine worker', 'Wooden polish', 'Barber', 'Wall Painting', 'Motor Mechanic', 'Juice/Ice Cream vendor',
      'Security Guard', 'Loom Worker', 'Diamond Worker', 'Others'];

    this.numberOfYearsInSexWork = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25];
    this.averageNumberOfSexualActsPerWeek = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17,
      18, 19, 20, 21, 22, 23, 24, 25];
    this.daysAvailableList = [{ 'id': '0', 'dayValue': 'Sunday' }, { 'id': '1', 'dayValue': 'Monday' },
    { 'id': '2', 'dayValue': 'Tuesday' }, { 'id': '3', 'dayValue': 'Wednesday' }, { 'id': '4', 'dayValue': 'Thursday' },
    { 'id': '5', 'dayValue': 'Friday' }, { 'id': '6', 'dayValue': 'Saturday' }]
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'id',
      textField: 'dayValue',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 7,
      allowSearchFilter: true
    };

    //this.benefEditTiObj.dateOfRegistration = this.datePipe.transform(Date.now(), 'yyyy-MM-dd');
    //this.getOrwNames();
    //this.getPeNames();
    // this.getFacilityNames();
    //this.getMaxOfBeneficiaryId();
    this.hrgcategories = [{ id: 1, categoryname: 'FSW' }, { id: 2, categoryname: 'MSM/TG' }, { id: 3, categoryname: 'IDU' }];
    this.sunday = true;
    this.isAlcoholConsumed = false;
    this.isCondomUse = false;
    this.isSexOtherThanSpouse = false;
    setTimeout(() => {
      if (this._beneficiaryTIService.getBeneficiaryTI()) {
        this.benefEditTiObj = this._beneficiaryTIService.getBeneficiaryTI();
        this.isAlcoholConsumed = this.benefEditTiObj ? this.benefEditTiObj.alcoholConsume : false;
        this.isCondomUse = this.benefEditTiObj ? this.benefEditTiObj.condomUsage : false;
        this.isSexOtherThanSpouse = this.benefEditTiObj ? this.benefEditTiObj.sexOtherThanSpouse : false;
        this.benefEditTiObj.childrenAgeGenderMapping.forEach(element => {
          this.name2 = element.childrenAge;
          this.name1 = element.childrenGender;
          var child = {};
          child["childrenGender"] = this.name1;
          child["childrenAge"] = this.name2;
          this.childrenAgesList.push(child);
          // console.log(child);
          this.childrenAgeGenderMapping.push(child);
          //console.log(this.childrenAgeGenderMapping);
        });
      }
    }, 1000);
  }

  get beneficiaryTiEdit() { return this.editForm.controls; }

  //Method to edit beneficiary
  editBeneficiary() {
    this.submitted = true;
    if (this.editForm.invalid) {
      console.log("Error in editForm", this.editForm.errors);
      return;
    }
    else {
      // this.benefEditTiObj.address.pincode = this.editForm.value.pincode;
      // this.benefEditTiObj.address.houseNo = this.editForm.value.houseNo;
      // this.benefEditTiObj.address.block = this.editForm.value.block;
      console.log("Form is valid form values", this.editForm.value)
      this.userInfo = JSON.parse(localStorage.getItem('currentUser'));
      this.benefEditTiObj.facilityId = this.userInfo ? this.userInfo.facilityId : 0;
      this.benefEditTiObj.userId = this.userInfo ? this.userInfo.userId : 0;
      //this._beneficiaryTIService.setBeneficiaryTI(this.benefEditTiObj);

      this.updateBeneficiaryReferralOfflineData(this.benefEditTiObj);//save offline

      this._beneficiaryTIApiService.addBeneficiaryTI(this.benefEditTiObj)
        .subscribe((response) => {
          Swal.fire('', 'Successfully saved changes!', 'success');
          this.benefEditTiObj.beneficiaryActivityStatus = "Pending R&V Assessment";
          this.router.navigateByUrl("/ti/beneficiary/list");
        }, (error) => {
          console.log(error);
        });
    }
  }
  updateBeneficiaryReferralOfflineData(data: BeneficiaryTI) {
    if (this.referredFacility_list) {
      for (let i = 0; i < this.referredFacility_list.length; i++) {
        if (this.referredFacility_list[i].id == data.referredFacility) {
          data.referredFacilityName = this.referredFacility_list[i].facilityTypeName ? this.referredFacility_list[i].facilityTypeName.toString() : "";
          break;
        }
      }
    }

    if (this.referredTo_list) {
      for (let i = 0; i < this.referredTo_list.length; i++) {
        if (this.referredTo_list[i].id == data.referredTo) {
          data.referredToName = this.referredTo_list[i].name ? this.referredTo_list[i].name.toString() : "";
          break;
        }
      }
    }
    //console.log("###data" + JSON.stringify(data));
    this.localStorage.get("ti_beneficiary_list").subscribe((list: any) => {
      let beneficiaryList = list ? JSON.parse(list) : [];
      beneficiaryList.forEach(o => {
        if (o.uidNumber === data.uidNumber) {
          if (data.outwardReferal) {
            o.referredFacility = data.referredFacility;
            o.referredTo = data.referredTo;
            o.referralStatus = data.referralStatus;
            o.outwardReferal = data.outwardReferal;
            o.referredToName = data.referredToName;
            o.referredFacilityName = data.referredFacilityName;
          }

          if (data.isTransit === "Yes") {
            o.transitEndDate = data.transitEndDate;
            o.transitStartDate = data.transitStartDate;
            o.transitReferal = true;
            o.transitStatus = "In-Transit";
          }

          o.inwardReferal = true;
          o.referredFacilityName = data.referredFacilityName;

          o.transferOutTransferredReferal = false;
          o.transferOutDueForTransferReferal = true;

          o.transferInReferal = true;

        }
      });
      this.localStorage.set("ti_beneficiary_list", JSON.stringify(beneficiaryList)).subscribe((x) => {
      });
    });
  }

  //Method to add age and gender of children in the subsection children and their ages
  addItem() {
    let name1 = (<HTMLInputElement>document.getElementById("name1")).value
    let name2 = (<HTMLInputElement>document.getElementById("name2")).value
    var child = {};
    if (name1 && name2) {
      child["childrenGender"] = name1;
      child["childrenAge"] = name2;
      this.childrenAgesList.push(child);
      console.log(child);
      this.childrenAgeGenderMapping.push(child);
      this.benefEditTiObj.childrenAgeGenderMapping = this.childrenAgeGenderMapping;
    }

  }


  // Method to get the beneficiaryid
  getMaxOfBeneficiaryId() {
    this._beneficiaryTIApiService.getMaxOfBeneficiaryId().subscribe((Data) => {
      this.maxOfId = Data;
      var formattedNumber = ("00000000" + this.maxOfId).slice(-8);
      this.benefEditTiObj.beneficiaryCode = "U" + formattedNumber;
    }, (error) => {
      console.log(error);
    });
  }

  //Method to remove the row in the sub section children and their ages
  removeRow(index: any) {
    this.childrenAgesList.splice(index, 1);
    if (this.childrenAgeGenderMapping.length === 1) {
      this.childrenAgeGenderMapping = [];
      this.benefEditTiObj.childrenAgeGenderMapping = this.childrenAgeGenderMapping;
    } else {
      this.childrenAgeGenderMapping.splice(index, 1);
      this.benefEditTiObj.childrenAgeGenderMapping = this.childrenAgeGenderMapping;
    }
  }


  // Method to check the value in hrgprimarycategory and hrgsecondarycategory and for listing
  //corresponding subcategory values
  onCheck() {
    let hrgprimary = (<HTMLInputElement>document.getElementById("hrgprimary")).value

    let hrgsecondary = (<HTMLInputElement>document.getElementById("hrgsecondary")).value
    if (hrgprimary === hrgsecondary) {
      Swal.fire('', 'Enter a different Value for HRG Secondary Category', 'warning');
    }
    var a = this.benefEditTiObj.hrgPrimaryCategory

    if (a === 'FSW') {
      this.subcategoryList = ['Home/secrect based', 'Street/Public Place Based', 'Brothel based',
        'Lodge/hotel based', 'Dhaba based', 'Highway sex workers', 'Any Others(Specify)']

    }
    else if (a === 'MSM/TG') {
      this.subcategoryList = ['Kothi', 'Panthi', 'Double decker', 'Bisexual', 'Any Other Specify']

    }
    else if (a === 'IDU') {
      this.subcategoryList = ['Daily injectors', 'Non-Daily Injector']

    }
    else {
      this.subcategoryList = null;
    }

  }


  //Method to calculate the age from DOB when dob is given
  ageFromDateOfBirth(dateOfBirth: any) {
    let dob = moment().diff(dateOfBirth, 'years');
    this.benefEditTiObj.age = dob + '';
    this.editForm.controls['age'].setValue(this.benefEditTiObj.age);
  }




  //Method to get the list of orw names
  getOrwNames(): void {
    console.log("Inside getOrw Names");
    this._beneficiaryTIApiService.getOrwNames().subscribe((Data) => {
      this.orwNameList = Data;
      this.totalOrwNameRecords = this.orwNameList.length;

    }, (error) => {
      console.log(error);
    }
    );
  }

  // //Method to get the list of pe Names
  getPeNames(): void {
    console.log("Inside pe Names");
    this._beneficiaryTIApiService.getPeNames().subscribe((Data) => {
      this.peNameList = Data;
      this.totalPeNameRecords = this.peNameList.length;

    }, (error) => {
      console.log(error);
    }
    );
  }

  getOrwCode(orwName: string) {

    this.peNameList.forEach(element => {
      if (element.name == orwName) {
        this.benefEditTiObj.orwCode = element.id;

      }
    },
      (error) => {
        console.log(error);
      }
    );
    //   this.benefEditTiObj.orwCode = this._beneficiaryTIApiService.getOrwNames().subscribe((Data) => {
    //   this.benefEditTiObj.orwCode = Data;
    // },
    // (error) => {
    //   console.log(error);
    //  }
    //  );
  }


  getPeCode(peName: string) {

    this.peNameList.forEach(element => {
      if (element.name == peName) {
        this.benefEditTiObj.orwCode = element.id;
      }
    },
      (error) => {
        console.log(error);
      }
    );

    //   this.benefEditTiObj.peCode = this._beneficiaryTIApiService.getPeCode().subscribe((Data) => {
    //   this.benefEditTiObj.peCode = Data;
    // },
    // (error) => {
    //   console.log(error);
    //  }
    //  );
  }


  //Method to reset the form
  onReset() {

    this.submitted = false;
    this.editForm.reset();

  }


  //Method to get all FacilityNames in the drop down.
  getFacilityNames() {
    this._facilityApiService.getAllFacilities().subscribe((facilityData) => {
      this.facilityNames_list = facilityData;
      this.totalFacilityRecords = this.facilities_list.length;
    }, (error) => {
      console.log(error);
    }
    );
  }


  //Method to fetch the corresponding facility facilityCode
  getFacilityCode(facility: any) {
    this.facilities_list.forEach(element => {
      if (element.name == facility) {
        this.benefEditTiObj.facilityCode = element.code;
      }
    });
  }

  onCancel() {
    this.router.navigateByUrl("ti/beneficiary/list");
  }

  // onReferredFacilityChange(eventObj) {
  //   if (eventObj) {
  //     // to do enable refferedTo
  //     // for (let i = 0; i < this.referredFacility_list.length; i++) {
  //     //   if (this.referredFacility_list[i].id == eventObj){
  //     //     this.benefEditTiObj.referredFacilityName =this.referredFacility_list[i].facilityTypeName ? this.referredFacility_list[i].facilityTypeName.toString() : "";
  //     //     break;
  //     //   }
  //     // }
  //   }

  // }


  // onReferredToChange(eventObj) {
  //   if (eventObj) {
  //     // to do enable refferedTo
  //     // for (let i = 0; i < this.referredTo_list.length; i++) {
  //     //   if (this.referredTo_list[i].id == eventObj){
  //     //     this.benefEditTiObj.referredToName =this.referredTo_list[i].name ? this.referredTo_list[i].name.toString() : "";
  //     //     break;
  //     //   }
  //     // }
  //   }
  // }

  // onReferralStatusChange(eventObj) {
  //   if (eventObj === "Not Referred") {
  //     this.addOtherRefferedToVal();
  //     //make referral facility read only
  //   }

  // }

  // addOtherRefferedToVal() {
  //   //get admin settings screen selection
  // }

  //Method to get all FacilityTypes in the drop down.
  getReferredFacility() {
    this._facilityApiService.getAllFacilityTypes().subscribe((facilityData) => {
      this.referredFacility_list = facilityData;
      // this.localStorage.set("referredFacilityList", JSON.stringify(this.referredFacility_list)).subscribe((x) => {
      // });

    }, (error) => {
      console.log(error);
    }
    );
  }


  //Method to get all FacilityNames in the drop down.
  getReferredToList() {
    this._facilityApiService.getAllFacilities().subscribe((facilityData) => {
      this.referredTo_list = facilityData;
      // this.localStorage.set("referredToList", JSON.stringify(this.referredTo_list)).subscribe((x) => {
      // });
    }, (error) => {
      console.log(error);
    }
    );
  }



}
