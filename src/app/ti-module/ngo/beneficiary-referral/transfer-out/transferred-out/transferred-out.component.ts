import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { BeneficiaryTI } from '@global/interfaces/beneficiaryTIRegistrationInterface';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { BeneficiaryTIApiService } from '@global/services/TI-Beneficiary/beneficiary-ti-api.service';
import { BeneficiaryApiService } from '@global/services/TI-Beneficiary/beneficiary-api.service';
import Swal from 'sweetalert2';
import { StorageMap } from '@ngx-pwa/local-storage';

@Component({
  selector: 'app-transferred-out',
  templateUrl: './transferred-out.component.html',
  styleUrls: ['./transferred-out.component.css']
})
export class TransferredOutComponent implements OnInit {

  activeLink: any = "";
  link: any = "";
  //displayedColumns: string[] = ['select', 'id', 'firstName', 'transferTo','transferDate','transferStatus'];
  displayedColumns: string[] = ['uidNumber', 'name', 'transferTo', 'transferDate', 'transferStatus'];
  dataSource: MatTableDataSource<BeneficiaryTI>;
  selection = new SelectionModel<BeneficiaryTI>(true, []);

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  isTransferred: boolean;
  transferForm: FormGroup;
  searchValue = new FormControl(); //Store value to search
  beneficiaryId: any;
  beneficiary = new BeneficiaryTI;

  constructor(private localStorage: StorageMap, private formBuilder: FormBuilder, private router: Router, private beneficiaryTIApiService: BeneficiaryTIApiService, private beneficiaryApiService: BeneficiaryApiService) {
    beneficiaryTIApiService.getAllBeneficiariesTransferOutTI().subscribe((Data) => {
      this.dataSource = new MatTableDataSource(Data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }, (error) => {
      console.log(error);
    });
  }

  ngOnInit() {
    this.transferForm = this.formBuilder.group({
      firstName: [''],
      mobileNumber: [''],
      beneficiaryUId: [''],
      beneficiaryId: [''],
    }
    );
  }

  ngAfterViewInit() {
    this.localStorage.get("ti_beneficiary_list").subscribe((list: any) => {
      // console.log("####outward list" + list);
      let beneficiaryList = list ? JSON.parse(list):[];
      let filterBeneficiary = beneficiaryList.filter(o => {
        return o['transferOutTransferredReferal'] == true
      });
      console.log("####filterBeneficiary list" + JSON.stringify(filterBeneficiary));
      this.createDisplayData(filterBeneficiary);
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  // isAllSelected() {
  //   const numSelected = this.selection.selected.length;
  //   const numRows = this.dataSource.data.length;
  //   return numSelected === numRows;
  // }

  // /** Selects all rows if they are not all selected; otherwise clear selection. */
  // masterToggle() {
  //   this.isAllSelected() ?
  //       this.selection.clear() :
  //       this.dataSource.data.forEach(row => this.selection.select(row));
  // }

  // /** The label for the checkbox on the passed row */
  // checkboxLabel(row?: BeneficiaryTI): string {
  //   if (!row) {
  //     return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
  //   }
  //  // return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.transferDate + 1}`;
  // }

  createDisplayData(filterBeneficiary) {
    this.dataSource = filterBeneficiary.map(o => {
      return {
        'uidNumber': o.uidNumber,
        'name': o.firstName + ' ' + o.lastName,
        'transferTo': o.transferTo,
        'transferDate': o.transferDate,
        'transferStatus': o.transferStatus ? "Transferred" : "Due For Transfer",
      }

    });

  }

  //For searching based on UID (Beneficiary Id) and Mobile Number
  searchBeneficiary() {
    this.localStorage.get("ti_beneficiary_list").subscribe((list: any) => {
      let beneficiaryList = list? JSON.parse(list):[];
      let filterBeneficiary = beneficiaryList.filter(o => {
        return o['uidNumber'] == this.searchValue.value || o['mobileNumber'] == this.searchValue.value
      });
      console.log("####search result list" + JSON.stringify(filterBeneficiary));
      this.createDisplayData(filterBeneficiary);
    });
    this.beneficiaryApiService.getBeneficiaryBysearchValue(this.searchValue.value).subscribe((Data) => {
      this.beneficiary = Data;
      this.beneficiaryId = this.beneficiary.id;
      //This is used to display values in text fields (using formControlName) without ngModel- donot remove this
      this.transferForm.patchValue({
        //assigning values to formControl variable from the object that is subscribed value from api call.
        firstName: this.beneficiary.firstName,
        uidNumber: this.beneficiary.uidNumber,
        beneficiaryId: this.beneficiary.id,
        mobileNumber: this.beneficiary.mobileNumber,
        activityStatus: this.beneficiary.beneficiaryActivityStatus,
        isActive: this.beneficiary.isActive,
      });
      console.log(this.beneficiary);
    }, (error) => {
      Swal.fire(error.error.details ? error.error.details[0].description + "" : "");
    }
    );
  }
}
