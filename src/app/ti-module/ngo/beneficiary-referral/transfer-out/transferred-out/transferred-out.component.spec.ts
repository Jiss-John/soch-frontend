import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransferredOutComponent } from './transferred-out.component';

describe('TransferredOutComponent', () => {
  let component: TransferredOutComponent;
  let fixture: ComponentFixture<TransferredOutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransferredOutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransferredOutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
