import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-transfer-out',
  templateUrl: './transfer-out.component.html',
  styleUrls: ['./transfer-out.component.css']
})
export class TransferOutComponent implements OnInit {
  navTransferLinks = [
    {
      label: 'Due for Transfer',
      link: './duetransfer',
      index: 0
    }, {
      label: 'Transferred Out',
      link: './transferredout',
      index: 1
    }
  ];
  activeLink = this.navTransferLinks[0];

  constructor() {
  }

  ngOnInit() {
 
  }

}

