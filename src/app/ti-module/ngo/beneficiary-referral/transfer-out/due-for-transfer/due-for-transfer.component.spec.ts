import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DueForTransferComponent } from './due-for-transfer.component';

describe('DueForTransferComponent', () => {
  let component: DueForTransferComponent;
  let fixture: ComponentFixture<DueForTransferComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DueForTransferComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DueForTransferComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
