import { Component, OnInit, ViewChild } from '@angular/core';
import Swal from 'sweetalert2';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Pattern } from '@shared';
import { BeneficiaryTI } from '@global/interfaces/beneficiaryTIRegistrationInterface';
import { Router } from '@angular/router';
import { BeneficiaryTIApiService } from '@global/services/TI-Beneficiary/beneficiary-ti-api.service';
import { BeneficiaryApiService } from '@global/services/TI-Beneficiary/beneficiary-api.service';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { StorageMap } from '@ngx-pwa/local-storage';

@Component({
  selector: 'app-inward',
  templateUrl: './inward.component.html',
  styleUrls: ['./inward.component.css']
})
export class InwardComponent implements OnInit {

  inwardForm: FormGroup;
  submitted = false;
  outwardReferralObj = new BeneficiaryTI;
  pattern = new Pattern();
  userInfo: any;
  error: string;
  searchValue = new FormControl(); //Store value to search
  beneficiaryId: any;
  beneficiary = new BeneficiaryTI;
  activeLink: any = "";
  link: any = "";
  //displayedColumns: string[] = ['select', 'id', 'firstName', 'dateOfReferral', 'referredFacility', 'HIVStatus','referredStatus', 'action'];
  displayedColumns: string[] = ['uidNumber', 'name', 'dateOfReferral', 'referredFacility', 'hivStatus', 'referralStatus', 'action'];
  dataSource: MatTableDataSource<BeneficiaryTI>;
  selection = new SelectionModel<BeneficiaryTI>(true, []);

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  isTransferred: boolean;
  selectedRow: BeneficiaryTI;

  constructor(private localStorage: StorageMap, private formBuilder: FormBuilder, private router: Router, private _beneficiaryTIApiService: BeneficiaryTIApiService, private beneficiaryApiService: BeneficiaryApiService) {
    // _beneficiaryTIApiService.getAllBeneficiariesInwardTI().subscribe((data) => {
    //   this.dataSource = new MatTableDataSource(data);
    //   this.dataSource.paginator = this.paginator;
    //   this.dataSource.sort = this.sort;
    // }, (error) => {
    //   console.log(error);
    // });
  }

  ngOnInit() {
    this.inwardForm = this.formBuilder.group({
      firstName: [''],
      mobileNumber: [''],
      beneficiaryUId: [''],
      beneficiaryId: [''],
    }
    );
  }

  ngAfterViewInit() {
    this.localStorage.get("ti_beneficiary_list").subscribe((list: any) => {
      // console.log("####outward list" + list);
      let beneficiaryList = list? JSON.parse(list):[];
      let filterBeneficiary = beneficiaryList.filter(o => {
        return o['inwardReferal'] == true
      });
      console.log("####filterBeneficiary list" + JSON.stringify(filterBeneficiary));
      this.createDisplayData(filterBeneficiary);
    });
  }

  createDisplayData(filterBeneficiary) {
    this.dataSource = filterBeneficiary.map(o => {
      return {
        'uidNumber': o.uidNumber,
        'name': o.firstName + ' ' + o.lastName,
        'dateOfReferral': o.dateOfReferral,
        'referredFacility': o.referredFacilityName,
        'hivStatus': o.hivStatus,
        'referralStatus': o.referralStatus,
      }
    });

  }

  //For searching based on UID (Beneficiary Id) and Mobile Number
  searchBeneficiary() {
    this.localStorage.get("ti_beneficiary_list").subscribe((list: any) => {
      let beneficiaryList = list? JSON.parse(list) : [];
      let filterBeneficiary = beneficiaryList.filter(o => {
        return o['uidNumber'] == this.searchValue.value || o['mobileNumber'] == this.searchValue.value
      });
      console.log("####search result list" + JSON.stringify(filterBeneficiary));
      this.createDisplayData(filterBeneficiary);
    });
    this.beneficiaryApiService.getBeneficiaryBysearchValue(this.searchValue.value).subscribe((Data) => {
      this.beneficiary = Data;
      this.beneficiaryId = this.beneficiary.id;
      //This is used to display values in text fields (using formControlName) without ngModel- donot remove this
      this.inwardForm.patchValue({
        //assigning values to formControl variable from the object that is subscribed value from api call.
        firstName: this.beneficiary.firstName,
        uidNumber: this.beneficiary.uidNumber,
        beneficiaryId: this.beneficiary.id,
        mobileNumber: this.beneficiary.mobileNumber,
        activityStatus: this.beneficiary.beneficiaryActivityStatus,
        isActive: this.beneficiary.isActive,
      });
      console.log(this.beneficiary);
    }, (error) => {
      Swal.fire(error.error.details ? error.error.details[0].description + "  !use advanced search" : "!use advanced search");
    }
    );
  }

  getSelRecordFromTable(rowData: BeneficiaryTI) {
    this.selectedRow = rowData;
  }

  //Method to inward beneficiary referral
  doInwardReferral() {
    if (this.selectedRow) {
      if (this.selectedRow.referralStatus != "Referred") {
        this.updateBeneficiaryReferralOfflineData(this.selectedRow);
        this._beneficiaryTIApiService.referralAdd(this.selectedRow)
          .subscribe((response) => {
            console.log("Response", response)
            Swal.fire('', 'Successfully saved changes!', 'success');
            this.router.navigateByUrl("/ti/beneficiary/list");
          }, (error) => {
            console.log(error);
          });
      } else {
        Swal.fire('', 'Already referred!', 'success');
      }
    }
  }

  updateBeneficiaryReferralOfflineData(data: BeneficiaryTI) {
    this.localStorage.get("ti_beneficiary_list").subscribe((list: any) => {
      let beneficiaryList = list? JSON.parse(list) : [];
      beneficiaryList.forEach(o => {
        if (o.uidNumber === data.uidNumber) {
          o.referralStatus = "Referred";
        }
      });
      this.localStorage.set("ti_beneficiary_list", JSON.stringify(beneficiaryList)).subscribe((x) => {
        Swal.fire('', 'Successfully saved changes!', 'success');
        this.router.navigateByUrl("/ti/beneficiary/list");
      });
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  // isAllSelected() {
  //   const numSelected = this.selection.selected.length;
  //   const numRows = this.dataSource ? this.dataSource.data.length : 0;
  //   return numSelected === numRows;
  // }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  // masterToggle() {
  //   this.isAllSelected() ?
  //       this.selection.clear() :
  //       this.dataSource.data.forEach(row => this.selection.select(row));
  // }

  /** The label for the checkbox on the passed row */
  //   checkboxLabel(row?: BeneficiaryTI): string {
  //     if (!row) {
  //       return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
  //     }
  //    // return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.transferDate + 1}`;
  //   }
}


