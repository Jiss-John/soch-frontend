import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Pattern } from '@shared';
import { BeneficiaryTI } from '@global/interfaces/beneficiaryTIRegistrationInterface';
import { Router } from '@angular/router';
import { BeneficiaryTIApiService } from '@global/services/TI-Beneficiary/beneficiary-ti-api.service';
import { BeneficiaryApiService } from '@global/services/TI-Beneficiary/beneficiary-api.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {

 
  settingsForm: FormGroup;
  submitted = false;
  settingsObj = new BeneficiaryTI;
  pattern = new Pattern();
  userInfo: any;
  error: string;
  searchValue = new FormControl(); //Store value to search
  beneficiaryId: any;
  beneficiary =new BeneficiaryTI;

  constructor(private formBuilder: FormBuilder, private router: Router, private beneficiaryTIApiService: BeneficiaryTIApiService, private beneficiaryApiService: BeneficiaryApiService) { }

  ngOnInit() {
    this.settingsForm = this.formBuilder.group({
      rntcpCenter:[''],
      dsrcCenter:[''],
      ictcCenter:[''],
      tiCenter:['']
    });
    this.fetchRNTCPData();
    this.fetchICTCPData();
    this.fetchDSRCData();
    this.fetchTIData();
  }
  fetchTIData() {
    
  }
  fetchRNTCPData() {
    
  }

  fetchDSRCData() {
    
  }

  fetchICTCPData() {
    
  }
   

  get settings() { return this.settingsForm.controls; }

  //Method to reset the form
  onReset() {
    this.submitted = false;
    this.settingsForm.reset();
  }

  // onCancel() {
  //   this.router.navigateByUrl("ti/beneficiary/referral");
  // }

  //Method to settings beneficiary
  doCounselling() {
    this.submitted = true;
    if (this.settingsForm.invalid) {
      this.settingsObj = this.settingsForm.value;
      console.log("Error in settingsForm", this.settingsForm.errors);
      return;
    }
    else {
      console.log("Form is valid form values", this.settingsForm.value)
      this.settingsObj = this.settingsForm.value;
      this.beneficiaryTIApiService.referralAdd(this.settingsObj)
        .subscribe((response) => {
          console.log("Response", response)
          Swal.fire('', 'Successfully saved changes!', 'success');
         
          this.router.navigateByUrl("/ti/beneficiary/referral");
          this.onReset();

        }, (error) => {
          console.log(error);
        });
    }

  }

}
