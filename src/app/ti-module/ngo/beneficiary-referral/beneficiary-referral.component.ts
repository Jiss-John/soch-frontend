import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-beneficiary-referral',
  templateUrl: './beneficiary-referral.component.html',
  styleUrls: ['./beneficiary-referral.component.css']
})
export class BeneficiaryReferralComponent implements OnInit {

  navReferralLinks = [
    {
      label: 'Outward Referral',
      link: './outward',
      index: 0
    }, {
      label: 'Inward Referral',
      link: './inward',
      index: 1
    }, {
      label: 'Transit',
      link: './transit',
      index: 2
    }, {
      label: 'Transfer In',
      link: './transferin',
      index: 3
    }, {
      label: 'Transfer Out',
      link: './transferout',
      index: 4
    },
  ];
  activeLink = this.navReferralLinks[0];

  constructor() {
  }

  ngOnInit() {

  }

}
