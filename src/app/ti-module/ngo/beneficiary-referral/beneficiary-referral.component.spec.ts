import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BeneficiaryReferralComponent } from './beneficiary-referral.component';

describe('BeneficiaryReferralComponent', () => {
  let component: BeneficiaryReferralComponent;
  let fixture: ComponentFixture<BeneficiaryReferralComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BeneficiaryReferralComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BeneficiaryReferralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
