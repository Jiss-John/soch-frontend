import { Component, OnInit, ɵConsole } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import * as moment from 'moment';
import { DatePipe } from '@angular/common';
import { BeneficiaryTI } from '@global/interfaces/beneficiaryTIRegistrationInterface';
import { hrgsecondarycategory } from '@global/interfaces/hrgsecondarycategory';
import { Pattern } from '@shared';
import { Facility } from '@global/interfaces/facilityInterface';
import { FacilityApiService } from '@global/services/Facility/facility-api.service';
import { BeneficiaryTIApiService } from '@global/services/TI-Beneficiary/beneficiary-ti-api.service';
import { BeneficiaryTIService } from '@global/services/TI-Beneficiary/beneficiary-ti.service';
import { StorageMap } from '@ngx-pwa/local-storage';

@Component({
  selector: 'beneficiary-registration',
  templateUrl: './beneficiary-registration.component.html',
  styleUrls: ['./beneficiary-registration.component.css']
})
export class BeneficiaryRegistrationComponent implements OnInit {

  registerForm: FormGroup;
  submitted = false;
  benefTiObj = new BeneficiaryTI();
  id: number;
  msg: string;
  code: any;
  today:Date;
  occupationList: any[];
  childrenAgesList: any = [];
  childrenAgeGenderMapping: any = [];
  age: any;
  gender: any;
  dropdownSettings: IDropdownSettings;
  daysAvailableList: (string | { 'id': string; 'dayValue': string; })[];
  otherDetailsList: any = [];
  maxOfId: any;
  beneficiaryId: string;
  date: Date;
  orwNameList: BeneficiaryTI[];
  totalOrwNameRecords: number;
  peNameList: any;
  totalPeNameRecords: number;
  numberOfYearsInSexWork: number[];
  averageNumberOfSexualActsPerWeek: number[];
  hrgcategory: string[];
  subCategory: string[];
  hrgcategories: hrgsecondarycategory[];
  subcategoryList: string[];
  datePipeString: any;
  setRegistrationDate: string;
  events: string[] = [];
  pattern = new Pattern();
  facilities_list: Facility[];
  totalFacilityRecords: number;
  userInfo: any;
  isUserTIOST: boolean;
  constructor(private localStorage: StorageMap, private formBuilder: FormBuilder, private _beneficiaryTIApiService: BeneficiaryTIApiService,
    private _beneficiaryTIService: BeneficiaryTIService, private router: Router,
    private datePipe: DatePipe, private _facilityApiService: FacilityApiService) { }

  ngOnInit() {

    this.userInfo = JSON.parse(localStorage.getItem('currentUser'));
    this.isUserTIOST =  this.userInfo && this.userInfo.facilityTypeId == 10 ? true: false;

    this.benefTiObj.dateOfRegistration = this.datePipe.transform(Date.now(), 'yyyy-MM-dd');
    this.today = new Date();
   
    //this.getOrwNames();
    //this.getPeNames();
    // this.getFacilityNames();
    //this.getMaxOfBeneficiaryId();
    this.hrgcategories = [{ id: 1, categoryname: 'FSW' }, { id: 2, categoryname: 'MSM/TG' }, { id: 3, categoryname: 'IDU' }]
    this.registerForm = this.formBuilder.group({
      orwCode: [''],
      peCode: [''],
      peName: [''],
      orwName: [''],
      //facilityCode: ['', Validators.required],
      // facility:[''],
      //  beneficiaryId:['', Validators.required],
      dateOfRegistration: ['', Validators.required],
      firstName: ['', [Validators.required, Validators.maxLength(99), Validators.pattern]],
      middleName: ['', [Validators.maxLength(99), Validators.pattern]],
      lastName: ['', [Validators.required, Validators.maxLength(99), Validators.pattern]],
      dateOfBirth: [''],
      age: ['', [Validators.required, Validators.pattern(this.pattern.beneficiaryTiAgePattern)]],
      gender: ['', Validators.required],
      maritalStatus: ['', Validators.required],
      regularPartners: ['', [Validators.required, Validators.maxLength(99)]],
      numberOfPartners: ['', [Validators.required, Validators.pattern(this.pattern.beneficiaryTiNumberOfPartnersPattern)]],
      employmentStatus: ['', Validators.required],
      educationLevel: [''],
      pincode: ['', [Validators.required, Validators.pattern(this.pattern.pinCodePattern), Validators.minLength(6)]],
      block: ['', Validators.maxLength(99)],
      houseNo: ['', Validators.maxLength(99)],
      mobileNumber: ['', [Validators.required, Validators.pattern(this.pattern.digitsOnlyPattern), Validators.minLength(10)]],
      clientStatus: ['', Validators.required],
      viewPastActivity: [''],
      hrgPrimaryCategory: ['', Validators.required],
      hrgSecondaryCategory: [''],
      subCategory: [''],
      averageNumberofSexualActsUponRegistration: [''],
      numberOfYearsInSexWork: [''],
      alcoholConsume: ['', Validators.required],
      howManyDaysAWeekIfAlcoholConsuming: ['', Validators.pattern(this.pattern.beneficiaryTiIfYesHowManyDaysAWeekPattern)],
      occupation: ['', Validators.required],
      registrationPlace: [''],
      durationOfTheStays: [''],
      stayingWith: [''],
      typeOfMobility: [''],
      condomUsage: [''],
      sexOtherThanSpouse: [''],
      noOfSexualActivity: ['', Validators.maxLength(99)],
      addiction: [''],
     // consentDocumented: ['', Validators.required],
      availableDays: ['', Validators.required],
      childrenAge: ['', Validators.pattern(this.pattern.agePattern)],
      childrenGender: ['',],
      withinTheDistrictNoOfTimes: ['',Validators.pattern(this.pattern.digitsOnlyPattern)],
      withinTheDistrictNoOfDays: ['',Validators.pattern(this.pattern.digitsOnlyPattern)],
      withinTheStateNoOfTimes: ['',Validators.pattern(this.pattern.digitsOnlyPattern)],
      withinTheStateNoOfDays: ['',Validators.pattern(this.pattern.digitsOnlyPattern)],
      outsideTheStateNoOfTimes: ['',Validators.pattern(this.pattern.digitsOnlyPattern)],
      outsideTheStateNoOfDays: ['',Validators.pattern(this.pattern.digitsOnlyPattern)],
      lastRiskVulnerabilityAssessment: [''],
      tbStatus: [''],
      currentSyphilis: [''],
      currentHivStatus: [''],
      nextFollowUpDate: [''],
      dispensationStatus: [''],
      dispensedLastDate: [''],
      currentDose: [''],
      ostInitiated: [''],
      ostNumber: [''],
      otherDetails: [''],
      sunday: [''],
      monday: [''],
      tuesday: [''],
      wednesday: [''],
      thursday: [''],
      friday: [''],
      saturday: [''],
      id: [''],
      uidNumber: [''],
      childrenAgeGenderMapping: ['']
    });

    this.benefTiObj.numberOfPartners = 1;


    this.occupationList = ['Industrial worker', 'Construction worker', 'Daily Wages/Mathadi Kamgaar/ Auto',
      'Sand quarry worker', 'Quarry worker', 'Skilled workers (Loom,Furniture, Jewellery, Zari etc.)', 'Hotel worker',
      'Dairy workers', 'Hammal / Labour', 'Farm worker/ Agriculture', 'Brick Kiln workers', 'Hawkers', 'Carpenter',
      'Mine worker', 'Wooden polish', 'Barber', 'Plumber', 'Wall Painting', 'Motor Mechanic', 'Juice/Ice Cream vendor',
      'Security Guard', 'Loom Worker', 'Diamond Worker', 'Others'];

    this.numberOfYearsInSexWork = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25];
    this.averageNumberOfSexualActsPerWeek = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17,
      18, 19, 20, 21, 22, 23, 24, 25];
    this.daysAvailableList = [{ 'id': '0', 'dayValue': 'Sunday' }, { 'id': '1', 'dayValue': 'Monday' },
    { 'id': '2', 'dayValue': 'Tuesday' }, { 'id': '3', 'dayValue': 'Wednesday' }, { 'id': '4', 'dayValue': 'Thursday' },
    { 'id': '5', 'dayValue': 'Friday' }, { 'id': '6', 'dayValue': 'Saturday' }]
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'id',
      textField: 'dayValue',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 7,
      allowSearchFilter: true
    };

  }

  get beneficiaryTi() { return this.registerForm.controls; }

  //Method to add beneficiary
  addBeneficiary() {

    this.submitted = true;
    if (this.registerForm.invalid) {
      console.log("Error in registerForm", this.registerForm.errors);
      return;
    }
    else {
     
      // this.benefTiObj.address.pincode = this.registerForm.value.pincode;
      // this.benefTiObj.address.houseNo = this.registerForm.value.houseNo;
      // this.benefTiObj.address.block = this.registerForm.value.block;
      console.log("Form is valid form values", this.registerForm.value)
      this.userInfo = JSON.parse(localStorage.getItem('currentUser'));
      this.benefTiObj.facilityId = this.userInfo ? this.userInfo.facilityId : 0;
      this.benefTiObj.userId = this.userInfo ? this.userInfo.userId : 0;
      // this._beneficiaryTIService.setBeneficiaryTI(this.benefTiObj);

      //this.updateBeneficiaryReferralOfflineData(this.benefTiObj);

      this._beneficiaryTIApiService.addBeneficiaryTI(this.benefTiObj)
        .subscribe((response) => {
          console.log("Response", response)
          Swal.fire('', 'Successfully saved changes!', 'success');
          this.benefTiObj.beneficiaryActivityStatus = "Pending R&V Assessment";
          // localStorage.setItem("ti_beneficiary_list",JSON.stringify(response));
          this.updateBeneficiaryReferralOfflineData(response);
          this.router.navigateByUrl("/ti/beneficiary/list");
          this.onReset();

        }, (error) => {
          console.log(error);
        });
    }

  }

  addToOfflineBeneficiaryList(dataVal) {
    let data = JSON.stringify(dataVal);
    this.localStorage.set("ti_beneficiary_list", data).subscribe(() => {
      console.log("##addToOfflineBeneficiaryList "+data);
    });
  }

  updateBeneficiaryReferralOfflineData(data: BeneficiaryTI) {
    this.localStorage.get('ti_beneficiary_list').subscribe((list: any) => {
      let dataToUpdate = list ? JSON.parse(list) : [] ;
      dataToUpdate.push(data);
      this.addToOfflineBeneficiaryList(dataToUpdate);
    });
  }

 


  //Method to add age and gender of children in the subsection children and their ages
  addItem() {
    let name1 = (<HTMLInputElement>document.getElementById("name1")).value
    let name2 = (<HTMLInputElement>document.getElementById("name2")).value
    var child = {};
    child["childrenGender"] = name1;
    child["childrenAge"] = name2;
    this.childrenAgesList.push(child);
    this.childrenAgeGenderMapping.push(child);
    this.benefTiObj.childrenAgeGenderMapping = this.childrenAgeGenderMapping;

  }


  // Method to get the beneficiaryid
  getMaxOfBeneficiaryId() {
    this._beneficiaryTIApiService.getMaxOfBeneficiaryId().subscribe((Data) => {
      this.maxOfId = Data;
      var formattedNumber = ("00000000" + this.maxOfId).slice(-8);
      this.benefTiObj.beneficiaryCode = "U" + formattedNumber;
    }, (error) => {
      console.log(error);
    });
  }

  //Method to remove the row in the sub section children and their ages
  removeRow(index: any) {
    this.childrenAgesList.splice(index, 1);
    if(this.childrenAgeGenderMapping.length === 1){
      this.childrenAgeGenderMapping = [];
      this.benefTiObj.childrenAgeGenderMapping = this.childrenAgeGenderMapping;
    }else{
      this.childrenAgeGenderMapping.splice(index,1);
      this.benefTiObj.childrenAgeGenderMapping = this.childrenAgeGenderMapping;
    }
    }


  // Method to check the value in hrgprimarycategory and hrgsecondarycategory and for listing
  //corresponding subcategory values
  onCheck() {
    let hrgprimary = (<HTMLInputElement>document.getElementById("hrgprimary")).value

    let hrgsecondary = (<HTMLInputElement>document.getElementById("hrgsecondary")).value
    if (hrgprimary === hrgsecondary) {
      Swal.fire('', 'Please select a different secondary category', 'warning');
    }
    var a = this.benefTiObj.hrgPrimaryCategory
    console.log(a)
    if (a === 'FSW') {
      this.subcategoryList = ['Home/secrect based', 'Street/Public Place Based', 'Brothel based',
        'Lodge/hotel based', 'Dhaba based', 'Highway sex workers', 'Any Others(Specify)']

    }
    else if (a === 'MSM/TG') {
      this.subcategoryList = ['Kothi', 'Panthi', 'Double decker', 'Bisexual','Hijras', 'Any Other Specify']

    }
    else if (a === 'IDU') {
      this.subcategoryList = ['Daily injectors', 'Non-Daily Injector']

    }
    else {
      this.subcategoryList = null;
    }

  }


  //Method to calculate the age from DOB when dob is given
  ageFromDateOfBirth(dateOfBirth: any) {
    let dob = moment().diff(dateOfBirth, 'years');
    this.benefTiObj.age = dob + '';
    this.registerForm.controls['age'].setValue(this.benefTiObj.age);
  }




  //Method to get the list of orw names
  getOrwNames(): void {
    console.log("Inside getOrw Names");
    this._beneficiaryTIApiService.getOrwNames().subscribe((Data) => {
      this.orwNameList = Data;
      this.totalOrwNameRecords = this.orwNameList.length;

    }, (error) => {
      console.log(error);
    }
    );
  }

  // //Method to get the list of pe Names
  getPeNames(): void {
    console.log("Inside pe Names");
    this._beneficiaryTIApiService.getPeNames().subscribe((Data) => {
      this.peNameList = Data;
      this.totalPeNameRecords = this.peNameList.length;

    }, (error) => {
      console.log(error);
    }
    );
  }

  getOrwCode(orwName: string) {

    this.peNameList.forEach(element => {
      if (element.name == orwName) {
        this.benefTiObj.orwCode = element.id;

      }
    },
      (error) => {
        console.log(error);
      }
    );
    //   this.benefTiObj.orwCode = this._beneficiaryTIApiService.getOrwNames().subscribe((Data) => {
    //   this.benefTiObj.orwCode = Data;
    // },
    // (error) => {
    //   console.log(error);
    //  }
    //  );
  }


  getPeCode(peName: string) {

    this.peNameList.forEach(element => {
      if (element.name == peName) {
        this.benefTiObj.orwCode = element.id;
      }
    },
      (error) => {
        console.log(error);
      }
    );

    //   this.benefTiObj.peCode = this._beneficiaryTIApiService.getPeCode().subscribe((Data) => {
    //   this.benefTiObj.peCode = Data;
    // },
    // (error) => {
    //   console.log(error);
    //  }
    //  );
  }


  //Method to reset the form
  onReset() {

    this.submitted = false;
    this.registerForm.reset();

  }


  //Method to get all FacilityNames in the drop down.
  getFacilityNames() {
    this._facilityApiService.getAllFacilities().subscribe((facilityData) => {
      this.facilities_list = facilityData;
      this.totalFacilityRecords = this.facilities_list.length;
    }, (error) => {
      console.log(error);
    }
    );
  }


  //Method to fetch the corresponding facility facilityCode
  getFacilityCode(facility: any) {
    this.facilities_list.forEach(element => {
      if (element.name == facility) {
        this.benefTiObj.facilityCode = element.code;
      }
    });
  }

  onCancel() {
    this.router.navigateByUrl("ti/beneficiary/list");
  }
}
