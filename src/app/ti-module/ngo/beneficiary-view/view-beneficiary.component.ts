import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'
import { Beneficiary } from '@global/interfaces/Beneficiary';
import { BeneficiaryService } from '@global/services/TI-Beneficiary/beneficiary.service';

@Component({
  selector: 'app-view-beneficiary',
  templateUrl: './view-beneficiary.component.html',
  styleUrls: ['./view-beneficiary.component.css']
})
export class ViewBeneficiaryComponent implements OnInit {

  constructor(private _beneficiaryService: BeneficiaryService ,private router:Router) { }

  benefObj:Beneficiary;
  
  ngOnInit() {
    if (this._beneficiaryService.getBeneficiary()) {
      this.benefObj = this._beneficiaryService.getBeneficiary();
    }
  }

  back(){
    this.router.navigateByUrl("/beneficiary/list")
 }


}
