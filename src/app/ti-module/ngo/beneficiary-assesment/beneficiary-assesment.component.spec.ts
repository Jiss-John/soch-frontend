import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BeneficiaryAssesmentComponent } from './beneficiary-assesment.component';


describe('BeneficiaryAssesmentComponent', () => {
  let component: BeneficiaryAssesmentComponent;
  let fixture: ComponentFixture<BeneficiaryAssesmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BeneficiaryAssesmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BeneficiaryAssesmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
