import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BeneficiaryServicesComponent } from './beneficiary-services.component';

describe('BeneficiaryServicesComponent', () => {
  let component: BeneficiaryServicesComponent;
  let fixture: ComponentFixture<BeneficiaryServicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BeneficiaryServicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BeneficiaryServicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
