import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import Swal from 'sweetalert2';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Pattern } from '@shared';
import { BeneficiaryTI } from '@global/interfaces/beneficiaryTIRegistrationInterface';
import { Router } from '@angular/router';
import { BeneficiaryTIApiService } from '@global/services/TI-Beneficiary/beneficiary-ti-api.service';
import { BeneficiaryApiService } from '@global/services/TI-Beneficiary/beneficiary-api.service';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { LocalStorage, StorageMap } from '@ngx-pwa/local-storage';

@Component({
  selector: 'app-beneficiary-services',
  templateUrl: './beneficiary-services.component.html',
  styleUrls: ['./beneficiary-services.component.css']
})
export class BeneficiaryServicesComponent implements OnInit, AfterViewInit {

  inwardForm: FormGroup;
  submitted = false;
  outwardReferralObj = new BeneficiaryTI;
  pattern = new Pattern();
  userInfo: any;
  error: string;
  searchValue = new FormControl(); //Store value to search
  beneficiaryId: any;
  beneficiary = new BeneficiaryTI;
  activeLink: any = "";
  link: any = "";
  displayedColumns: string[] = ['uid', 'name', 'ostCode', 'mobileNumber', 'dueDateAssessment', 'assessmentPendingDays', 'beneficiaryActivityStatus', 'update'];
  dataSource: MatTableDataSource<BeneficiaryTI>;
  selection = new SelectionModel<BeneficiaryTI>(true, []);

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  isTransferred: boolean;
  isTIOST: boolean;
  isUserTIOST: boolean;

  constructor
    (
      private formBuilder: FormBuilder,
      private router: Router,
      private beneficiaryTIApiService: BeneficiaryTIApiService,
      private beneficiaryApiService: BeneficiaryApiService,
      private localStorage: StorageMap
    ) {
    // Create 100 users
    const users = Array.from({ length: 100 }, (_, k) => getReferralData(k + 1));

    // Assign the data to the data source for the table to render
    this.dataSource = new MatTableDataSource(users);
  }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.inwardForm = this.formBuilder.group({
      firstName: [''],
      mobileNumber: [''],
      beneficiaryUId: [''],
      beneficiaryId: [''],
    }
    );
    
    this.userInfo = JSON.parse(localStorage.getItem('currentUser'));
    this.isUserTIOST =  this.userInfo && this.userInfo.facilityTypeId == 10 ? true: false;
    
  }

  ngAfterViewInit() {
    this.localStorage.get("ti_beneficiary_list").subscribe((list: any) => {
      let listArr = list?JSON.parse(list) : [];
      this.dataSource = listArr.map(o => {
        return {
          'name': o.firstName + ' ' + o.lastName,
          'mobileNumber': o.mobileNumber,
          'uid': o.uidNumber,
          'beneficiaryActivityStatus': o.beneficiaryActivityStatus,
          'ostCode': o.ostNumber,
          'dueDateAssessment': new Date(),
          'assessmentPendingDays': 0
        }
      })
      console.log("### createOfflineBeneficiaryList", this.dataSource);
    });
  }



  //For searching based on UID (Beneficiary Id) and Mobile Number
  searchBeneficiary() {
    this.beneficiaryApiService.getBeneficiaryBysearchValue(this.searchValue.value).subscribe((Data) => {
      this.beneficiary = Data;
      this.beneficiaryId = this.beneficiary.id;
      //This is used to display values in text fields (using formControlName) without ngModel- donot remove this
      this.inwardForm.patchValue({
        //assigning values to formControl variable from the object that is subscribed value from api call.
        firstName: this.beneficiary.firstName,
        uidNumber: this.beneficiary.uidNumber,
        beneficiaryId: this.beneficiary.id,
        mobileNumber: this.beneficiary.mobileNumber,
        activityStatus: this.beneficiary.beneficiaryActivityStatus,
        isActive: this.beneficiary.isActive,
      });
      console.log(this.beneficiary);
    }, (error) => {
      Swal.fire(error.error.details ? error.error.details[0].description + "  !use advanced search" : "!use advanced search");
    }
    );
  }

  get outward() { return this.inwardForm.controls; }


  //Method to outward beneficiary
  referredTo() {
    this.submitted = true;

    if (this.inwardForm.invalid) {
      this.outwardReferralObj = this.inwardForm.value;
      console.log("Error in inwardForm", this.inwardForm.errors);
      return;
    }
    else {
      this.outwardReferralObj = this.inwardForm.value;
      console.log("Form is valid form values", this.inwardForm.value)
      // let refferalStatusVal = this.outwardReferralObj.referralStatus;
      // if (refferalStatusVal && refferalStatusVal === "Not Referred")
      //   return;

      this.beneficiaryTIApiService.referralAdd(this.outwardReferralObj)
        .subscribe((response) => {
          console.log("Response", response)
          Swal.fire('', 'Successfully saved changes!', 'success');

          this.router.navigateByUrl("/ti/beneficiary/list");

        }, (error) => {
          console.log(error);
        });
    }

  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: BeneficiaryTI): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    // return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.transferDate + 1}`;
  }

  doCommodityDistribution(uid: string) {
    this.router.navigate([`ti/beneficiary/distribution/${uid}`]);
  }

  doRVAssessment(uid: string) {
    this.router.navigate([`/ti/beneficiary/r&vAssessment/${uid}`]);
  }

  doScreening(uid: string) {
    this.router.navigate([`/ti/beneficiary/screening/${uid}`]);
  }

  doCounselling(uid: string) {
    this.router.navigate([`/ti/beneficiary/counselling/${uid}`]);
  }

  doRefferal(uid: string) {
    //this.router.navigate([`/ti/beneficiary/referral/${uid}`]);
    this.router.navigateByUrl('/ti/beneficiary/referral/outward');
  }

  doSTITreatment(uid: string) {
    this.router.navigate([`/ti/beneficiary/sti/${uid}`]);
  }

  doFollowUp(uid: string) {
    this.router.navigate([`ti/beneficiary/followup/${uid}`]);
  }

  doOSTAssessment(uid: string) {
    this.router.navigate([`/ti/beneficiary/ostAssessment/${uid}`]);
  }

  doPrescribe(uid: string) {
    this.router.navigate([`/ti/beneficiary/prescribe/${uid}`]);
  }

  doDispense(uid: string) {
    this.router.navigate([`/ti/beneficiary/dispense/${uid}`]);
  }

}


function getReferralData(id: number): BeneficiaryTI {
  let data = new BeneficiaryTI();
  return data;
}