import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BeneficiaryDistributionComponent } from './beneficiary-distribution.component';

describe('BeneficiaryDistributionComponent', () => {
  let component: BeneficiaryDistributionComponent;
  let fixture: ComponentFixture<BeneficiaryDistributionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BeneficiaryDistributionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BeneficiaryDistributionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
