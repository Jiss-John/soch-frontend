import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { DatePipe } from '@angular/common';
import Swal from 'sweetalert2';
import { Pattern } from '@shared';
import { BeneficiaryApiService } from '@global/services/TI-Beneficiary/beneficiary-api.service';
import { StorageMap } from '@ngx-pwa/local-storage';
import { TiDistributionDetail } from '@global/interfaces/tiDistributionInterface';

@Component({
  selector: 'app-beneficiary-distribution',
  templateUrl: './beneficiary-distribution.component.html',
  styleUrls: ['./beneficiary-distribution.component.css']
})
export class BeneficiaryDistributionComponent implements OnInit {

  distributionForm: FormGroup;
  submitted = false;
  pattern = new Pattern();
  beneficiary: any = {};
  distributionDetails = new TiDistributionDetail();
  error: string;
  searchValue = new FormControl(); //Store value to search
  beneficiaryId: any;
  constructor(private router: Router,
    private datePipe: DatePipe,
    private formBuilder: FormBuilder,
    private beneficiaryApiService: BeneficiaryApiService,
    private route: ActivatedRoute,
    private localStorage: StorageMap
  ) {
    this.beneficiaryId = this.route.snapshot.paramMap.get('uid');
  }

  ngOnInit() {
    this.screeningBeneficiary();
    this.distributionForm.patchValue({
      dateOfScreening: this.datePipe.transform(Date.now(), 'yyyy-MM-dd')
    });

    this.localStorage.get("ti_beneficiary_list").subscribe((list: any) => {

      let beneficiaryList = list ? JSON.parse(list) : [];

      console.log("### createOfflineBeneficiary", beneficiaryList);

      let filterBeneficiary = beneficiaryList.filter(o => {
        return o['uidNumber'] == this.beneficiaryId
      });

      this.beneficiary = filterBeneficiary.length > 0 ? filterBeneficiary[0] : null;
      console.log("### createOfflineBeneficiary", this.beneficiary);
      this.screeningBeneficiary();
    });
  }

  screeningBeneficiary() {
    this.distributionForm = this.formBuilder.group({
      beneficiaryName: [this.beneficiary.firstName + ' ' + this.beneficiary.lastName, [Validators.required, Validators.maxLength(99)]],
      mobileNumber: [this.beneficiary.mobileNumber, [Validators.required, Validators.pattern(this.pattern.mobilePattern)]],
      beneficiaryActivityStatus: [this.beneficiary.beneficiaryActivityStatus, [Validators.required]],
      // infection: ['', Validators.required],
      // dateOfScreening: ['', [Validators.required]],
      // screeningStatusHiv: ['', Validators.required],
      // screeningStatusSyphilis: ['', Validators.required],
      // tbStatus: ['', Validators.required],
      // presenceOfSweatGreaterThanThreeWeeks: ['false', Validators.required],
      // prolongedCoughGreaterThanThreeWeeks: ['false', Validators.required],
      // weightlossGreaterThan3kgInLastFourWeeks: ['false', Validators.required],
      // feverGreaterThanThreeWeeks: ['false', Validators.required],
      beneficiaryUId: [this.beneficiaryId],
      // screeningId: [''],
      condomsDistributed: ['', [Validators.required, Validators.maxLength(99)]],
      syringesNeedlesDistributed: ['', [Validators.required, Validators.maxLength(99)]],
      syringesNeedlesReturned: ['', [Validators.required, Validators.maxLength(99)]],
      condomUse: ['YES', Validators.required],
      typeOfContact: ['', Validators.required],
      notSharingNeedleSyringe: ['', Validators.required],
    }
    );
  }
  get beneficiaryScreen() { return this.distributionForm.controls; }

  //For searching based on UID (Beneficiary Id) and Mobile Number
  searchBeneficiary() {
    this.beneficiaryApiService.getBeneficiaryBysearchValue(this.searchValue.value).subscribe((Data) => {
      this.beneficiary = Data;
      this.beneficiaryId = this.beneficiary.id;
      //This is used to display values in text fields (using formControlName) without ngModel- donot remove this
      this.distributionForm.patchValue({
        //assigning values to formControl variable from the object that is subscribed value from api call.
        beneficiaryName: this.beneficiary.firstName,
        beneficiaryUId: this.beneficiary.uidNumber,
        beneficiaryId: this.beneficiary.id,
        mobileNumber: this.beneficiary.mobileNumber,
        beneficiaryActivityStatus: this.beneficiary.beneficiaryActivityStatus
      });
      console.log(this.beneficiary);
    }, (error) => {
      Swal.fire(error.error.details[0].description + "  !use advanced search");
    }
    );
  }

  //For saving the value to DB
  saveDistribution() {
    this.submitted = true;
    this.saveOfflineBeneficiary();
    if (this.distributionForm.invalid) {
      this.distributionDetails = this.distributionForm.value;
      return;
    }
    else {
      this.distributionDetails = this.distributionForm.value;
      //console.log(this.distributionDetails);
      // this.beneficiaryApiService.saveDistributionDetails(this.distributionDetails)
      //   .subscribe((response) => {
      //     Swal.fire('', 'Successfully saved changes!', 'success');
      //     this.router.navigateByUrl("ti/beneficiary/services");
      //   }, (error) => {
      //     console.log(error);
      //   });
    }
  }




  showErrorPopup(error) {
    console.log("####Service Error-:" + error);
    Swal.fire('', error, 'error');
  }

  // Function works on cancel button to navigate to benificiary list
  onCancel() {
    this.router.navigateByUrl("ti/beneficiary/services");
  }

  //Function to reset the form
  onReset() {
    this.submitted = false;
    this.distributionForm.reset();
  }

  saveOfflineBeneficiary() {
    let beneficiaryList;
    this.localStorage.get("ti_beneficiary_list").subscribe((list: any) => {
      beneficiaryList = list ? JSON.parse(list) : [];
      beneficiaryList.forEach(o => {
        if (o.uidNumber === this.beneficiaryId) {
          o.condomsDistributed = this.distributionForm.value.condomsDistributed;
          o.syringesNeedlesDistributed = this.distributionForm.value.syringesNeedlesDistributed;
          o.syringesNeedlesReturned = this.distributionForm.value.syringesNeedlesReturned;
          o.typeOfContact = this.distributionForm.value.typeOfContact;
          o.condomUse = this.distributionForm.value.condomUse;
          o.notSharingNeedleSyringe = this.distributionForm.value.notSharingNeedleSyringe;
        }
      });
      // this.localStorage.delete('ti_beneficiary_list');
      try {
        this.localStorage.set("ti_beneficiary_list", JSON.stringify(beneficiaryList)).subscribe((x) => {
          Swal.fire('', 'Successfully saved changes!', 'success');
          this.router.navigateByUrl("/ti/beneficiary/services");
        });
      } catch (err) {
        //to uncomment below code once issue is fixed
        Swal.fire('', 'Successfully saved changes!', 'success');
        this.router.navigateByUrl("/ti/beneficiary/services");
      }

      // console.log('main', this.localStorage.get("ti_beneficiary_list"));
    });
  }
}