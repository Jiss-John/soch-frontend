import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BeneficiaryCounsellingComponent } from './beneficiary-counselling.component';

describe('BeneficiaryCounsellingComponent', () => {
  let component: BeneficiaryCounsellingComponent;
  let fixture: ComponentFixture<BeneficiaryCounsellingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BeneficiaryCounsellingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BeneficiaryCounsellingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
