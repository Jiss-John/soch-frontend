import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Counselling } from '@global/Interfaces/counselling';
import { Pattern } from '@shared/infrastructure/Validator/pattern';
import Swal from 'sweetalert2';
import { BeneficiaryTIApiService } from '@global/services/TI-Beneficiary/beneficiary-ti-api.service';
import { BeneficiaryTI } from '@global/interfaces/beneficiaryTIRegistrationInterface';
import { BeneficiaryApiService } from '@global/services/TI-Beneficiary/beneficiary-api.service';
import { StorageMap } from '@ngx-pwa/local-storage';

@Component({
  selector: 'app-beneficiary-counselling',
  templateUrl: './beneficiary-counselling.component.html',
  styleUrls: ['./beneficiary-counselling.component.css']
})
export class BeneficiaryCounsellingComponent implements OnInit {
  counsellingForm: FormGroup;
  submitted = false;
  counselObj = new Counselling;
  pattern = new Pattern();
  userInfo: any;
  error: string;
  searchValue = new FormControl(); //Store value to search
  beneficiaryId: any;
  beneficiary = new BeneficiaryTI;

  constructor(private formBuilder: FormBuilder,
    private router: Router,
    private beneficiaryTIApiService: BeneficiaryTIApiService,
    private beneficiaryApiService: BeneficiaryApiService,
    private route: ActivatedRoute,
    private localStorage: StorageMap) {
    this.beneficiaryId = this.route.snapshot.paramMap.get('uid');
  }

  ngOnInit() {

    this.counselling();
    
    this.localStorage.get("ti_beneficiary_list").subscribe((list: any) => {

      let beneficiaryList = list ? JSON.parse(list) : [];

      console.log("### createOfflineBeneficiary", beneficiaryList);

      let filterBeneficiary = beneficiaryList.filter(o => {
        return o['uidNumber'] == this.beneficiaryId
      });

      this.beneficiary = filterBeneficiary.length > 0 ? filterBeneficiary[0] : null;
      console.log("### 2 createOfflineBeneficiary", this.beneficiary);
      this.counselling();
    });

    
  }
  counselling(){
    this.counsellingForm = this.formBuilder.group({
      typeOfCounsellingProvided: [''],
      durationOfCounselling: ['', Validators.pattern(this.pattern.counsellingDurationPattern)],
      noOfCondomsDistributed: ['', Validators.pattern(this.pattern.beneficiaryTiNumberOfPartnersPattern)],
      healthStatus: [''],
      firstName: [''],
      //isActive:['']
      beneficiaryName: [this.beneficiary.firstName + ' ' + this.beneficiary.lastName, [Validators.required, Validators.maxLength(99)]],
      mobileNumber: [this.beneficiary.mobileNumber, [Validators.required, Validators.pattern(this.pattern.mobilePattern)]],
      beneficiaryUId: [this.beneficiaryId],
      beneficiaryId: [this.beneficiary.id],
      beneficiaryActivityStatus: [this.beneficiary.beneficiaryActivityStatus, [Validators.required]],
    });
  }

  //For searching based on UID (Beneficiary Id) and Mobile Number
  searchBeneficiary() {
    this.beneficiaryApiService.getBeneficiaryBysearchValue(this.searchValue.value).subscribe((Data) => {
      this.beneficiary = Data;
      this.beneficiaryId = this.beneficiary.id;
      //This is used to display values in text fields (using formControlName) without ngModel- donot remove this
      this.counsellingForm.patchValue({
        //assigning values to formControl variable from the object that is subscribed value from api call.
        firstName: this.beneficiary.firstName,
        uidNumber: this.beneficiary.uidNumber,
        beneficiaryId: this.beneficiary.id,
        mobileNumber: this.beneficiary.mobileNumber,
        activityStatus: this.beneficiary.beneficiaryActivityStatus,
        isActive: this.beneficiary.isActive,
      });
      console.log(this.beneficiary);
    }, (error) => {
      Swal.fire(error.error.details ? error.error.details[0].description + "  !use advanced search" : "!use advanced search");
    }
    );
  }

  get counsell() { return this.counsellingForm.controls; }

  //Method to reset the form
  onReset() {
    this.submitted = false;
    this.counsellingForm.reset();
  }

  onCancel() {
    this.router.navigateByUrl("ti/beneficiary/services");
  }

  //Method to counsell beneficiary
  doCounselling() {
    this.submitted = true;
    this.saveOfflineBeneficiary()
    if (this.counsellingForm.invalid) {
      this.counselObj = this.counsellingForm.value;
      console.log("Error in counsellingForm", this.counsellingForm.errors);
      return;
    }
    else {
      console.log("Form is valid form values", this.counsellingForm.value)
      this.counselObj = this.counsellingForm.value;
      this.beneficiaryTIApiService.doCounselling(this.counselObj)
        .subscribe((response) => {
          console.log("Response", response)
          Swal.fire('', 'Successfully saved changes!', 'success');

          this.router.navigateByUrl("/ti/beneficiary/services");
          this.onReset();

        }, (error) => {
          console.log(error);
        });
    }

  }
  saveOfflineBeneficiary() {
    this.beneficiary.beneficiaryActivityStatus = this.counsellingForm.value.beneficiaryActivityStatus;
    let beneficiaryList;
    this.localStorage.get("ti_beneficiary_list").subscribe((list: any) => {
      beneficiaryList = list? JSON.parse(list) : [];
      beneficiaryList.forEach(o => {
        if (o.uidNumber === this.beneficiaryId) {
          o.beneficiaryActivityStatus = this.counsellingForm.value.beneficiaryActivityStatus;
        }
      });
      // this.localStorage.delete('ti_beneficiary_list');
      this.localStorage.set("ti_beneficiary_list", JSON.stringify(beneficiaryList)).subscribe((x) => {
        Swal.fire('', 'Successfully saved changes!', 'success');
        this.router.navigateByUrl("/ti/beneficiary/services");
      });
     // console.log('main', this.localStorage.get("ti_beneficiary_list"));
    });
  }
}
