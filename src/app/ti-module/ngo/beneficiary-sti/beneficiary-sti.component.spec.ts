import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BeneficiaryStiComponent } from './beneficiary-sti.component';

describe('BeneficiaryStiComponent', () => {
  let component: BeneficiaryStiComponent;
  let fixture: ComponentFixture<BeneficiaryStiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BeneficiaryStiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BeneficiaryStiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
