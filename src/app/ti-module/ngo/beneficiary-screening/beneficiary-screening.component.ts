import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { DatePipe } from '@angular/common';
import Swal from 'sweetalert2';
import { Pattern } from '@shared';
import { BeneficiaryTI } from '@global/interfaces/beneficiaryTIRegistrationInterface';
import { TiScreeningDetail } from '@global/interfaces/tiScreeningDetailInterface';
import { BeneficiaryApiService } from '@global/services/TI-Beneficiary/beneficiary-api.service';
import { StorageMap } from '@ngx-pwa/local-storage';

@Component({
  selector: 'beneficiary-screening',
  templateUrl: './beneficiary-screening.component.html',
  styleUrls: ['./beneficiary-screening.component.css']
})
export class BeneficiaryScreeningComponent implements OnInit {

  screeningForm: FormGroup;
  submitted = false;
  pattern = new Pattern();
  beneficiary = new BeneficiaryTI;
  tiScreeningDetails = new TiScreeningDetail();
  error: string;
  searchValue = new FormControl(); //Store value to search
  beneficiaryId: any;
  constructor(private router: Router,
    private datePipe: DatePipe,
    private formBuilder: FormBuilder,
    private beneficiaryApiService: BeneficiaryApiService,

    private route: ActivatedRoute,
    private localStorage: StorageMap
  ) {
    this.beneficiaryId = this.route.snapshot.paramMap.get('uid');
  }

  ngOnInit() {
    this.screeningBeneficiary();
    this.screeningForm.patchValue({
      dateOfScreening: this.datePipe.transform(Date.now(), 'yyyy-MM-dd')
    });
    this.localStorage.get("ti_beneficiary_list").subscribe((list: any) => {

      let beneficiaryList =list? JSON.parse(list):[];

      console.log("### createOfflineBeneficiary", beneficiaryList);

      let filterBeneficiary = beneficiaryList.filter(o => {
        return o['uidNumber'] == this.beneficiaryId
      });

      this.beneficiary = filterBeneficiary.length > 0 ? filterBeneficiary[0] : null;
      console.log("### 2 createOfflineBeneficiary", this.beneficiary);
      this.screeningBeneficiary();
    });
  }

  screeningBeneficiary() {
    this.screeningForm = this.formBuilder.group({
      beneficiaryName: [this.beneficiary.firstName + ' ' + this.beneficiary.lastName, [Validators.required, Validators.maxLength(99)]],
      mobileNumber: [this.beneficiary.mobileNumber, [Validators.required, Validators.pattern(this.pattern.mobilePattern)]],
      beneficiaryUId: [this.beneficiaryId],
      beneficiaryId: [this.beneficiary.id],
      beneficiaryActivityStatus: [this.beneficiary.beneficiaryActivityStatus, [Validators.required]],

      infection: ['', Validators.required],
      dateOfScreening: ['', [Validators.required]],
      screeningStatusHiv: ['', Validators.required],
      screeningStatusSyphilis: ['', Validators.required],
      tbStatus: ['', Validators.required],
      presenceOfSweatGreaterThanThreeWeeks: ['false', Validators.required],
      prolongedCoughGreaterThanThreeWeeks: ['false', Validators.required],
      weightlossGreaterThan3kgInLastFourWeeks: ['false', Validators.required],
      feverGreaterThanThreeWeeks: ['false', Validators.required],
      screeningId: ['']
    }
    );
  }
  get beneficiaryScreen() { return this.screeningForm.controls; }

  //For searching based on UID (Beneficiary Id) and Mobile Number
  searchBeneficiary() {
    this.beneficiaryApiService.getBeneficiaryBysearchValue(this.searchValue.value).subscribe((Data) => {
      this.beneficiary = Data;
      this.beneficiaryId = this.beneficiary.id;
      //This is used to display values in text fields (using formControlName) without ngModel- donot remove this
      this.screeningForm.patchValue({
        //assigning values to formControl variable from the object that is subscribed value from api call.
        beneficiaryName: this.beneficiary.firstName,
        beneficiaryUId: this.beneficiary.uidNumber,
        beneficiaryId: this.beneficiary.id,
        mobileNumber: this.beneficiary.mobileNumber,
        beneficiaryActivityStatus: this.beneficiary.beneficiaryActivityStatus
      });
      console.log(this.beneficiary);
    }, (error) => {
      Swal.fire(error.error.details[0].description + "  !use advanced search");
    }
    );
  }

  //For saving the value to DB
  saveScreening() {
    this.submitted = true;
    this.saveOfflineBeneficiary()

    if (this.screeningForm.invalid) {
      this.tiScreeningDetails = this.screeningForm.value;
      return;
    }
    else {
      this.tiScreeningDetails = this.screeningForm.value;
      console.log(this.tiScreeningDetails);
      this.beneficiaryApiService.saveScreeningDetails(this.tiScreeningDetails)
        .subscribe((response) => {
          Swal.fire('', 'Successfully saved changes!', 'success');
          this.router.navigateByUrl("ti/beneficiary/services");
        }, (error) => {
          console.log(error);
        });
    }
  }
  saveOfflineBeneficiary() {
    this.beneficiary.beneficiaryActivityStatus = this.screeningForm.value.beneficiaryActivityStatus;
    let beneficiaryList;
    this.localStorage.get("ti_beneficiary_list").subscribe((list: any) => {
      beneficiaryList = list?JSON.parse(list):[];
      beneficiaryList.forEach(o => {
        if (o.uidNumber === this.beneficiaryId) {
          o.beneficiaryActivityStatus = this.screeningForm.value.beneficiaryActivityStatus;
        }
      });
      // this.localStorage.delete('ti_beneficiary_list');
      this.localStorage.set("ti_beneficiary_list", JSON.stringify(beneficiaryList)).subscribe((x) => {
        Swal.fire('', 'Successfully saved changes!', 'success');
        this.router.navigateByUrl("ti/beneficiary/services");
      });
      //console.log('main', this.localStorage.get("ti_beneficiary_list"));
    });
  }

  //Function to display 'Screening Details for TB if Infection is TB'  based on selection in 'Screening Status (TB) as Positive'
  onChangeTbStatus() {
    this.tiScreeningDetails = this.screeningForm.value;
    console.log(this.tiScreeningDetails.tbStatus)
    //If selection changes to 'Negative' then need to reset the following field as false else they remains in last entered value.
    if (this.tiScreeningDetails.tbStatus == 'Negative') {
      this.screeningForm.patchValue({
        presenceOfSweatStatus: false,
        prolongedcoughStatus: false,
        weightLosStatus: false
      });
    }
  }

  showErrorPopup(error) {
    console.log("####Service Error-:" + error);
    Swal.fire('', error, 'error');
  }

  // Function works on cancel button to navigate to benificiary list
  onCancel() {
    this.router.navigateByUrl("ti/beneficiary/services");
  }

  //Function to reset the form
  onReset() {
    this.submitted = false;
    this.screeningForm.reset();
  }
}
