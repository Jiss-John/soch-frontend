import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BeneficiaryScreeningComponent } from './beneficiary-screening.component';

describe('BeneficiaryScreeningComponent', () => {
  let component: BeneficiaryScreeningComponent;
  let fixture: ComponentFixture<BeneficiaryScreeningComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BeneficiaryScreeningComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BeneficiaryScreeningComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
