import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'beneficiary-home',
  templateUrl: './beneficiary-home.component.html',
  styleUrls: ['./beneficiary-home.component.css']
})
export class BeneficiaryHomeComponent implements OnInit {

  userInfo = JSON.parse(localStorage.getItem('currentUser'));
  isUserTIOST =  this.userInfo && this.userInfo.facilityTypeId == 10 ? true: false;
  
  navTiCoreLinks = [
    {
      label: 'Beneficiary List',
      link: './list',
      index: 0
    }, {
      label: 'Add Beneficiary',
      link: './add',
      index: 1
    } ,
    {
      label: 'Pending Services',
      link: './services',
      index: 2
    },
    //  {
    //   label: 'Assessment',
    //   link: './r&vAssessment',
    //   index: 2
    // },
    // {
    //   label: 'Counselling',
    //   link: './counselling',
    //   index: 4
    // },
    {
      label: 'Referral',
      link: './referral',
      index: 3
    },
    // {
    //   label: 'STI Treatment',
    //   link: './sti',
    //   index: 6
    // },
    // {
    //   label: 'Commodity Distribution',
    //   link: './distribution',
    //   index: 7
    // },
    // {
    //   label: 'Screening',
    //   link: './screening',
    //   index: 8
    // },
  ];
  activeNGOLink = this.navTiCoreLinks[0];

  navTiOSTLinks = [
    {
      label: 'Beneficiary List',
      link: './list',
      index: 0
    }, {
      label: 'Add Beneficiary',
      link: './add',
      index: 1
    } ,
    {
      label: 'Pending Services',
      link: './pendingServices',
      index: 2
    },
   
  ];
  activeOSTLink = this.navTiOSTLinks[0];

  constructor() { 
  }

  ngOnInit() {
   
  }

}
