import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { faSearch, faEdit, faTrash, faEye } from '@fortawesome/free-solid-svg-icons';
import { Designation } from '../../global-module/interfaces/designationInterface';
import { Router } from '@angular/router';
import { DesignationApiService } from '@global/services/Designation/designation-api.service';
import { DesignationService } from '@global/services/Designation/designation.service';
import {PaginatorModule} from 'primeng/paginator';


@Component({
  selector: 'app-designation-list',
  templateUrl: './designation-list.component.html',
  styleUrls: ['./designation-list.component.css'],
 encapsulation : ViewEncapsulation.None 
})
export class DesignationListComponent implements OnInit {

  designation_list: Designation[];
  cols: any[];
  searchIcon = faSearch;
  edit_icon = faEdit;
  trash_icon = faTrash;
  view_icon = faEye;
  totalRecords: any;
  public popoverTitle: string = '';
  public popoverMessage: string = 'Do you want to delete Designation?';
  public confirmClicked: boolean = false;
  public cancelClicked: boolean = false;

  constructor(private _designationApiService: DesignationApiService,
              private router: Router, 
              private _designationService: DesignationService) { }

  ngOnInit() {
    this.getDesignations();
    this.cols = [
      { field: 'title', header: 'Title' },
      { field: 'active', header: 'Active Status' },
      { field: 'description', header: 'Description' },
    ];
  }

  getDesignations(): void {
    this._designationApiService.getAllDesignations().subscribe((Data) => {
      this.designation_list = Data;
      this.totalRecords = this.designation_list.length;
    }, (error) => {
      console.log(error);
    }
    );
  }

  addDesignation() {
    let designation: Designation;
    this._designationService.setDesignation(designation);
    this.router.navigateByUrl('/designation/add');
  }

  deleteDesignation(designation_row: Designation): void {
    if (this.confirmClicked = true) {
      this._designationApiService.deleteDesignation(designation_row).subscribe((response) => {
        this.getDesignations();
      }, (error) => {
        console.log(error);
      }
      );
    }
  }

  editDesignation(designation_row: Designation) {
    this._designationService.setDesignation(designation_row);
    this.router.navigateByUrl('/designation/add');
  }

  viewDesignation(designation_row: Designation) {
    this._designationService.setDesignation(designation_row);
    this.router.navigateByUrl('/designation/view');
  }
}




