import { Component, OnInit } from '@angular/core';
import { Designation } from '../../global-module/interfaces/designationInterface';
import { Router } from '@angular/router';
import { DesignationService } from '@global/services/Designation/designation.service';

@Component({
  selector: 'app-view-designation',
  templateUrl: './view-designation.component.html',
  styleUrls: ['./view-designation.component.css']
})
export class ViewDesignationComponent implements OnInit {

  designationObj = new Designation();

  constructor(private _designationService: DesignationService, private router: Router) { }

  ngOnInit() {
    if (this._designationService.getDesignation()) {
      this.designationObj = this._designationService.getDesignation();
      console.log(this.designationObj);
    }
  }
  addDesignation() {
    let designation: Designation;
    this._designationService.setDesignation(designation);
    this.router.navigateByUrl('/designation/add');
  }
  back() {
    this.router.navigateByUrl('/designation/list');
  }

}
