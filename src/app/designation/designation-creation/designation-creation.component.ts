import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Designation } from '../../global-module/interfaces/designationInterface';

import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { DesignationApiService } from '@global/services/Designation/designation-api.service';
import { DesignationService } from '@global/services/Designation/designation.service';
import { Pattern } from '@shared/infrastructure/Validator/pattern';


@Component({
  selector: 'app-designation-creation',
  templateUrl: './designation-creation.component.html',
  styleUrls: ['./designation-creation.component.css']
})
export class DesignationCreationComponent implements OnInit {

  addDesignation: FormGroup;
  submitted = false;
  error: any;
  duplicateCheck="";
  pattern = new Pattern();

  namePattern = '^[a-zA-Z]+[ a-zA-Z]*$';

  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private _designationApiService: DesignationApiService,
              private _designationService: DesignationService) { }

  designationObj = new Designation();

  ngOnInit() {
    if (this._designationService.getDesignation()) {
      this.designationObj = this._designationService.getDesignation();
    }

    this.addDesignation = this.formBuilder.group({
      title: ['', [Validators.required, Validators.pattern,Validators.maxLength(99)]],
      description: ['', [Validators.required]],
      active: ['true', [Validators.required]],
      id: ['']
    });
    this.designationObj.active = true;
  }

  get desgn() {
    return this.addDesignation.controls;
  }

  onSubmit() {
    this.submitted = true;
    if (this.addDesignation.invalid) {
      return;
    }
    else {
      this.designationObj = this.addDesignation.value;
      this._designationApiService.addDesignation(this.designationObj)
        .subscribe((response) => {
          Swal.fire('', 'Successfully saved changes!', 'success');
          this.router.navigateByUrl('/designation/list');
        }, (error) => {
          this.error = error.error.details[0].description;
          this.duplicateCheck=this.error;
        });
    }
  }

  onReset() {
    this.submitted = false;
    this.addDesignation.reset();
  }

  duplicationCheck(){
    let duplicateStr="Duplicate found '"+this.designationObj.title+"'";
    if(this.duplicateCheck.toLowerCase() == duplicateStr.toLowerCase()){
      this.error =this.duplicateCheck;
    }else{
      this.error ="";
    }
  }

}
