import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Division } from '../../global-module/interfaces/divisionInterface';
import { DivisionApiService } from '@global/services/Division/division-api.service';
import { DivisionService } from '@global/services/Division/division.service';
import Swal from 'sweetalert2';
import { FacilityType } from '../../global-module/interfaces/facilityTypeInterface';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { FacilityTypeApiService } from '@global/services/FacilityType/facility-type-api.service';
import { Pattern } from '@shared/Infrastructure/Validator/pattern';

@Component({
  selector: 'app-add-division',
  templateUrl: './add-division.component.html',
  styleUrls: ['./add-division.component.css']
})
export class AddDivisionComponent implements OnInit {


  registerForm: FormGroup;
  submitted = false;
  facility_types: FacilityType[];
  dropdownSettings: IDropdownSettings;
  divObj = new Division();
  error: any;
  errorMsg: any;
  pattern = new Pattern();
  constructor(private formBuilder: FormBuilder,
              private divisionService: DivisionService,
              private divisionApiService: DivisionApiService,
              private router: Router,
              private facilityTypeApiService: FacilityTypeApiService) { }

  ngOnInit() {

    this.registerForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      facilityTypeIds: ['', [Validators.required]],
      code: [''],
      headDdgName: ['', Validators.required],
      sccName: ['', [Validators.pattern('[a-zA-Z+ \s]+$')]],
      headDdgEmail: ['', [Validators.required]],
      sccEmail: ['', [Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$')]],
      npoEmail: ['', [Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$')]],
      headDdgMobileNo: ['', [Validators.minLength(10), Validators.pattern('[0-9]{10}$')]],
      npoMobileNo: ['', [Validators.minLength(10), Validators.pattern('[0-9]{10}$')]],
      sccMobileNo: ['', [Validators.minLength(10), Validators.pattern('[0-9]{10}$')]],
      isActive: ['Active', Validators.required],
      id: [''],
      npoName: ['', [Validators.required, Validators.maxLength(20)]]
    });


    this.getFacilityType();

    if (this.divisionService.getDivision()) {
      this.divObj = this.divisionService.getDivision();
    }

    this.getFacilityType();

    this.dropdownSettings = {
      singleSelection: false,
      idField: 'id',
      textField: 'facilityTypeName',
      itemsShowLimit: 7,
      enableCheckAll: false,
      allowSearchFilter: true
    };



  }
  getFacilityType(): void {
    this.facilityTypeApiService.getDivisionNotMappedFacilityTypes().subscribe((data) => {
      this.facility_types = data;
      for(var i = 0; i < this.divObj.facilityTypes.length; i++){
        this.facility_types.push(this.divObj.facilityTypes[i]);
      }
    }, (error) => {
      console.log(error);
    }
    );
  }

  get addDivision() { return this.registerForm.controls; }


  createDivision() {
    this.submitted = true;
    if (this.registerForm.invalid) {
      if(this.divObj.facilityTypes.length==0){
        this.error ="Facility Type is required"
       }
       this.errorMsg = '';
      return;
    }


    else {
      let arr = [];
      this.divObj = this.registerForm.value;
      this.divObj.facilityTypeIds.forEach(element => {
        arr.push(element.id);
      });
      this.divObj.facilityTypeIds = arr;
      this.divisionApiService.saveDivision(this.divObj)
        .subscribe((response) => {
          Swal.fire('', 'Successfully saved changes!', 'success');
          this.router.navigateByUrl('/division/list');
        }, (error) => {
        
          this.error=error.error.details[0].description;
         
        });
    }
  
  }

  onReset() {
    this.submitted = false;
    this.registerForm.reset();
  }
  onCancel() {
    this.onReset();
    this.router.navigateByUrl('/division/list');
  }
}
