import { Component, OnInit } from '@angular/core';
import { faSearch, faEdit, faTrash, faEye } from '@fortawesome/free-solid-svg-icons';
import { Router } from '@angular/router';
import { Division } from '../../global-module/interfaces/divisionInterface';
import { DivisionApiService } from '@global/services/Division/division-api.service';
import { DivisionService } from '@global/services/Division/division.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-division-list',
  templateUrl: './division-list.component.html',
  styleUrls: ['./division-list.component.css']
})
export class DivisionListComponent implements OnInit {

  divisions_list: Division[];
  cols: any[];
  searchIcon = faSearch;
  edit_icon = faEdit;
  trash_icon = faTrash;
  view_icon = faEye;
  totalRecords: any;
  public popoverTitle: string = '';
  public popoverMessage: string = 'Do you want to delete Division?';
  public confirmClicked: boolean = false;
  public cancelClicked: boolean = false;


  constructor(private divisionApiService: DivisionApiService,
              private divisionService: DivisionService,
              private router: Router) { }

  ngOnInit() {
    this.divisionList();
    this.cols = [
      { field: 'code', header: 'Code' },
      { field: 'name', header: 'Division Name' },
      { field: 'facilityTypes', header: 'Facility Type' },
      { field: 'headDdgName', header: 'Contact person' },
      { field: 'headDdgMobileNo', header: 'Mobile Number', }
    ];

  }

  divisionList(): void {
    this.divisionApiService.divisionList()
      .subscribe((data) => {
        this.divisions_list = data,
        this.totalRecords = this.divisions_list.length;
      }, (error) => {
        console.log(error);
      });
  }

  addDivisionButton() {
    const division = new Division();
    this.divisionService.setDivision(division);
    this.router.navigateByUrl('/division/add');
  }

  editDivision(division: Division) {
    this.divisionService.setDivision(division);
    this.router.navigateByUrl('/division/add');
  }

  deleteDivision(division: Division) {
    this.divisionApiService.divisionDelete(division)
      .subscribe((data) => {
        Swal.fire('', 'Successfully deleted!', 'success');
        this.divisionList();
      }, (error) => {
        console.log(error);
      });

  }
  viewDivision(viewDivision: Division) {
    this.divisionService.setDivision(viewDivision);
    this.router.navigateByUrl('/division/view');
  }

}
