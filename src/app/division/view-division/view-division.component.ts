import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { Division } from '../../global-module/interfaces/divisionInterface';
import { DivisionService } from '@global/services/Division/division.service';


@Component({
  selector: 'app-view-division',
  templateUrl: './view-division.component.html',
  styleUrls: ['./view-division.component.css']
})
export class ViewDivisionComponent implements OnInit {

  constructor(private divisionService:DivisionService, private router:Router) { }

  divObj = new Division();

  ngOnInit() {

    if(this.divisionService.getDivision()){
      this.divObj=this.divisionService.getDivision();
    }
  }
  addDivisionButton() {
    const division = new Division();
    this.divisionService.setDivision(division);
    this.router.navigateByUrl('/division/add');
  }
  goBack() {
    this.router.navigateByUrl('/division/list');
  }

}
