import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { faSearch, faEdit, faTrash, faEye } from '@fortawesome/free-solid-svg-icons';
import { Router } from '@angular/router';
import { ProductService } from '../services/product.service';
import { Product } from '../interfaces/product.interface';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit, AfterViewInit {
  cols: any[];
  searchIcon = faSearch;
  edit_icon = faEdit;
  trash_icon = faTrash;
  view_icon = faEye;
  totalRecords: any;
  public popoverTitle: string = '';
  public popoverMessage: string = 'Do you want to delete Product?';
  public confirmClicked: boolean = false;
  public cancelClicked: boolean = false;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  displayedColumns: string[] = ['id','productName', 'shortCode','productType','division','status','update'];
  productListDataSource: MatTableDataSource<Product>;
  dt:any;

  constructor(private productService: ProductService, private router: Router) {
  }

  ngOnInit() {
   
  }

  ngAfterViewInit() {
    this.productService.getAllProducts().subscribe((productData: any) => {
      this.productListDataSource = productData;
      this.productListDataSource.paginator = this.paginator;
      this.productListDataSource.sort = this.sort;
    });
  }

  getProducts(): void {
    this.productService.getAllProducts().subscribe((productData: any) => {
      this.productListDataSource = productData;
    }, (error) => {
      console.log(error);
    }
    );
  }

  deleteProduct(productRow): void {
    this.productService.deleteProduct(productRow).subscribe((response) => {
      this.getProducts();
    }, (error) => {
      console.log(error);
    }
    );
  }

  editProduct(productRow: Product) {
    this.productService.setProduct(productRow);
    this.router.navigateByUrl('product/add');
  }

  navigateToAddProductPage(): void {
    this.productService.setProduct(null);
    this.router.navigateByUrl("product/add");
  }

  viewProduct(product: Product) {
    this.productService.setProduct(product);
    this.router.navigateByUrl('product/view');
  }

}


