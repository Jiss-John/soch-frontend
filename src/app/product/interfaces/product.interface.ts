import { Master } from '@global/Interfaces/master.interface';

export class Product
{

    id:any;
    productName:any;
    uom:Master;
    batchNo:any;
    shortCode:any;
    productType:Master;
    division:Master;
    facilityTypes:Master[];
    labTypes:Master[];
    minShelfLife:any;
    productImage:any;
    formulationComposition:any;
    productDetails:any;
    status:Master;
    
}