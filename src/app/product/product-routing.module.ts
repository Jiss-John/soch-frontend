import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductCreateComponent } from './product-create/product-create.component';
import { ProductListComponent } from './product-list/product-list.component';
import { ProductViewComponent } from './product-view/product-view.component';


const mainRoutes: Routes = [
  { redirectTo: 'list', path: '', pathMatch: 'full' },
  { path: 'list', component: ProductListComponent},
  { path: 'add', component: ProductCreateComponent},
  { path: 'view', component: ProductViewComponent}
];

@NgModule({
  imports: [RouterModule.forChild(mainRoutes)],
  exports: [RouterModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ProductRoutingModule { }
