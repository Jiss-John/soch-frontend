import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { Product } from '../interfaces/product.interface';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  product:Product;

  constructor(private _httpService: HttpClient) { }

  setProduct(product: Product) {
    this.product = product;
  }

  getProduct(){
    return this.product;
  }

  saveProduct(product: Product) {
    let body = JSON.stringify(product);
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options = { headers: headers }
    return this._httpService.post(environment.APIUrl + 'product/save', body, options);

  }

  deleteProduct(product: Product) {

    let body = JSON.stringify(product);
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options = { headers: headers };
    return this._httpService.delete(environment.APIUrl + 'product/'+product.id+'/delete', options);
  }

  getAllProducts(): Observable<Product[]> {
    return this._httpService.get<Product[]>(environment.APIUrl + 'product/list');
  }


}
