import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProductService } from '../services/product.service';
import { Product } from '../interfaces/product.interface';

@Component({
  selector: 'app-product-view',
  templateUrl: './product-view.component.html',
  styleUrls: ['./product-view.component.css']
})
export class ProductViewComponent implements OnInit {

  constructor(private router: Router, private productService:ProductService) { }

  product = new Product();
  
  ngOnInit() {

    if(this.productService.getProduct()){
      this.product=this.productService.getProduct();
    }
  }
  
  navigateToAddProductPage(): void{
    const product = new Product();
		this.productService.setProduct(product);
    this.router.navigateByUrl("product/add");
  }

  goBack() {
    this.router.navigateByUrl('/product/list');
  }

}
