import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Pattern } from '@shared';
import { Product } from '../interfaces/product.interface';
import { ProductService } from '../services/product.service';
import Swal from 'sweetalert2';
import { MastersApiService } from '@global/services/masters/masters-api.service';
import { DivisionApiService } from '@global/services/Division/division-api.service';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { FileUploadService } from '@global/services/fileupload.service';
import { Master } from '@global/Interfaces/master.interface';
import { ValidateDropdownSelection, ValidateMultiDropdownSelection } from '@shared/validator/validator';

@Component({
  selector: 'app-product-create',
  templateUrl: './product-create.component.html',
  styleUrls: ['./product-create.component.css']
})
export class ProductCreateComponent implements OnInit {

  productCreationForm: FormGroup;
  dropdownSettings: IDropdownSettings;
  masterDropdownSettings: IDropdownSettings;
  fileToUpload: File = null;
  uomList: any[];
  productTypeList: any[];
  divisionList: any[];
  facilityTypeList: any[];
  labTypeList: any[] = [];
  batchNoOptionList: any[] = [];
  submitted = false;
  product = new Product();
  pattern = new Pattern();



  constructor(private formBuilder: FormBuilder, private router: Router,
    private productService: ProductService, private mastersApiService: MastersApiService,
    private divisionApiService: DivisionApiService,
    private fileUploadService: FileUploadService) { }

  ngOnInit() {
    this.setMultiSelectSettings();
    this.initControls();
    this.getUomList();
    this.getProductyTypeList();
    this.getDivisionList();
    this.getFacilityTypeList();
    this.getLabTypeList();
    this.getAllBatchNoOptions();

  }
  initControls() {
    let id: number;
    let productName: string;
    let uom: number = -1;
    let batchNo: string = '-1';
    let shortCode: string;
    let productType: number = -1;
    let division: number = -1;
    let facilityTypes: any[] = [];
    let labTypes: Master[];
    let minShelfLife: string;
    let productImage: string;
    let formulationComposition: string;
    let productDetails: string;
    let status: number = 1;

    if (this.productService.getProduct() && this.productService.getProduct().id > 0) {
      this.product = this.productService.getProduct();
      id = this.product.id;
      productName = this.product.productName;
      if (this.product.uom) {
        uom = this.product.uom.id;
      }
      if (this.product.batchNo) {
        batchNo = this.product.batchNo.code;
      }
      shortCode = this.product.shortCode;
      if (this.product.productType) {
        productType = this.product.productType.id;
      }
      if (this.product.division) {
        division = this.product.division.id;
      }
      if (this.product.facilityTypes) {
        this.product.facilityTypes.forEach(function (value) {
          facilityTypes.push({ "id": value.id, "facilityTypeName": value.name });
        });
      }
      labTypes = this.product.labTypes;
      minShelfLife = this.product.minShelfLife;
      productImage = this.product.productImage;
      formulationComposition = this.product.formulationComposition;
      productDetails = this.product.productDetails;
      if (this.product.status) {
        status = this.product.status.id;
      }
    }
    this.productCreationForm = this.formBuilder.group({
      id: [id],
      productName: [productName, [Validators.required, Validators.pattern(this.pattern.whiteSpace)]],
      uom: [uom, [Validators.required, ValidateDropdownSelection]],
      batchNo: [batchNo, [Validators.required, ValidateDropdownSelection]],
      shortCode: [shortCode, [Validators.required]],
      productType: [productType],
      division: [division, [Validators.required, ValidateDropdownSelection]],
      facilityTypes: [facilityTypes, [Validators.required, ValidateMultiDropdownSelection]],
      labTypes: [labTypes],
      minShelfLife: [minShelfLife],
      productImage: [productImage],
      formulationComposition: [formulationComposition],
      productDetails: [productDetails],
      status: [status],
    });
  }

  setMultiSelectSettings(): void {
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'id',
      textField: 'facilityTypeName',
      itemsShowLimit: 7,
      enableCheckAll: false,
      allowSearchFilter: true
    };
    this.masterDropdownSettings = {
      singleSelection: false,
      idField: 'id',
      textField: 'name',
      itemsShowLimit: 7,
      enableCheckAll: false,
      allowSearchFilter: true
    };
  }

  getUomList(): void {
    this.mastersApiService.fetchProductUomList().subscribe((data) => {
      this.uomList = data;
    }, (error) => {
      this.showErrorPopup(error);
    }
    );
  }

  getProductyTypeList(): void {
    this.mastersApiService.fetchProductTypeList().subscribe((data) => {
      this.productTypeList = data;
    }, (error) => {
      this.showErrorPopup(error);
    }
    );
  }

  getDivisionList(): void {
    this.divisionApiService.divisionList().subscribe((data) => {
      this.divisionList = data;
    }, (error) => {
      this.showErrorPopup(error);
    }
    );
  }

  getFacilityTypeList(): void {
    this.mastersApiService.fetchFacilityTypeList().subscribe((data) => {
      this.facilityTypeList = data;
    }, (error) => {
      this.showErrorPopup(error);
    }
    );
  }

  getLabTypeList(): void {
    this.mastersApiService.fetchAllLabTypes().subscribe((data) => {
      this.labTypeList = data;
    }, (error) => {
      this.showErrorPopup(error);
    }
    );
  }

  getAllBatchNoOptions(): void {
    this.mastersApiService.fetchAllBatchNoOptions().subscribe((data) => {
      this.batchNoOptionList = data;
    }, (error) => {
      this.showErrorPopup(error);
    }
    );
  }

  uploadFileToServer(files: FileList) {
    this.fileToUpload = files.item(0);
    this.fileUploadService.uploadFileToServer(this.fileToUpload).subscribe(data => {
      // do something, if upload success
    }, error => {
      console.log(error);
    });
  }

  saveProduct() {
    this.submitted = true;
    if (this.productCreationForm.invalid) {
      return;
    }
    else {
      this.constructProductFromForm();
      this.productService.saveProduct(this.product)
        .subscribe((response) => {
          Swal.fire('', 'Successfully saved changes!', 'success');
          this.router.navigateByUrl('product/list');
        }, (error) => {
          console.log(error);
        });
    }
  }

  constructProductFromForm(): void {
    this.product = new Product();
    let formValue = this.productCreationForm.value;
    this.product.id = formValue.id;
    this.product.productName = formValue.productName;

    if(formValue.uom){
      let uom = new Master();
      uom.id = formValue.uom;
      this.product.uom = uom;
    }
    
    if(formValue.batchNo){
      this.product.batchNo = formValue.batchNo;
    }
    this.product.shortCode = formValue.shortCode;

    if(formValue.productType){
      let productType = new Master();
      productType.id = formValue.productType;
      this.product.productType = productType;
    }
   
    if(formValue.division){
      let division = new Master();
      division.id = formValue.division;
      this.product.division = division;
    }
    
    let facilityTypes: Master[] = [];
    formValue.facilityTypes.forEach(function (value) {
      let facilityType = new Master();
      facilityType.id = value.id;
      facilityTypes.push(facilityType);
    });
    this.product.facilityTypes = facilityTypes;

    let labTypes: Master[] = [];
    formValue.labTypes.forEach(function (value) {
      let labType = new Master();
      labType.id = value.id;
      labTypes.push(labType);
    });
    this.product.labTypes = labTypes;

    this.product.minShelfLife = formValue.minShelfLife;
    this.product.productImage = formValue.productImage;
    this.product.formulationComposition = formValue.formulationComposition;
    this.product.productDetails = formValue.productDetails;

    let status = new Master();
    status.id = formValue.status;
    this.product.status = status;

  }

  // To reset the facilityCreationForm
  onReset() {
    this.submitted = false;
    this.productCreationForm.reset();
  }

  onCancel() {
    this.router.navigateByUrl('/product/list');
  }

  showErrorPopup(error) {
    console.log("####Service Error-:" + error);
    Swal.fire('', error, 'error');
  }

  // convenience getter for easy access to form fields
  get productForm() { return this.productCreationForm.controls; }



}
