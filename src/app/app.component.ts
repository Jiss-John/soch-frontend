import { Component } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { AuthenticationService } from '@global/services/Login/authentication.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  currentUser: any;
  showHead: boolean = false;

  checkClass:boolean;

  constructor(router: Router, private authenticationService: AuthenticationService) {
    this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
    // on route change to '/login', set the variable showHead to false
    router.events.forEach((event) => {
      if (event instanceof NavigationStart) {
        if ( event['url'] == '/login' || event['url'].includes("/login")) {
          this.showHead = false;
        }
        else {
          this.showHead = true;
        }
      }
    });
  }
  parendCSD(dd){
    // console.log("parendCSD");
    // console.log(dd);
    this.checkClass=dd;
  }
}
