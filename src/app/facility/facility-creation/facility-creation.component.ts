import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Facility } from '@global/interfaces/facilityInterface';
import { FacilityApiService } from '@global/services/Facility/facility-api.service';
import { FacilityService } from '@global/services/Facility/facility.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { State } from '@global/interfaces/stateInterface';
import { SelectDistrictFromStateService } from '@global/services/Facility/select-district-from-state.service';
import { FacilityType } from '@global/interfaces/facilityTypeInterface';
import { Division } from '@global/interfaces/divisionInterface';
import { User } from '@global/interfaces/userInterface';
import { UserService } from '@global/services/User/user.service';
import { Pattern } from '@shared';


@Component({
  selector: 'app-facility-creation',
  templateUrl: './facility-creation.component.html',
  styleUrls: ['./facility-creation.component.css']
})
export class FacilityCreationComponent implements OnInit {

  facilityCreationForm: FormGroup;
  submitted = false;

  facilityObj = new Facility();
  pattern = new Pattern();

  statesAndDistricts_list: State[] = [];
  facilityType_list: FacilityType[] = [];
  division_list: Division[] = [];
  districts: any;
  user_list: FacilityType[] = [];
  userFacilityType_list: User[];
  maxOfId: number;
  code: String;
  divisionId: any;
  divisionName: any;
  facilityTypeId: number;
  error: any;


  constructor(private formBuilder: FormBuilder,
    private facilityApiService: FacilityApiService,
    private FacilityService: FacilityService,
    private router: Router,
    private selectService: SelectDistrictFromStateService,
    private userService: UserService,
    private facilityService: FacilityService) { }

  ngOnInit() {
    this.FacilityCreationForm();
    this.getAllStatesAndDistricts();
    this.getFacilityTypeList();

    if (this.FacilityService.getFacility()) {
      this.facilityObj = this.FacilityService.getFacility();
    }
  }

  // decalaring form control names with required validations
  FacilityCreationForm() {
    this.facilityCreationForm = this.formBuilder.group({
      id: [''],
      name: ['', [Validators.required,   Validators.pattern(this.pattern.whiteSpace)]],
      code: [''],
      addressId: [''],
      status: ['Active', Validators.required, ],
      facilityTypeId: ['', Validators.required],
      divisionId: ['', Validators.required],
      divisionName: [''],
      stateId: ['', Validators.required],
      districtId: ['', Validators.required],
      city: ['', Validators.required],
      facilityAddress: ['', Validators.required],
      // primaryUserId: ['', Validators.required],
      pincode: ['', Validators.required],
      userFacilityMappingId: ['']
    });
  }

  // convenience getter for easy access to form fields
  get facility() { return this.facilityCreationForm.controls; }

  // To get all state and districts in INDIA to display in dropdown
  getAllStatesAndDistricts() {
    this.selectService.getAllStatesAndDistricts().subscribe((Data) => {
      this.statesAndDistricts_list = Data;
      if (this.facilityObj.id) {
        this.onSelect(this.facilityObj.address.stateId);
      }
      if (!this.facilityObj.id && this.facilityObj.name) {
        this.onSelect(this.facilityObj.stateId);
      }
    }, (error) => {
      console.log(error);
    }
    );
  }

  // To get all facility types with its corresponding divsion details
  getFacilityTypeList() {
    this.facilityApiService.getAllFacilityTypes().subscribe((Data) => {
      this.facilityType_list = Data;
      if (this.facilityObj.name) {
        this.onFacilityTypeSelect(this.facilityObj.facilityTypeId);

      }
    }, (error) => {
      console.log(error);
    }
    );
  }
  // To get distict list correponding to a selected state
  onSelect(stateid: any) {
    if (this.facilityObj.id) {
      this.statesAndDistricts_list.forEach(element => {
        if (element.id == stateid) {
          this.districts = element.districts;
        }
      });
    }
    else if (!this.facilityObj.id) {
      console.log(stateid);
      this.statesAndDistricts_list.forEach(element => {
        console.log(element.id)
        if (element.id == stateid) {
          this.districts = element.districts;
          console.log("dis"+this.districts);
          this.facilityCreationForm.patchValue({'districtId': this.districts[0].id});
          // this.districtId=this.districts[0].id;
        }
      });
    }
  }



  // To get divsion list & user list corresponding to selected Facility type
  onFacilityTypeSelect(facilityTypeId: any) {
    this.facilityTypeId = facilityTypeId;
    // Divsion List
    this.facilityType_list.forEach(element => {
      if (element.id == facilityTypeId) {
        this.division_list = element.division;
        this.division_list.forEach(element => {
          this.divisionId = element.id;
          this.divisionName = element.name;
        });
      }
    }
    );
    // User List
    this.facilityType_list.forEach(element => {
      if (element.id == facilityTypeId) {
        this.user_list = element.user;
      }
    });
  }

  // To reset the facilityCreationForm
  onReset() {
    this.submitted = false;
    this.facilityCreationForm.reset();
  }

  // cancel function to reset the page and redirect to facility list page
  onCancel() {
    this.onReset();
    this.router.navigateByUrl('facility/list');
  }

  // to redirect to add user page
  // addUserBtn() {
  //   const user = new User();
  //   user.facilityTypeId = this.facilityTypeId;
  //   user.divisionId = this.divisionId;
  //   user.needToResendToFacility = true;
  //   this.userService.setUser(user);
  //   this.facilityObj = this.facilityCreationForm.value;
  //   console.log(this.facilityObj);
  //   this.facilityService.setFacility(this.facilityObj);
  //   this.router.navigateByUrl('/user/add');
  // }

  saveFacility() {
    console.log("Coming")
    this.submitted = true;
    if (this.facilityCreationForm.invalid) {
      return;
    }
    else {
      this.facilityObj = this.facilityCreationForm.value;
      this.facilityApiService.saveFacility(this.facilityObj)
        .subscribe((response) => {
          Swal.fire('', 'Successfully saved changes!', 'success');
          this.router.navigateByUrl('facility/list');
        }, (error) => {
           this.error =error.error.details[0].description;
          console.log(error);
        });
    }
    this.onReset();
  }
}
