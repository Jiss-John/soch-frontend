import { Component, OnInit } from '@angular/core';
import { faSearch, faEdit, faTrash, faEye } from '@fortawesome/free-solid-svg-icons';
import { FacilityApiService } from '@global/services/Facility/facility-api.service';
import { FacilityService } from '@global/services/Facility/facility.service';
import { Facility } from '@global/interfaces/facilityInterface';
import { Router } from '@angular/router';
import { Address } from '@global/interfaces/addressInterface';
import { User } from '@global/interfaces/userInterface';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { element } from 'protractor';

@Component({
  selector: 'app-facility-list',
  templateUrl: './facility-list.component.html',
  styleUrls: ['./facility-list.component.css']
})
export class FacilityListComponent implements OnInit {
  facilities_list: Facility[];
  cols: any[];
  searchIcon = faSearch;
  edit_icon = faEdit;
  trash_icon = faTrash;
  view_icon = faEye;
  totalRecords: any;
  public popoverTitle: string = '';
  public popoverMessage: string = 'Do you want to delete facility?';
  public confirmClicked: boolean = false;
  public cancelClicked: boolean = false;

  constructor(private _facilityApiService: FacilityApiService, private _FacilityService: FacilityService, private router: Router) { }

  ngOnInit() {
    this.getFacilities();

    this.cols = [
      
      { field: 'facilityName', header: 'Facility Name' },
      { field: 'code', header: 'Facility Code' },
      { field: 'division', header: 'Division' },
      { field: 'facilityType', header: 'Facility Type' },
      // { field: 'primaryUserName', header: 'Primary User' },
      { field: 'status', header: 'Status' }
    ];
  }

  getFacilities(): void {
    this._facilityApiService.getAllFacilities().subscribe((facilityData) => {
      this.facilities_list = facilityData;
      console.log(this.facilities_list)

      this.totalRecords = this.facilities_list.length;
    }, (error) => {
      console.log(error);
    }
    );
  }

  deleteFacility(facility_row): void {
    console.log(facility_row);
    this._facilityApiService.deleteFacility(facility_row).subscribe((response) => {
      this.getFacilities();
    }, (error) => {
      console.log(error);
    }
    );
  }

  editFacility(facility_row: Facility) {
    this._FacilityService.setFacility(facility_row);
    this.router.navigateByUrl('facility/add');
  }

  viewFacility(facility_row: Facility) {
    this._FacilityService.setFacility(facility_row);
    this.router.navigateByUrl('facility/view');
  }

  addFacBtn(){
    const user=new User();
    const address=new Address();
    const facility = new Facility();
    facility.address=address;
    facility.user=user;
    this._FacilityService.setFacility(facility);
    this.router.navigateByUrl("facility/add");
  }

  
}



