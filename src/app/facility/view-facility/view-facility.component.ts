import { Component, OnInit } from '@angular/core';
import { FacilityService } from '@global/services/Facility/facility.service';
import { Facility } from '@global/interfaces/facilityInterface';
import { Router } from '@angular/router';
import { Address } from '@global/interfaces/addressInterface';
import { User } from '@global/interfaces/userInterface';


@Component({
  selector: 'app-view-facility',
  templateUrl: './view-facility.component.html',
  styleUrls: ['./view-facility.component.css']
})
export class ViewFacilityComponent implements OnInit {

  facilityObj = new Facility();

  constructor(private _FacilityService: FacilityService, private router: Router) { }

  ngOnInit() {
    this.getFacilities();

  }

  getFacilities(): void {
    if (this._FacilityService.getFacility()) {
      this.facilityObj = this._FacilityService.getFacility();
    }
  }
  addFacBtn(){
    const user=new User();
    const address=new Address();
    const facility = new Facility();
    facility.address=address;
    facility.user=user;
    this._FacilityService.setFacility(facility);
    this.router.navigateByUrl("facility/add");
  }
  listFacBtn() {
    let facility: Facility;
    this.router.navigateByUrl("facility/list");
  }

}
