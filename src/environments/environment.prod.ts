import { NgxLoggerLevel } from 'ngx-logger';

// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: true,
  environmentName: 'Production',
  domain: '.client1.com', 
  logLevel: NgxLoggerLevel.ERROR,
  serverLogLevel: NgxLoggerLevel.ERROR,
  serverLoggingUrl: 'http://localhost:4300/api/logs',
  APIUrl : 'http://104.211.200.169:8080/admin/',
  tiAPIUrl:"http://104.211.200.169:8080/ti/",
  cstAPIUrl: "http://104.211.200.169:8080/cst/",
};

